<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Person</title>
    </head>
   <h2 class="etext">Edit Company</h2>
            <div class="formsection">
			 <form action="#" method="post">
			   <table width="749" border="0" cellspacing="0" cellpadding="0" align="center">
				  <tr>
					<td  align="left" valign="top" width="145" class="gtext">Name</td>
					<td  align="left" valign="top" width="2" class="gtext">:</td>
					<td align="right" valign="top" width="602">${person?.company?.name}</td>
				  </tr>
				  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
				  <tr>
					<td colspan="3" align="left" valign="top">
					  <table width="749" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="377" align="left" valign="top">
						  <table width="377" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Category</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.category}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Address</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.address?.street1}</textarea></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Phone</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.address?.phone}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Toll Free</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.address?.tollfreephone}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Fax</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.address?.fax}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Year Founder</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.founded}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Employee Count</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.noofemployees}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Company Information</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.bio}</textarea></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">Terms of Service</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="230">${person?.company?.tos}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="145" class="gtext">&nbsp;</td>
								<td align="left" valign="top" width="2" class="gtext">&nbsp;</td>
								<td align="left" valign="top" width="230"> <input type="submit" value="Update" name="update" id="update" class="updatebtn" /></td>
							  </tr>
							</table>

						</td>
						<td align="left" valign="top" width="25">&nbsp;</td>
						<td align="left" valign="top" width="345">
						   <table width="345" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">Membership Level</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">
								 ${person?.company?.membershiplevel?.memlevel}
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">Country</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">
								 ${person?.company?.address?.country?.name}
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">State</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">
								 ${person?.company?.address?.state}
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">City</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">${person?.company?.address?.city}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">Zip</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">${person?.company?.address?.postalcode}</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" width="125" class="gtext">Timezone</td>
								<td align="left" valign="top" width="2" class="gtext">:</td>
								<td align="right" valign="top" width="220">${person?.company?.address?.timezone}</td>
							  </tr>
							</table>
						</td>
					  </tr>
					</table>
					</td>
				  </tr>
				</table>

			 </form>
			</div>




                <g:form>
                    <input type="hidden" name="id" value="${person?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
