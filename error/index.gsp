<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title></title>
<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery-ui.css')}" />
<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/demo.css')}" />
<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/styleinner.css')}" />
<r:require module="home"/>
<r:require module="jquery"/>
<r:require modules="jquery-validation-ui" />
<r:layoutResources/>

<r:external uri="/js/pz/jquery-ui.js" />

</head>

<body>

<r:layoutResources/>
<script language="javascript" >
$(document).ready(function() { 
$('#dialog-login').dialog({ 
	autoOpen: false, 
    height: 500,     
	width: 700,	
    modal: true,	
 });    
$('#dialog-forgotPassword').dialog({ 
	autoOpen: false, 
    height: 300,     
	width: 400,	
    modal: true,	
 });    
$('#dialog-forgotPasswordConfirm').dialog({ 
	autoOpen: false, 
    height: 300,     
	width: 400,	
    modal: true,	
 });    
 });    

function authAjax()
{
var formdata = $('#loginForm').serialize();
var dataUrl = '${postUrl?postUrl:"/tdly/j_spring_security_check"}'      
jQuery.ajax({
type : 'POST',
url :  dataUrl ,
data : formdata,
success : function(response,textStatus)
{
//emptyForm();
       if(response.success)
   {
   var redirectUrl="${ createLink(action:'list' ,controller:'broadcast') }";
        window.location.assign(redirectUrl);
       }
       else
   {
        $('#errorLoginMsg').html(response.error);
   }
},
error : function(
XMLHttpRequest,
textStatus,
errorThrown) {
}
});
}
 
function submitPassword() {
var formdata = $('#forgotPasswordForm').serialize();
var dataUrl = '/tdly/register/forgotPassword'      
jQuery.ajax({
type : 'POST',
url :  dataUrl ,
data : formdata,
success : function(response,textStatus)
{
//emptyForm();
   if(response.success)
   {
	alert('success');
	$('#dialog-forgotPassword').dialog('close');
	$('#dialog-forgotPasswordConfirm').dialog('open');
	
   }
   else
   {
	alert(response.error);
        $('#errorLoginMsg').html(response.error);
   }
},
error : function(
XMLHttpRequest,
textStatus,
errorThrown) {
}
});
}

function forgotPassword() {
$('#dialog-login').dialog('close');
$('#dialog-forgotPassword').dialog('open');
}
</script>
<g:render template="/company/signup" />
<g:render template="/login/auth" />
<g:render template="/login/forgotPassword" />
<div id="dialog-forgotPasswordConfirm" title="Password Reset Confirm">
        <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                Password Reset Email sent. 
        </p>
</div>

 <div class="mainbg">
   <div class="wrapper">
     
	 <!--header starts-->
	    <div class="header">
		<img src="images/logo.png" alt="" />
		<ul id="nav">
			<li><a href="#" title="Support"><span>Support</span></a></li>
			<li><a href="#" title="Pricing"><span>Pricing</span></a></li>
			<li><a href="#" id="login" title="Login"><span>Login</span></a></li>
			<li>
			<a href="#" title="Tour" class="top_link"><span>Tour</span></a>
			 <ul class="sub">
			           <li><a href="#">Sample 1</a></li>
						<li><a href="#">Sample 2</a></li>
						<li><a href="#">Sample 3</a></li>
		    </ul>  
			</li>
			<li><a href="#" id="signup" title="Signup"><span>Signup</span></a></li>
			<li><a href="#" title="Home" class="active"><span>Home</span></a></li>
		</ul>
		</div>
	 <!-- header ends-->
	 
  </div>
  </div> 
   
    
	<!-- bodypanel starts-->
<div class="lightshade">
        <div class="i-bodycontent">
                <!--i-bodycontentwrapper start -->
                <div class="i-bodycontentwrapper">


<div class="tablewrap">
<br/>
<br/>
<div class="eachtab" id="tab-1">
<table>
<h3>Error</h3>
<tr><td>&nbsp;<br></td></tr>
<tr><td>
	${flash.error}
</td></tr>
</table>
</div>
</div>

                </div>
                <!--i-bodycontentwrapper end -->
        </div>
</div>

	<!-- bodypanel ends-->
   
 <!-- footer starts-->
  <div class="footer">
    <div class="wrapper">
     <div class="foot">
	   <ul>
	       	<li class="link">
			 <div class="footdiv">
			   <h2>Links</h2>
			   <ul>
				<li><a href="#">Home</a></li>         
				<li><a href="#" id="signup" >Signup</a></li>          
				<li><a href="#">Tour</a></li>          
				<li><a href="#" id="login" >Login</a></li>                  
				<li><a href="#">Support</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="pricing">
			 <div class="footdiv">
			   <h2>Pricing</h2>
			   <ul>
				<li><a href="#">Donec in risus sed</a></li>         
				<li><a href="#">Magna bibendum</a></li>          
				<li><a href="#">Auctor nec non lectus</a></li>          
				<li><a href="#">Sed ut arcu a enim</a></li>                  
				<li><a href="#">Aliquam vulputate</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="tour">
			 <div class="footdiv">
			   <h2>Tour</h2>
			   <ul>
				<li><a href="#">Vivamus tincidunt tortor</a></li>         
				<li><a href="#">Quis est tincidunt</a></li>          
				<li><a href="#">Lacinia. Praesent qua</a></li>          
				<li><a href="#">Commodo ut hendrerit</a></li>                  
				<li><a href="#">Ultricies, pretium eget</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="sign">
			 <div class="footdiv">
			   <h2>Sign Up/ Login</h2>
			   <p>Aenean non neque pellentesque 
				purus tempor egestas a quis 
				massa. Duis feugiat.</p>
			 </div>
			 <div class="spacer"></div>
			 <a href="#" class="signup">Sign Up/ Login</a>
			</li>
			
			<li class="support">
			 <div class="footdiv">
			   <h2>Support</h2>
			   <table width="187" border="0" cellspacing="0" cellpadding="0" class="suptable">
				  <tr>
					<td align="left" valign="top" width="59"><strong>Address:</strong></td>
					<td align="left" valign="top" width="128"><span>Nullam scelerisque 
					   porta nisi, ultrices 
					   sollicitudin lorem.</span></td>
				  </tr>
				  <tr><td colspan="2" height="10"></td></tr>
				  <tr>
					<td align="left" valign="top"><strong>Call:</strong></td>
					<td align="left" valign="top"><span>+1 234 5678</span></td>
				  </tr>
				   <tr><td colspan="2" height="10"></td></tr>
				  <tr>
					<td align="left" valign="top"><strong>Email:</strong></td>
					<td align="left" valign="top"><span><a href="#">help@test56tyg.com</a></span></td>
				  </tr>
				</table>

			 </div>
			</li>
		</ul>  
	 </div>
	 <div class="spacer"></div>
	 <div class="copyright">
	   <ul>
	     <li class="noBdr">Copyright � Website. All Rights Reserved.</li>
		 <li><a href="#">Terms of Service</a></li>
		 <li><a href="#">Privacy Policy</a></li>
	   </ul>
	 </div>
	 </div>
  </div>
 <!-- footer ends-->
</body>
</html>
<script language="javascript">
</script>

