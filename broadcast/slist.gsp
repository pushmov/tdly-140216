<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Broadcast List</title>

    </head>
    <body> 
        <div class="body">
<script> 
var myWin;
var theform;
var bid;

Ext.onReady(function(){
Ext.BLANK_IMAGE_URL = 'images/s.gif'; 
Ext.QuickTips.init(); 

}); 

function showResponse(prm) {
      bid = prm;
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Multi Column, Nested Layouts and HTML editor',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	layout:'column',

	items:[{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'First Name',

	name: 'first',

	anchor:'95%'

	}]

	},{

	columnWidth:.5,

	layout: 'form',

	items: [{

	xtype:'textfield',

	fieldLabel: 'Last Name',

	name: 'last',

	anchor:'95%'

	},{

	xtype:'textfield',

	fieldLabel: 'Email',

	name: 'email',

	vtype:'email',

	anchor:'95%'

	}]

	}]

	},{

	xtype:'htmleditor',

	id:'responsetext',

	name:'responsetext',

	fieldLabel:'Comments',

	height:200,

	anchor:'98%'

	}],


	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/broadcastResponse/save?bid=' + bid, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}
</script>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
	        
		
%>

				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="broadcasttype"  params="${[searchterm: flash.searchterm]}"  title="Broadcasttype" />
                   	        <g:sortableColumn property="sortableDateCreated"  params="${[searchterm: flash.searchterm]}" title="Created" />
                   	        <g:sortableColumn property="part"  params="${[searchterm: flash.searchterm]}" title="Part" />
                   	        <g:sortableColumn property="mfgname"  params="${[searchterm: flash.searchterm]}" title="Manufacturer" />
                   	        <g:sortableColumn property="condition"  params="${[searchterm: flash.searchterm]}" title="Condition" />
                   	        <g:sortableColumn property="price"  params="${[searchterm: flash.searchterm]}" title="Price" />
                   	        <g:sortableColumn property="qty"  params="${[searchterm: flash.searchterm]}" title="quantity" />
                   	        <th>Description</th>
                   	        <g:sortableColumn property="companyName"  params="${[searchterm: flash.searchterm]}" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${broadcastList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcast, field:'broadcasttype')}</td>
                            <td>${fieldValue(bean:broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
                            <td>${broadcast.unit.manufacturer.mfgname}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast, field:'description')}</td>
                            <td>${fieldValue(bean:broadcast.company, field:'name')}</td>
				<g:if test="${broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>
                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                <g:paginate total="${flash.total}" params="${[searchterm:flash.searchterm]}"  />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
        </div>
    </body>
</html>
