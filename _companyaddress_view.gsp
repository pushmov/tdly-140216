                    
                        <div class="row" >
                            <td valign="top" class="name">Street1:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'street1')}</td>
                            
                        </div>
                    
                        <div class="row" >
                            <td valign="top" class="name">Street2:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'street2')}</td>
                            
                        </div>

                        <div class="row" >
                            <td valign="top" class="name">City:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'city')}</td>
                            
                        </div>

                        <div class="row" >
                            <td valign="top" class="name">State:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'state')}</td>
                            
                        </div>

                        <div class="row" >
                            <td valign="top" class="name">Postalcode:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'postalcode')}</td>
                            
                        </div>
                    

                        <div class="row" >
                            <td valign="top" class="name">Country:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'country.name')}</td>
                            
                        </div>

                        <div class="row" >
                            <td valign="top" class="name">Phone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'phone')}</td>
                            
                        </div>
                    

                    
                        <div class="row" >
                            <td valign="top" class="name">Fax:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:address, field:'fax')}</td>
                            
                        </div>
                    
                    
                    
