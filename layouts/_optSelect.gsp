<select name="${selectName}" id="${selectName}" multiple="multiple">
    <optgroup label="--"><option value="">(Select One)</option></optgroup>
    <g:each in="${dataMap}" var="data">
        <optgroup label="${data.key}">
            <g:each in="${data.value}" var="value">
                <option value="${value.id}" ${(selected?.contains(value.id)?"selected":"")} >${value.name}</option>
            </g:each>
        </optgroup>
    </g:each>
</select>