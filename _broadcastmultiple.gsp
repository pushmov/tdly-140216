<%@page import="test2.Manufacturer" %>

							<g:hasErrors bean="${broadcast}">
							<div class="errors">
								<g:renderErrors bean="${broadcast}" as="list" />
							</div>
							</g:hasErrors>
							<g:hasErrors bean="${broadcast?.unit}">
							<div class="errors">
								<g:renderErrors bean="${broadcast?.unit}" as="list" />
							</div>
							</g:hasErrors>
							
							<!--modal-header-->
              <g:form action="savemultiple" method="post" name="fmultiple" class="form-horizontal">
                <div class="modal-body">
									<div class="message-response"></div>
                  <div class="form-group">
                    <label for="m_p_type" class="col-sm-2 control-label">Type:</label>
                    <div class="col-sm-8">
											<g:select class="form-control" from="${['WTB','WTS']}" name="broadcasttype" value="${fieldValue(bean:broadcast,field:'broadcasttype')}"></g:select>
                    </div>
                  </div>
                  <!--form-group-->
                  <div class="form-group">
                    <label for="m_p_title" class="col-sm-2 control-label">Title:</label>
                    <div class="col-sm-8">
                      <input type="text" name="title" id="title" class="form-control" value="${broadcast?.title}">
                    </div>
                  </div>
                  <!--form-group-->
                  <div class="form-group">
                    <label for="m_p_des" class="col-sm-2 control-label">Description:</label>
                    <div class="col-sm-8">
                      <textarea class="form-control" id="fmultiple-editor-b" name="description-a" rows="5">${broadcast?.description}</textarea>
                    </div>
                  </div>
                  <!--form-group-->
                </div>
                <!--modal-body-->
								<input type="hidden" name="showCreateButton" value="false" id="showCreateButtonM" >
                <div class="modal-footer">
									<p class="ajax-resp" align="left"></p>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-8">
                        <div class="pull-left loader hide">
                            <img src="/tdly/images/ajax-loader.gif"> Loading...
                        </div>
                        <div class="pull-right">
                            <button type="button" id="submit-broadcast-multiple" class="btn btn-primary">Broadcasts</button>
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                        
                    </div>
                  </div>
                </div>
                <!--modal-footer-->
							</g:form>
							
                <button type="button" data-target="#modal_broadcast_success" data-toggle="modal"></button>
							<script src="${createLinkTo(dir:'assets',file:'partszap/js/editor.js')}"></script>
							<script>
								$(document).ready(function() {
									$('#fmultiple-editor-b').Editor();
									
									$('#submit-broadcast-multiple').click(function(){
										var form = $('#fmultiple');
                                                                                form.find('.loader').removeClass('hide');
										$('.ajax-resp').html('');
										
										
										$.ajax({
											type: 'POST',
											url: form.attr('action'),
											data: form.serialize() + "&description=" + $('#fmultiple .Editor-editor').html(),
											success: function(response){
                                                                                                form.find('.loader').addClass('hide');
												if(response == 'good'){
													setTimeout(function(){
                                                                                                                $('.ajax-resp').html('<div class="alert alert-success" role="alert">Broadcast Sent.</div>');
													}, 1000)
												} else {
													$('#createmultiple').html(response);
												}
											}
										});
									});
									
								});
							</script>