

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit VendorRequest</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">VendorRequest List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New VendorRequest</g:link></span>
        </div>
        <div class="body">
            <h1>Edit VendorRequest</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${vendorRequest}">
            <div class="errors">
                <g:renderErrors bean="${vendorRequest}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${vendorRequest?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${vendorRequest?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby">Createdby:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'createdby','errors')}">
                                    <input type="text" id="createdby" name="createdby" value="${fieldValue(bean:vendorRequest,field:'createdby')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdon">Createdon:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'createdon','errors')}">
                                    <input type="text" id="createdon" name="createdon" value="${fieldValue(bean:vendorRequest,field:'createdon')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="vendor">Vendor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'vendor','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="vendor.id" value="${vendorRequest?.vendor?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
