<%@page import="test2.MyConnection" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

	<script>

    		var vid;


	function addConnection(id) {


		
		var conn = new Ext.data.Connection();
		conn.request({
		    url: '../connectionRequest/acceptVendor?addToMyVendor=' + addToMyVendor + '&id=' + vid,
		    method: 'POST',
		    success: function(responseObject) {
		        //showHistoryDialog(responseObject.responseText);
				Ext.Msg.alert('Status', 'Done.');

		    },
		     failure: function() {
		         Ext.Msg.alert('Status', 'Unable to show history at this time. Please try again later.');
		     }
		});
		
	}


	</script>

</head>

				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

                   	        <g:sortableColumn property="name" title="Name" />
                   	        <g:sortableColumn property="name" title="Company" />
                   	        <g:sortableColumn property="country" title="Location" />

									<th><span>Date Created</span></th>
                   	        <g:sortableColumn property="message" title="Message" />
									
									<th>Actions</th>

								</tr>
								
                    <g:each in="${connectionRequestList}" status="i" var="connectionRequest">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
	                            <td>${fieldValue(bean:connectionRequest.vendor, field:'name')}</td>
	                            <td>${connectionRequest.vendor.address.city}, ${connectionRequest.vendor.address.state} - ${connectionRequest.person.company.address.country}</td>
				    <td>${sdf.format(connectionRequest.dateCreated)}</td>
        	                    <td>${fieldValue(bean:connectionRequest, field:'message')}</td>
				<td><a href="javascript:void(0)" onClick="addConnection('${connectionRequest.id}')" id="showResp">Accept</a>  
				
	<div id="bresp"></div>
</td>
                        </tr>
                    </g:each>
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>

			                <g:paginate total="${ConnectionRequest.countByContact(session.person)}" />

						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
			</div>
			<!--[if !IE]>end page<![endif]-->
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
