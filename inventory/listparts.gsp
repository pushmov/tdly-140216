<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

<script>
    var yesHandler = function(o) {
        alert('You clicked "Yes"');
        this.cancel();
    }

var myWin;
var theform;
var bid;

Ext.onReady(function(){
Ext.BLANK_IMAGE_URL = 'images/s.gif'; 
Ext.QuickTips.init(); 

}); 
var genres = new Ext.data.SimpleStore({ 
fields: ['broadcasttype', 'displaytype'], 
data : [['WTB','WTB'],['WTS','WTS']]
});

function showResponse(part,manufacturer,condition,price,quantity,mfgid) {
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Send Broadcast',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	xtype:'textfield',

	fieldLabel: 'Part',

	name: 'part',

      value: part,

	anchor:'95%'

	}
      ,{

	xtype:'combo',

	fieldLabel: 'Broadcast Type',

	name: 'broadcasttype',

mode: 'local', 
store: genres, 
	displayField:'displaytype', 

	anchor:'95%'

	}
	,{


	fieldLabel: 'Manufacturer : ' + manufacturer,

	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Condition',

	name: 'condition',
	value: condition,
	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Price',

	name: 'price',
	value: price,

	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Quantity',

	name: 'qty',
	value: quantity,

	anchor:'95%'

	}
	,{

	xtype:'textarea',

	fieldLabel: 'Description',

	name: 'description',

	anchor:'95%'

	}
	,{

	xtype:'checkbox',

	fieldLabel: 'Send to MyVendors Only',

	name: 'myvendor',

	anchor:'95%'

	}

      
	]
	,

	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/broadcast/save?manufacturer=' + mfgid, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin = new Ext.Window({ // 2 
id : 'myWin', 
modal : true , 
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin.show();
}


function editInv(invid,part,manufacturer,condition,price,quantity,mfgid) {
	theform=new Ext.FormPanel({

	labelAlign: 'top',

	frame:true,

	title: 'Send Broadcast',

	bodyStyle:'padding:5px 5px 0',

	width: 600,

	items: [{

	xtype:'textfield',

	fieldLabel: 'Part',

	name: 'part',

      value: part,

	anchor:'95%'

	}
      ,{

	xtype:'combo',

	fieldLabel: 'Broadcast Type',

	name: 'broadcasttype',

mode: 'local', 
store: genres, 
	displayField:'displaytype', 

	anchor:'95%'

	}
	,{


	fieldLabel: 'Manufacturer : ' + manufacturer,

	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Condition',

	name: 'condition',
	value: condition,
	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Price',

	name: 'price',
	value: price,

	anchor:'95%'

	}
	,{

	xtype:'textfield',

	fieldLabel: 'Quantity',

	name: 'qty',
	value: quantity,

	anchor:'95%'

	}
	,{

	xtype:'textarea',

	fieldLabel: 'Description',

	name: 'description',

	anchor:'95%'

	}
	,{

	xtype:'checkbox',

	fieldLabel: 'Send to MyVendors Only',

	name: 'myvendor',

	anchor:'95%'

	}

      
	]
	,

	buttons: [{ 
text: 'Save', 
handler: function(){ 
theform.getForm().submit({ 
url:'/test2/inventory/update?id=' + invid + '&mfgid=' + mfgid, 
			waitMsg:'Saving Data...',

success: function(f,a){ 
Ext.Msg.alert('Success', a.result.msg); 
myWin2.close();
}, 
failure: function(f,a){ 
Ext.Msg.alert('Warning', 'Error'); 
} 
}); 
}
}, { 
text: 'Reset', 
handler: function(){ 
theform.getForm().reset(); 
}
}]
	});




myWin2 = new Ext.Window({ // 2 
id : 'myWin2', 
modal : true , 
height : 500, 
width : 700, 
items : [ 
theform 
] 
}); 
myWin2.show();
}


</script>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.company.timezone));
	        
		
%>

</head>

				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link  id="selected_search_tab" url="[action:'list']"   ><span><span>List</span></span></g:link></li>
								<li><g:link   url="[action:'create']"   ><span><span>Add</span></span></g:link></li>
								<li><g:link   url="[action:'searchinit']"   ><span><span>Search</span></span></g:link></li>
								<li><g:link   url="[action:'initupload']"   ><span><span>Upload</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						<div id="product_list_menu">
							<!--[if !IE]>start product list tabs<![endif]-->
							<ul id="product_list_tabs">
								<li><a href="#" class="selected"><span><span>Featured</span></span></a></li>
								<li><a href="#"><span><span>All products</span></span></a></li>
								<li><a href="#"><span><span>Best Sellers</span></span></a></li>
							</ul>
							<!--[if !IE]>end product list tabs<![endif]-->
							<a href="#" class="update"><span><span><em>Add New Record</em><strong></strong></span></a>
						</div>
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="item.part" title="Part" />
                   	        <g:sortableColumn property="item.manufacturer" title="Mfg" />
                   	        <g:sortableColumn property="item.condition" title="Condition" />
                   	        <g:sortableColumn property="item.price" title="Price" />
                   	        <g:sortableColumn property="item.qty" title="Qty" />
                   	        <g:sortableColumn property="visible" title="Visible" />

<!--                        
                   	        <g:sortableColumn property="myvendorsonly" title="Myvendorsonly" />
                        
                    	        <g:sortableColumn property="searches" title="Searches" />
                        
                   	        <g:sortableColumn property="hits" title="Hits" />
                        
                   	        <g:sortableColumn property="wtb" title="Wtb" />

  -->
									
									<th>Actions</th>
								</tr>
								
                    <g:each in="${inventoryList}" status="i" var="inventory">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                            <td>${sdf.format(inventory.dateCreated)}</td>
                            <td id="part${i}" >${fieldValue(bean:inventory.item, field:'part')}</td>
                            <td id="${fieldValue(bean:inventory.item.manufacturer, field:'id')}" >${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'condition')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'price')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'qty')}</td>
                        
                            <td>${fieldValue(bean:inventory, field:'visible')}</td>
<!--                        
                        
                            <td>${fieldValue(bean:inventory, field:'myvendorsonly')}</td>
                            <td>${fieldValue(bean:inventory, field:'searches')}</td>
                        
                            <td>${fieldValue(bean:inventory, field:'hits')}</td>
                        
                            <td>${fieldValue(bean:inventory, field:'wtb')}</td>

  -->
			    
									<td>
										<div class="actions_menu">
											<ul>
												<li><a href="javascript:void(0)" class="details" id="respond" onClick="javascript:showResponse('${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition')}','${fieldValue(bean:inventory.item, field:'price')}','${fieldValue(bean:inventory.item, field:'qty')}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >Broadcast</a></li>
												<li><a href="javascript:void(0)" class="edit" id="respond" onClick="javascript:editInv('${fieldValue(bean:inventory, field:'id')}','${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition')}','${fieldValue(bean:inventory.item, field:'price')}','${fieldValue(bean:inventory.item, field:'qty')}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >Edit</a></li>
												<li><g:remoteLink class="delete" action="delete" id="${inventory?.id}" onclick="return confirm('Are you sure?');" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

			 

                    <input type="hidden" name="id" value="${inventory?.id}" />
				

			    </td>

                        
                        </tr>
                    </g:each>
								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						<g:paginate total="${Inventory.count()}" />
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
