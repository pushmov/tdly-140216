<%@ page import="test2.BroadcastFilter" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Country" %>
   
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="pz" />

<script language="Javascript" >

	var prevSelect = null;

$(document).ready(function() {

  $('#country').multiselect();

  $('#dialog-filter').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            'Create a Reminder': function() {
		$('#broadcastFilterForm').submit();              
                $(this).dialog('close');
            },
            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });



	      $('#broadcastFilterForm').submit( function() {
    
                            alert('frm submitted');
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFilter/save",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
					location.reload = true;
				   //$('#dialog-responseSave').dialog("open");
    			    }
			    
                            });

        	            return false;

              });



  $("#add_reminder").click(function() {
	$('#dialog-filter').dialog('open');
    });


  $("select#continents").change(function(){
      $("#countrySelect").load("../continent/updateSelectFilter",{continentid: $(this).val(),prevcontinentid : prevSelect ,countryid: $("#countries").val(), domain: 'broadcastFilter' , ajax: 'true'});
	prevSelect = $(this).val();
  });




        });


  
 function getFilter(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {
		
			alert($.toJSON(data));
			//alert('filterid : ' + data.id);

			$('#id').val(data.id);
			$('#name').val(data.name);
			$('#keyword').val(data.keyword);
			$('#partlist').val(data.partlist);

			if(data.myVendors) {
				$('input[name="companyType"][value="myVendors"]').attr('checked', true);
			}

			if (data.broadcasttype != null) {
				var btArr = data.broadcasttype.split(',');
			
				if( $.inArray('WTB',btArr) != -1) {
					$('#wtb').attr('checked', true);
				}
				else {
					$('#wtb').attr('checked', false);
				}
			
				if( $.inArray('WTS',btArr) != -1) {
					$('#wts').attr('checked', true);
				}
				else {
					$('#wts').attr('checked', false);
				}
			}

			if (data.condition != null) {
				var conditionArr = data.condition.split(','); 
	
				if( $.inArray('New',conditionArr) != -1) {
					$('input[name="conditionS"][value="New"]').attr('checked', true);
				}

				if( $.inArray('Used',conditionArr) != -1) {
					$('input[name="conditionS"][value="Used"]').attr('checked', true);
				}

				if( $.inArray('Refurbished',conditionArr) != -1) {
					$('input[name="conditionS"][value="Refurbished"]').attr('checked', true);
				}
			}


			if (data.myCountries) {
				$('input[name="countryType"][value="myCountries"]').attr('checked', true);
			}

			if (data.myConnections) {
				$('input[name="companyType"][value="myConnections"]').attr('checked', true);
			}
		
			if (data.myVendors) {
				$('input[name="companyType"][value="myVendors"]').attr('checked', true);
			}

			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
			}
				
			$("#manufacturer").val(mfgArr)

			var companyArr = new Array(data.companies.length);	
			for(i=0;i<data.companies.length;i++) {
				companyArr[i] = data.companies[i].id;
			}
				
			$("#companies").val(companyArr);

			var countryArr = new Array(data.countries.length);	
			for(i=0;i<data.countries.length;i++) {
				countryArr[i] = data.countries[i].id + '';
				alert(data.countries[i].id);
			}
				
			$("#country").val(countryArr);
			
		


			//$('#dialog-form').binddata(data);
			$('#dialog-filter').dialog('open');
			

                }
             );
	
 }


</script>

    </head>
<div id="dialog-responseSave" title="Broadcast Response Sent"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast/Alert Filter Saved.
	</p> 
</div>

<g:render template="/filterDialog"  />


        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><a href="#" id="add_reminder" ><g:message code="default.new.label" args="[entityName]" /></a></span>
        </div>
<div class="tablewrap">
<br/>
<br/>
<div class="eachtab" id="tab-1">

<table width="100%" border="0" cellspacing="1" cellpadding="0"  id="rounded-corner"  >
<thead>                        <tr>

                            <g:sortableColumn property="name" title="${message(code: 'broadcastFilter.name.label', default: 'Name')}" />
                            <g:sortableColumn property="broadcasttype" title="${message(code: 'broadcastFilter.broadcasttype.label', default: 'Broadcast Type')}" />
                            <g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.keyword.label', default: 'Keywords')}" />
                            <g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.partlist.label', default: 'Partno / Partlist')}" />
                            <g:sortableColumn property="keyword" title="${message(code: 'broadcastFilter.condition.label', default: 'Condition')}" />
                            <th>Manufacturers</th>
                            <th>Companies</th>
                            <th>Countries</th>
									<th>Actions</th>

                        
                        </tr>
</thead>
 <tfoot>
    </tfoot>
<tbody>
                    <g:each in="${broadcastFilterList}" status="i" var="broadcastFilterInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td>${fieldValue(bean: broadcastFilterInstance, field: "name")}</td>
                            <td>${broadcastFilterInstance.broadcasttype}</td>
                            <td>${fieldValue(bean: broadcastFilterInstance, field: "keyword")}</td>
                            <td>${broadcastFilterInstance.partno?broadcastFilterInstance.partno:broadcastFilterInstance.partlist}</td>
                            <td>${broadcastFilterInstance.condition}</td>
<%
	def manufacturers = new StringBuffer()
	def manufacturer = (broadcastFilterInstance.manufacturers.size() == 0)?"All":"list"
	if (manufacturer == "list") {
		
		if (!(broadcastFilterInstance.manufacturers instanceof String)) {

			broadcastFilterInstance.manufacturers.each {
				manufacturers << it.mfgname + ", "
			}

		}
		else {
			manufacturers = broadcastFilterInstance.manufacturers
		}
	}
		
%>


                            <td>${manufacturers}</td>

<%
	def companies = new StringBuffer()
	def company
	if (broadcastFilterInstance.myVendors) {
		company = "My Vendors"
	}
	else if (broadcastFilterInstance.myConnections) {
		company = "My Connections"
	}
	else if (broadcastFilterInstance.companies.size() == 0) {
		company = "All"
	}
	else {
		company = "list"
	}

	if (company == "list") {
		
		if (!(broadcastFilterInstance.companies instanceof String)) {

			broadcastFilterInstance.companies.each {
				companies << it.name + ", "
			}

		}
		else {
			companies = broadcastFilterInstance.companies
		}
	}
	else {
		companies = company
	}
		
%>


                            <td>${companies}</td>
<%
	def countries = new StringBuffer()
	def country = broadcastFilterInstance.myCountries?"My Countries":((broadcastFilterInstance.countries.size() == 0)?"All":"list")
	if (country == "list") {
		
		if (!(broadcastFilterInstance.countries instanceof String)) {

			broadcastFilterInstance.countries.each {
				countries << it.name + ", "
			}

		}
		else {
			countries = broadcastFilterInstance.countries
		}
	}
	else {
		countries = country
	}
		
%>
                            <td>${countries}</td>
									<td>
										<div class="actions_menu">
											<ul>
												<li><a href="javascript:void(0)" class="edit" id="respond" onClick="getFilter(${broadcastFilterInstance.id});" >Edit</a></li>
												<li><g:remoteLink class="delete" action="delete" id="${broadcastFilterInstance?.id}" before="return confirm('Are you sure?');" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>


                        
                        </tr>
                    </g:each>
                    </tbody>
							
								
							</table>
	<div class="pagination">
                <g:paginate total="${BroadcastFilter.count()}" />
					</div>

		</div>
			<!--inner-contrightpan end -->
</div>
		
