<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<div class="table-responsive">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<util:remoteSortableColumn  action="myVendor"  property="bc.dateCreated" title="Date"  update="tabs-4"   />
				<util:remoteSortableColumn  action="myVendor" property="bc.broadcasttype" title="Broadcast Type"  update="tabs-4" />
				<util:remoteSortableColumn  action="myVendor"  property="bc.unit.part" title="Part"  update="tabs-4"  />
				<util:remoteSortableColumn  action="myVendor"  property="bc.unit.manufacturer" title="Manufacturer"  update="tabs-4"  />
				<util:remoteSortableColumn  action="myVendor"  property="bc.unit.condition" title="Condition"  update="tabs-4"  />
				<util:remoteSortableColumn  action="myVendor"  property="bc.unit.price" title="Price"  update="tabs-4"  />
				<util:remoteSortableColumn  action="myVendor"  property="bc.unit.qty" title="Quantity"  update="tabs-4"  />
				<th>Description</th>
				<util:remoteSortableColumn  action="myVendor"   property="bc.company" title="Company"  update="tabs-4"   />
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<g:render template="/alert/alertRow" />
		</tbody>
	</table>
</div>
<div class="pagination">
	<util:remotePaginate action="myVendor" total="${broadcastTotal}" update="tabs-4" />
</div>
