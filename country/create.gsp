

<%@ page import="test2.Country" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'country.label', default: 'Country')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${countryInstance}">
            <div class="errors">
                <g:renderErrors bean="${countryInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="country.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${countryInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="continent"><g:message code="country.continent.label" default="Continent" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'continent', 'errors')}">
                                    <g:select name="continent.id" from="${test2.Continent.list()}" optionKey="id" value="${countryInstance?.continent?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fips104"><g:message code="country.fips104.label" default="Fips104" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'fips104', 'errors')}">
                                    <g:textField name="fips104" value="${countryInstance?.fips104}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="iso2"><g:message code="country.iso2.label" default="Iso2" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'iso2', 'errors')}">
                                    <g:textField name="iso2" value="${countryInstance?.iso2}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="iso3"><g:message code="country.iso3.label" default="Iso3" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'iso3', 'errors')}">
                                    <g:textField name="iso3" value="${countryInstance?.iso3}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="ison"><g:message code="country.ison.label" default="Ison" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'ison', 'errors')}">
                                    <g:textField name="ison" value="${countryInstance?.ison}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="internet"><g:message code="country.internet.label" default="Internet" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'internet', 'errors')}">
                                    <g:textField name="internet" value="${countryInstance?.internet}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="capital"><g:message code="country.capital.label" default="Capital" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'capital', 'errors')}">
                                    <g:textField name="capital" value="${countryInstance?.capital}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="mapreference"><g:message code="country.mapreference.label" default="Mapreference" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'mapreference', 'errors')}">
                                    <g:textField name="mapreference" value="${countryInstance?.mapreference}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nationalitysingular"><g:message code="country.nationalitysingular.label" default="Nationalitysingular" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'nationalitysingular', 'errors')}">
                                    <g:textField name="nationalitysingular" value="${countryInstance?.nationalitysingular}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nationalityplural"><g:message code="country.nationalityplural.label" default="Nationalityplural" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'nationalityplural', 'errors')}">
                                    <g:textField name="nationalityplural" value="${countryInstance?.nationalityplural}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="currency"><g:message code="country.currency.label" default="Currency" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'currency', 'errors')}">
                                    <g:textField name="currency" value="${countryInstance?.currency}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="currencycode"><g:message code="country.currencycode.label" default="Currencycode" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'currencycode', 'errors')}">
                                    <g:textField name="currencycode" value="${countryInstance?.currencycode}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="population"><g:message code="country.population.label" default="Population" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'population', 'errors')}">
                                    <g:textField name="population" value="${countryInstance?.population}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="title"><g:message code="country.title.label" default="Title" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'title', 'errors')}">
                                    <g:textField name="title" value="${countryInstance?.title}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comment"><g:message code="country.comment.label" default="Comment" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: countryInstance, field: 'comment', 'errors')}">
                                    <g:textField name="comment" value="${countryInstance?.comment}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
