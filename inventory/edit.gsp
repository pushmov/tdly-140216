

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Inventory</title>
    </head>
    <body>
        <div class="body">
            <h1>Edit Inventory</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${inventory}">
            <div class="errors">
                <g:renderErrors bean="${inventory}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="update" method="post" >
                <input type="hidden" name="id" value="${inventory?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>

				    <g:render template="/tradeunit" var="tradeunit" bean="${inventory.item}" />
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="visible">Visible:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:inventory,field:'visible','errors')}">
                                    <g:checkBox name="visible" value="${inventory?.visible}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="myvendorsonly">Myvendorsonly:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:inventory,field:'myvendorsonly','errors')}">
                                    <g:checkBox name="myvendorsonly" value="${inventory?.myvendorsonly}" ></g:checkBox>
                                </td>
                            </tr>                         
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
