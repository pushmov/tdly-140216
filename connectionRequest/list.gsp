<%@page import="test2.ConnectionRequest" %>
  <meta name="layout" content="pz" />

<r:script language="Javascript" >
$(document).ready(function() {

  $('#dialog-connectionRequest').dialog({
                autoOpen: false,
                height: 250,
                width: 350,
                modal: true,
                buttons: {
                            Ok: function() {
                                $(this).dialog('close');
                            }

                }        
             });	

});

function acceptConnection(id, element) {
                

                            $.ajax({
                            type: "POST",
                            url: "../myConnection/acceptConnection",
                            cache: false,
                            data: { id : id  },
                            error: function(data, textStatus, jqXHR) {
                                   $('#connectionRequestMessage').html(data.msg);
                                   $('#dialog-connectionRequest').dialog("open");
                                 
                            },
                            success: function(data, textStatus, jqXHR) {
					location.reload();
                                   //$('#connectionRequestMessage').html(data.msg);
                                   //$('#dialog-connectionRequest').dialog("open");
                            }
                            
                            });

                
        }

function deleteConnectionRequest(id,element) {


                            $.ajax({
                            type: "POST",
                            url: "${request.contextPath}/connectionRequest/delete",
                            cache: false,
                            data: { id : id },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
					location.reload();
                            }

                            });



}
</r:script>

<div id="dialog-connectionRequest" title="MyConnection"> 
        <p> 
                 <div id="connectionRequestMessage">hello</div> 
        </p> 
</div>

<div class="tablewrap">
<div class="eachtab" id="tab-1">
<g:render template="/connectionRequest/connectionRequestList" model="[connectionRequestInstanceList:connectionRequestInstanceList,messageType:messageType,connectionRequestInstanceTotal:connectionRequestInstanceTotal]" />
		</div>
</div>
