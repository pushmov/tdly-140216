<div class="modal fade broadcasts-popup" id="modal_reset_password" tabindex="-1" role="dialog" aria-labelledby="modal_reset_password_label" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_reset_password_label">Reset Password</h4>
              </div>
              <!--modal-header-->
              <div class="modal-body">
                    <g:form action="resetPassword" name="resetPasswordForm" method="post" class="form-horizontal">
		<input type="hidden" class="text"  id="person.id" name="id" value="${fieldValue(bean:person,field:'id')}" />
		<input type="hidden" name="returnTemplate" value="" id="returnTemplate" />
<g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
                <div class="form-group">
                    <label for="m_p_type" class="col-sm-2 control-label">New Password:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="passwd" value=""/>
                        <g:renderErrorsCap bean="${person}" as="list" field="passwd"  />
                    </div>
                </div>
                <div class="form-group">
                    <label for="m_p_type" class="col-sm-2 control-label">Re-Enter New Password:</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="passwd2" value=""/>
                        <g:renderErrorsCap bean="${person}" as="list" field="passwd2"  />
                    </div>
		</div>
</g:form>
              </div>
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
</div>