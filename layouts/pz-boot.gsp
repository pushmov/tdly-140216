	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>test</title>
	<g:javascript library="jquery" plugin="jquery" /> 



    <!-- MASTER STYLESHEET-->
        <link rel="stylesheet" href="${createLinkTo(dir:'bootstrap',file:'css/bootstrap.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'bootstrap',file:'css/bootstrap-responsive.css')}" />
	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'saas/grid.css')}"  />

    


    
    
          
    <![endif]-->

	

        <g:layoutHead />
	<jqui:resources/>
        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />
        <jqvalui:renderValidationScript for="test2.BroadcastFilter"  />	
	<g:javascript library="pz/tiny_mce/tiny_mce"  />
	<g:javascript library="pz/tiny_mce/jquery.tinymce"  />

        <g:javascript library="pz/jquery.databind" />				
        <g:javascript library="pz/jquery.json-2.2" />				
        <g:javascript library="pz/jquery.populate" />				
        <g:javascript library="pz/populate-page-functions" />				
        <g:javascript library="pz/jquery.parseQuery" />	
        <g:javascript library="pz/jquery.multiselect" />				
        <g:javascript library="bootstrap" />				
			

	  <tooltip:resources/>	




<script language="Javascript" >

	$(document).ready(function() {

		$( "#tabs" ).tabs();

    $('#mainsearch-message').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });


/*
             $('#mainsearchform').submit( function() {

                            
			    $.ajax({
    			    type: "POST",
    			    url: "../search/search",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    }
			    
                            });

		    
        	            return false;

              });
*/

              $('#mainsearch').click( function() {
			var data = $("#mainsearchform").serialize();
			var qstr = $.parseQuery(data);

			if (qstr.searchtype == "Parts") {
				$("#mainsearchform").attr("action","../search/searchparts");
			}
			if (qstr.searchtype == "Broadcasts") {
				$("#mainsearchform").attr("action","../broadcast/search");
			}
			if (qstr.searchtype == "Inventory") {
				$("#mainsearchform").attr("action","../inventory/search");
			}
			if (qstr.searchtype == "Company") {
				$("#mainsearchform").attr("action","../myVendor/searchcompany");
			}
			if (qstr.searchtype == "People") {
				$("#mainsearchform").attr("action","../person/searchpeople");
			}


    			$("#mainsearchform").submit();
			
              });


	})

</script>

		

</head>

<body>
<div id="mainsearch-message" title="Broadcast Created"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Searching......
	</p> 
</div> 


<div id="header" class="">

//    <div id="head_wrap" class="container_12">
    
/*        <div id="logo" class="grid_4">
          <h1></h1>
        </div>
*/
    	<div id="controlpanel" class="">
        
            <ul>
            
    			<li><p>Signed in as <strong>Johnathan Doe</strong></p></li>
                <li><a href="#" class="first">Settings</a></li>
                <li><a href="../logout/" class="last">Sign Out</a></li>
                
            </ul>
            
        </div>

      	<div id="navigation" class=" ">
           
            <ul>
            
<!--                <li><a href="#" >Dashboard</a></li> -->
		<li><a href="../broadcast/list" <g:if test="${(params.controller == 'broadcast') || (params.controller == 'broadcastFilter')}">class="active"</g:if>  ><span><span>Broadcasts</span></span></a></li>
		<li><a href="../inventory/list?_HDIV_STATE_=SSS" <g:if test="${params.controller == 'inventory'}">class="active"</g:if>  ><span><span>Inventory</span></span></a></li>
		<li><a href="../alert/list" <g:if test="${params.controller == 'alert'}">class="active"</g:if>  ><span><span>Alerts</span></span></a></li>
		<li><a href="../broadcastResponse/bcastresponse" <g:if test="${params.controller == 'broadcastResponse'}">class="active"</g:if>  ><span><span>Inbox</span></span></a></li>
		<li><a href="../myActivity/wtb" <g:if test="${params.controller == 'myActivity'}">class="active"</g:if>  ><span><span>My Activity</span></span></a></li>
		<li><a href="../myVendor/list" <g:if test="${params.controller == 'myVendor'}">class="active"</g:if>  ><span><span>My Vendors</span></span></a></li>
		<li><a href="../person/view"  <g:if test="${((params.controller == 'company') || (params.controller == 'person'))}">class="active"</g:if>  ><span><span>Account</span></span></a></li>
<!--		<li><a href="../preferences/wtb" <g:if test="${params.controller == 'preferences'}">class="active"</g:if>  ><span><span>Preferences</span></span></a></li>   -->
                <li><g:form name="mainsearchform" controller="search" action="search" method="post" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<g:select from="${['Parts','Broadcasts','Inventory','Company','People']}" name="searchtype" value="" ></g:select><input name="searchterm" value=""><input id="mainsearch" value="Search" type="button" /></g:form>
</li>
    
            </ul>

          
        </div>
     
    </div>
</div>
 
<!--
            <ul>
            
                <li><a href="#" >Dashboard</a></li>
		<li><a href="../broadcast/list" <g:if test="${params.controller == 'broadcast'}">class="active"</g:if>  ><span><span>Broadcasts</span></span></a></li>
		<li><a href="../inventory/list?_HDIV_STATE_=SSS" <g:if test="${params.controller == 'inventory'}">class="active"</g:if>  ><span><span>Inventory</span></span></a></li>
		<li><a href="../alert/list" <g:if test="${params.controller == 'alert'}">class="active"</g:if>  ><span><span>Alerts</span></span></a></li>
		<li><a href="../broadcastResponse/bcastresponse" <g:if test="${params.controller == 'broadcastResponse'}">class="active"</g:if>  ><span><span>Inbox</span></span></a></li>
		<li><a href="../myActivity/wtb" <g:if test="${params.controller == 'myActivity'}">class="active"</g:if>  ><span><span>My Activity</span></span></a></li>
		<li><a href="../myVendor/list" <g:if test="${params.controller == 'myVendor'}">class="active"</g:if>  ><span><span>My Vendors</span></span></a></li>
		<li><a href="..//show"  <g:if test="${((params.controller == 'company') || (params.controller == 'person'))}">class="active"</g:if>  ><span><span>Company</span></span></a></li>
		<li><a href="../preferences/wtb" <g:if test="${params.controller == 'preferences'}">class="active"</g:if>  ><span><span>Preferences</span></span></a></li>
                
    
            </ul>
 -->

<!-- staqrt subnav -->
<div id="sub_nav">

<div id="subnav_wrap" class="container_12">
	
    <!-- start sub nav list -->
	<div id="subnav" class=" grid_12">

	    <g:render template="/${params.controller}submenu"  />
    
        </div>
    <!-- end subnavigation list -->	

</div>

</div>
<!-- end sub_nav -->

<!-- EVERYTING BELOW IS THE MAIN CONTENT -->

<div id="main_content_wrap" class="container_12">





					<g:layoutBody />

</div>

<div class="clearfix">&nbsp;</div>
<div class="container_12">
     


   <!-- START FOOTER -->

    
    <div id="footer" class="grid_12">
    
        <p>&copy; Copyright 2010 Your Company | Powered by <a href="#">Reality Admin</a> | <a href="#">Top</a></p>
        
	</div>
    <!-- END FOOTER -->       
  </div><!-- end content wrap -->


</body>
</html>
