 
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>MyActivity - Private Broadcasts</title>


    </head>
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                        
                   	        <g:sortableColumn property="dateCreated" title="Created" />
                   	        <g:sortableColumn property="receiver.company.name" title="Sent To" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" /> 
                   	        <g:sortableColumn property="broadcast.unit.manufacturer.mfgname" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="quantity" />
                   	        <g:sortableColumn property="responsetext" title="Description" />
                        
                        
                        </tr>
                    <g:each in="${broadcastResponseList}" status="i" var="resp">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:resp, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:resp.broadcast.company, field:'name')}</td>
                            <td>${fieldValue(bean:resp.broadcast, field:'unit.part')}</td>
                            <td>${fieldValue(bean:resp.broadcast, field:'unit.manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:resp.broadcast, field:'unit.condition')}</td>
                            <td>${fieldValue(bean:resp.broadcast, field:'unit.price')}</td>
                            <td>${fieldValue(bean:resp.broadcast, field:'unit.qty')}</td>
                            <td>${fieldValue(bean:resp, field:'responsetext')}</td>
                        </tr>
                    </g:each>

							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
                                 <g:paginate controller="myActivity" action="wtbresponds" total="${wtbrTotal}"  params="[activetab: 0]" />
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->


</html>

