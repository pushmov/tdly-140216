<tr id="${person.id}" >
	<td>${fieldValue(bean:person, field:'firstname')}</td>
	<td>${fieldValue(bean:person, field:'lastname')}</td>
	<td><a href="view?id=${person.id}">${fieldValue(bean:person, field:'email')}</a></td>
	<td>${fieldValue(bean:person, field:'officephone')}</td>
	<td>${fieldValue(bean:person, field:'cellphone')}</td>
	<td>${fieldValue(bean:person.role, field:'authority')}</td>
	<td><g:isHigherRole person="${person}">
		<table class="regular_table" >
			<tr>
				<td>
					<a href="javascript:void(0)" class="details edit-detail" id="respond" data-value="${person.id}" ><i class="fa fa-pencil-square-o fa-lg"></i>&nbsp; Details/Edit</a>
				</td>
				<td>
					<a href="javascript:void(0)"  id="respond" onClick="javascript:resetPassword('${person.id}');" ><i class="fa fa-key fa-lg"></i>&nbsp; Reset Password</a>
				</td>
			</tr>
		</table>
		</g:isHigherRole>
	</td>
</tr>