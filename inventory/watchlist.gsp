<%@page import="test2.Inventory" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Watchlist List</title>
				<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

    </head>

<main class="content" role="main">
	<div class="main-content">
		<div class="table-responsive">
			<table class="table table-bordered gray-header responsive">
				<thead>
					<tr>
						<g:sortableColumn property="part.partno" title="Part" />
						<g:sortableColumn property="part.manufacturer" title="Mfg" />
						<th>Searches</th>
						<th>Wtb</th>
						<th>Min Price </th>
						<th>Max Price </th>
						<th>Avg Price </th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<g:each in="${watchList}" status="i" var="item">
					<tr id="row-${item?.id}" class="${(i % 2) == 0 ? 'first' : 'second'}">
						<td id="part${i}" >${fieldValue(bean:item.part, field:'partno')}</td>
						<td id="${fieldValue(bean:item.part, field:'manufacturer.mfgname')}" >${fieldValue(bean:item.part, field:'manufacturer.mfgname')}</td>
						<td id="${i}" >${fieldValue(bean:item.part, field:'searches')}</td>
						<td id="${i}" >${fieldValue(bean:item.part, field:'wtb')}</td>
						<td id="${i}" >${fieldValue(bean:item.part, field:'pricelow')}</td>
						<td id="${i}" >${fieldValue(bean:item.part, field:'pricehigh')}</td>
						<td id="${i}" >${fieldValue(bean:item.part, field:'priceavg')}</td>
						<td>
							<div class="action-control">
								<a href="#" class="delete" data-toggle="modal" data-target="#confirm-delete" data-id="${item?.id}">
									<i class="fa fa-trash-o fa-lg"></i>Delete
								</a>
							</div>
						</td>
					</tr>
					<input type="hidden" name="id" value="${inventory?.id}" />
					</g:each>
				</tbody>
	
			</table>
		</div>
	</div>
</main>

<div id="dialog-confirm" title="Delete this watchlist item ?">
  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Delete this watchlist item ?</p>
</div>

<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Delete this watchlist item ?</div>
			<div class="modal-body">
				Delete this watchlist item ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				<a data-id="" class="btn btn-danger btn-ok">Delete</a>
			</div>
        </div>
    </div>
</div>


<div class="modal fade broadcasts-popup" id="del_watchlist_success" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Success</h4>
            </div>
            <!--modal-header-->
            <div class="modal-body">
                <div id="createsingle" class="form-content">
                    Watchlist deleted
                </div>
            </div>
        </div>
        <!--modal-content-->
    </div>
    <!--modal-dialog-->
</div>

<button data-toggle="modal" data-target="#del_watchlist_success" style="display:none"></button>
<script>
	
	$(document).ready(function(){
		
		$('#dialog-confirm').dialog({
				autoOpen: false,
				modal:true,
				dialogClass:'delInventory'
		});
		
		$('a.delete').click(function(){
			$('.btn-danger').attr('data-id', $(this).attr('data-id'));
		});
		
		$('.btn-danger').click(function(){
                    var id = $(this).attr('data-id');
			jQuery.ajax({
				type:'POST', 
				url:'../watchlist/delete/' + id,
				success:function(data,textStatus){
                                    $('button[data-dismiss="modal"]').click();
                                    $('tr#row-' + id).remove();
                                    $('button[data-target="#del_watchlist_success"]').click();
                                
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
		});
		
	});
</script>