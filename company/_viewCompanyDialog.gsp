<g:render template="/vendor"  />
<%@page import="test2.MyConnection" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>
<%@page import="com.ucbl.util.PZUtil" %>
<div class="container">
	<g:form action="edit" method="post" class="search_form form-horizontal" >
		<g:render template="viewCompany" var="company" bean="${company}" />	
	</g:form>
	
	<g:each in="${emplist}" status="i" var="person">
	<div class="row row-item clearfix">
		<div class="col-sm-12 col-md-1 col-lg-1">
			<div class="icon-img"><img src="images/icon_img.jpg"></div>
		</div>
		<div class="col-sm-12 col-md-4 col-lg-4">
			<div class="con_m_heading"><g:link controller="person" action="profile" id="${person.id}" >${person.firstname} ${person.lastname} (${person.address.country.name})</g:link></div>
			<div class="con_m_para11">${person.company.name}</div>
			<div class="con_m_para1">${person.address.city}, ${person.address.state}</div>
			<div class="con_icon"><a href="#"><img title="Mailbox" alt="Mailbox" src="../images/mailbox.png"></a></div>
		</div>
		<div class="col-sm-12 col-md-4 col-lg-4">
			<div class="con_m_para11"><a href="../broadcast/search?createdById=${person.id}">Broadcasts : ${Broadcast.countByCreatedby(person)}</a></div>
			<div class="con_m_para11"><a href="../inventory/getInventory?pid=${person.id}">Inventory : ${Inventory.countByCreatedBy(person)}</a></div>
			<div class="con_m_para11"><a href="../myConnection/list?pid=${person.id}">Connection : ${MyConnection.countByPerson(person)}</a></div>
		</div>
		<div class="col-sm-12 col-md-3 col-lg-3">
			<g:ifNotConnection person="${person}" >
			<div class="buttons1 text-right">
			<a href="javascript:void(0)" id="respond" onClick="javascript:addConnection('${person?.id}');">Connect</a>
			</div>
			</g:ifNotConnection>
		</div>
	</div>
	</g:each>
	<div class="pagination"><g:paginate total="" /></div>
</div>