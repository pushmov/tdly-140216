

<%@ page import="test2.BroadcastFilterAlert" %>
<%@ page import="test2.BroadcastFilter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'broadcastFilterAlert.label', default: 'BroadcastFilterAlert')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastFilterAlertInstance}">
            <div class="errors">
                <g:renderErrors bean="${broadcastFilterAlertInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
				<div class="row">
					<label>Filter Name:</label>
					<div class="inputs">
					<span class="input_wrapper select_wrapper">
                                   <g:select optionKey="id" from="${BroadcastFilter.list()}" optionValue="name" name="broadcastFilter.id" value="" ></g:select>
					</span>
					</div>
												<div class="module_options">
													<div class="module_options_inner">
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="sms" value="${sms}" />SMS</dd>
															<dd><g:checkBox name="email"  value="${email}" />Email</dd>
															<dd><g:checkBox name="inbox"  value="${inbox}" />Inbox</dd>
														</dl>
													</div>
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="gtalk"  value="${gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="yahoo"  value="${yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="msn"  value="${msn}" />MSN</dd>
															<dd><g:checkBox name="aol"  value="${aol}" />AIM</dd>
														</dl>
													</div>
													</div>
												</div>
				</div>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
