 <%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Company" %>
 

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));

		SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MMM-yyyy HH:mm a");
		sdf2.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

                    <g:each in="${brList}" status="i" var="resp">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <%
						def company = Company.get(resp.person.company.id)
				    %>
			    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${company.id}');" >${company.name}</a></td>

                            <td width="70%">${fieldValue(bean:resp, field:'responsetext')}</a></td>
                            <td width="13%">${sdf2.format(resp.dateCreated)}</td>
                            <td width="3%">${resp.person?.company?.address?.country?.name}</td>
					<td width="3%">
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${resp.id}','${resp.listid}','${resp.broadcast.id}');" >Respond</a>
					</td>
                        </tr>
                    </g:each>
