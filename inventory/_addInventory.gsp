
<g:form class="form-horizontal" name="addInventory" >
	<div class="modal-body">
		<g:hasErrors bean="${inventory}">
		<div class="errors">
			<g:renderErrors bean="${inventory}" as="list" />
		</div>
		</g:hasErrors>
		<g:hasErrors bean="${inventory?.item}">
		<div class="errors">
			<g:renderErrors bean="${inventory?.item}" as="list" />
		</div>
		</g:hasErrors>
		
		<g:hiddenField name="id" value="${inventory?.id}" />
		<g:hiddenField name="mfgid" value="" />
		
		<g:render template="/tradeunitinv" var="item" bean="${inventory?.item}"  />	
                
                
	
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-8">
				<button type="button" id="addinvent" class="btn btn-primary">Save</button>
				<button type="button" data-dismiss="modal" class="btn btn-primary">Cancel</button>
			</div>
		</div>
	</div>
</g:form>

<script>
    			
		$('#addinvent').click(function(){
			
			console.log('clicked');
			$.ajax({
				type: "POST",
				url: "../inventory/save",
				cache: false,
				data: $('#addInventory').serialize() + "&description=" + $('#fsingle .Editor-editor').html(),
				error: function(data, textStatus, jqXHR) {
					alert("Error: " + textStatus);
				},
				success: function(data, textStatus, jqXHR) {
					if(data.indexOf("errors") != -1) {
						$('#addInventory').replaceWith(data);
					} else {
						window.location.reload()
					}
				}
			});
		});
	

	
</script>