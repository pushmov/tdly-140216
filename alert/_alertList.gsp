<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<script language="javascript" >
$(document).ready(function()
        {

        showMore(); 
         
});    

</script>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
%>

<div class="table-responsive">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<util:remoteSortableColumn  action="${actionName}" property="bc.broadcasttype" title="Type"  update="${actionName}" />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.unit.part" title="Part"  update="${actionName}"  />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.unit.manufacturer" title="Mfg"  update="${actionName}"  />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.unit.condition" title="Condition"  update="${actionName}"  />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.unit.price" title="Price"  update="${actionName}"  />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.unit.qty" title="Qty"  update="${actionName}"  />
				<th>Description</th>
				<util:remoteSortableColumn  action="${actionName}"   property="bc.company" title="Company"  update="${actionName}"   />
				<util:remoteSortableColumn  action="${actionName}"  property="bc.dateCreated" title="Date"  update="${actionName}"   />
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<g:render template="/alert/alertRow" />
		</tbody>
	</table>
</div>

<div class="pagination">
	<util:remotePaginate action="${actionName}" total="${broadcastTotal}" update="${actionName}" params="['pagination':true]" />
</div>
<style>
	.pagination a.step{
		font-family: Helvetica,Arial,sans-serif;
		background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
		color: #333333;
		display: inline-block;
		font-size: 15px;
		padding: 6px 12px;
		text-align: center;
		text-decoration: none;
		font-weight: 600;
		margin-left: 6px
	}
	
	.pagination a.step:hover{
		background: #f0f0f0;
		border-radius: 5px;
	}
	
	.pagination .currentStep,
	.pagination .nextLink{
		font-family: Helvetica,Arial,sans-serif;
		background: #f9f9f9 none repeat scroll 0 0;
		border: 1px solid #d5d5d5;
		border-radius: 5px;
		padding: 6px 12px;
		color: #00a2d9;
		text-decoration: none;
		font-weight: 600;
		margin-left: 6px
	}
	.pagination .nextLink{
		color: #d5d5d5
	}
	.pagination .nextLink:hover{
		color: #00a2d9;
	}
</style>