

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit TradeUnit</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">TradeUnit List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New TradeUnit</g:link></span>
        </div>
        <div class="body">
            <h1>Edit TradeUnit</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${tradeUnit}">
            <div class="errors">
                <g:renderErrors bean="${tradeUnit}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${tradeUnit?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="condition">Condition:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:tradeUnit,field:'condition','errors')}">
                                    <input type="text" id="condition" name="condition" value="${fieldValue(bean:tradeUnit,field:'condition')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="manufacturer">Manufacturer:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:tradeUnit,field:'manufacturer','errors')}">
                                    <input type="text" id="manufacturer" name="manufacturer" value="${fieldValue(bean:tradeUnit,field:'manufacturer')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="part">Part:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:tradeUnit,field:'part','errors')}">
                                    <input type="text" id="part" name="part" value="${fieldValue(bean:tradeUnit,field:'part')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="price">Price:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:tradeUnit,field:'price','errors')}">
                                    <input type="text" id="price" name="price" value="${fieldValue(bean:tradeUnit,field:'price')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="qty">Qty:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:tradeUnit,field:'qty','errors')}">
                                    <input type="text" id="qty" name="qty" value="${fieldValue(bean:tradeUnit,field:'qty')}" />
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
