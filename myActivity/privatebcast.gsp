 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>MyActivity - Private Broadcasts</title>
        <g:javascript src="myActivity.js" />				



    </head>
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                        
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />
                        	<td>Actions</td>
                        
                        </tr>
                    <g:each in="${broadcastList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast, field:'description')}</td>
				<td>
						<div class="actions_menu">
							<ul>
				   			<li><a href="javascript:void(0)" class="edit" id="respond" onClick="javascript:editInv('${fieldValue(bean:broadcast, field:'id')}','${fieldValue(bean:broadcast.unit, field:'part')}','${fieldValue(bean:broadcast.unit.manufacturer, field:'mfgname')}','${fieldValue(bean:broadcast.unit, field:'condition')}','${fieldValue(bean:broadcast.unit, field:'price')}','${fieldValue(bean:broadcast.unit, field:'qty')}','${fieldValue(bean:broadcast.unit.manufacturer, field:'id')}');" >Edit</a></li>
												<li><g:remoteLink class="delete" controller="broadcast" action="delete" id="${broadcast?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
												<li><a href="javascript:void(0)" class="add" id="respond" onClick="javascript:addToWatchlist('${broadcast.id}');" >Add Part To Watchlist</a></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>


							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
                                 <g:paginate controller="myActivity" action="privatebcast" total="${pbTotal}"  params="[activetab: 0]" />
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->


</html>
