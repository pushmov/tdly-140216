

<%@ page import="test2.UploadStatistics" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'uploadStatistics.label', default: 'UploadStatistics')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${uploadStatisticsInstance}">
            <div class="errors">
                <g:renderErrors bean="${uploadStatisticsInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post"  enctype="multipart/form-data">
                <g:hiddenField name="id" value="${uploadStatisticsInstance?.id}" />
                <g:hiddenField name="version" value="${uploadStatisticsInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="user"><g:message code="uploadStatistics.user.label" default="User" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'user', 'errors')}">
                                    <g:select name="user.id" from="${test2.Person.list()}" optionKey="id" value="${uploadStatisticsInstance?.user?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="comments"><g:message code="uploadStatistics.comments.label" default="Comments" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'comments', 'errors')}">
                                    <g:textField name="comments" value="${uploadStatisticsInstance?.comments}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="company"><g:message code="uploadStatistics.company.label" default="Company" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'company', 'errors')}">
                                    <g:select name="company.id" from="${test2.Company.list()}" optionKey="id" value="${uploadStatisticsInstance?.company?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="invErrors"><g:message code="uploadStatistics.invErrors.label" default="Inv Errors" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'invErrors', 'errors')}">
                                    <g:textField name="invErrors" value="${uploadStatisticsInstance?.invErrors}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="overwriteInventory"><g:message code="uploadStatistics.overwriteInventory.label" default="Overwrite Inventory" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'overwriteInventory', 'errors')}">
                                    <g:checkBox name="overwriteInventory" value="${uploadStatisticsInstance?.overwriteInventory}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="partsAdded"><g:message code="uploadStatistics.partsAdded.label" default="Parts Added" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'partsAdded', 'errors')}">
                                    <g:textField name="partsAdded" value="${fieldValue(bean: uploadStatisticsInstance, field: 'partsAdded')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="partsUpdated"><g:message code="uploadStatistics.partsUpdated.label" default="Parts Updated" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'partsUpdated', 'errors')}">
                                    <g:textField name="partsUpdated" value="${fieldValue(bean: uploadStatisticsInstance, field: 'partsUpdated')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="result"><g:message code="uploadStatistics.result.label" default="Result" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'result', 'errors')}">
                                    <g:textField name="result" value="${uploadStatisticsInstance?.result}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="type"><g:message code="uploadStatistics.type.label" default="Type" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'type', 'errors')}">
                                    <g:textField name="type" value="${uploadStatisticsInstance?.type}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="xlsFile"><g:message code="uploadStatistics.xlsFile.label" default="Xls File" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: uploadStatisticsInstance, field: 'xlsFile', 'errors')}">
                                    <input type="file" id="xlsFile" name="xlsFile" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
