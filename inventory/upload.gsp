<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta name="layout" content="pz" />
	<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">
</head>

<script language="Javascript" >
$(document).ready(function() {

    $('input').customInput();

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 250,
        width: 350,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }
    });

            var submitAutoupload = function(target) {


                            var data = $('#autoupload').serialize();

                            if ( document.getElementById('action').checked ) {
                                data += "&disableauto=on";
                            }


                            $.ajax({
                            type: "POST",
                            url: target,
                            cache: false,
                            data: $('#autoupload').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                   $('#msg').html(data.msg);
                                   $('#dialog-responseSave').dialog("open");
                            },
                            success: function(data, textStatus, jqXHR) {
                                   $('#msg').html(data.msg);
                                   $('#dialog-responseSave').dialog("open");

                            }

                            });

              };

              $('#saveupload').live("click", function() {
                        submitAutoupload("saveupload");
              });

              $('#testupload').live("click", function() {
                        submitAutoupload("testupload");
              });
});

</script>

<main class="content" role="main">
	<div class="main-content">
		
		<div id="tabs">
			<div role="tabpanel">
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" aria-controls="tabs-1" aria-labelledby="ui-id-1" aria-selected="true">
						<a href="${request.contextPath}/inventory/initupload" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-1">Upload Now</a>
					</li>
					<li role="presentation" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-2" aria-selected="false">
						<a href="${request.contextPath}/inventory/autoupload" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-2">Setup AutoUpload</a>
					</li> 
				</ul>
			</div>
				<!--eachtab start -->
			<div class="tab-content">
				<div class="tab-pane" id="tabs-1" aria-labelledby="ui-id-1" role="tabpanel" style="display: block;" aria-expanded="true" aria-hidden="false">
				</div>
				<div class="tab-pane" id="tabs-2" aria-labelledby="ui-id-2" role="tabpanel" style="display: none;" aria-expanded="false" aria-hidden="true">
				</div>
			</div>
		</div>
		
	</div>
</main>

</html>
