<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<div class="tablewrap">
<br/>
<br/>
<div class="eachtab" id="tab-1">

<table width="100%" border="0" cellspacing="1" cellpadding="0"  id="rounded-corner"  >
<thead>
								<tr>
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.dateCreated" title="Date"  update="tabs-3"   />
				<util:remoteSortableColumn  action="myWatchlist" property="bc.broadcasttype" title="Broadcast Type"  update="tabs-3" />
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.unit.part" title="Part"  update="tabs-3"  />
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.unit.manufacturer" title="Manufacturer"  update="tabs-3"  />
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.unit.condition" title="Condition"  update="tabs-3"  />
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.unit.price" title="Price"  update="tabs-3"  />
                   	        <util:remoteSortableColumn  action="myWatchlist"  property="bc.unit.qty" title="Quantity"  update="tabs-3"  />
                   	        <th>Description</th>
                   	        <util:remoteSortableColumn  action="myWatchlist"   property="bc.company" title="Company"  update="tabs-3"   />
								<th>Actions</th>

	
								</tr>

</thead>
 <tfoot>
    </tfoot>
<tbody>
								
  <g:render template="/alert/alertRow" />


		                </tbody>
							
								
							</table>
	<div class="pagination">

						
        	        	<util:remotePaginate action="myWatchlist" total="${broadcastTotal}" update="tabs-3" />
	</div>

		</div>
			<!--inner-contrightpan end -->
</div>
