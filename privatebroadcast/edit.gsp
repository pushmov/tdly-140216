

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Privatebroadcast</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Privatebroadcast List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Privatebroadcast</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Privatebroadcast</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${privatebroadcastInstance}">
            <div class="errors">
                <g:renderErrors bean="${privatebroadcastInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${privatebroadcastInstance?.id}" />
                <input type="hidden" name="version" value="${privatebroadcastInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${privatebroadcastInstance?.company?.id}" noSelection="['null':'']"></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dateCreated">Date Created:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'dateCreated','errors')}">
                                    <g:datePicker name="dateCreated" value="${privatebroadcastInstance?.dateCreated}" noSelection="['':'']"></g:datePicker>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby">Createdby:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'createdby','errors')}">
                                    <input type="text" id="createdby" name="createdby" value="${fieldValue(bean:privatebroadcastInstance,field:'createdby')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="noofresponses">Noofresponses:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'noofresponses','errors')}">
                                    <input type="text" id="noofresponses" name="noofresponses" value="${fieldValue(bean:privatebroadcastInstance,field:'noofresponses')}" />
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="broadcasttype">Broadcasttype:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'broadcasttype','errors')}">
                                    <input type="text" id="broadcasttype" name="broadcasttype" value="${fieldValue(bean:privatebroadcastInstance,field:'broadcasttype')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="description">Description:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'description','errors')}">
                                    <input type="text" id="description" name="description" value="${fieldValue(bean:privatebroadcastInstance,field:'description')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="unit">Unit:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastInstance,field:'unit','errors')}">
                                    
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
