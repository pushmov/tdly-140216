

<%@ page import="test2.MyConnection" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'myConnection.label', default: 'MyConnection')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${myConnectionInstance}">
            <div class="errors">
                <g:renderErrors bean="${myConnectionInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="contact"><g:message code="myConnection.contact.label" default="Contact" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: myConnectionInstance, field: 'contact', 'errors')}">
                                    <g:select name="contact.id" from="${test2.Person.list()}" optionKey="id" value="${myConnectionInstance?.contact?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby"><g:message code="myConnection.createdby.label" default="Createdby" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: myConnectionInstance, field: 'createdby', 'errors')}">
                                    <g:textField name="createdby" value="${myConnectionInstance?.createdby}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="person"><g:message code="myConnection.person.label" default="Person" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: myConnectionInstance, field: 'person', 'errors')}">
                                    <g:select name="person.id" from="${test2.Person.list()}" optionKey="id" value="${myConnectionInstance?.person?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
