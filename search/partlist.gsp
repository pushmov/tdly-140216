<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Inventory" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
	<g:javascript library="jquery" plugin="jquery" />
	<jqui:resources/>
        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />


<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

<script>

 
 

	$(document).ready(function() {


	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	    $('#dialog-bmessage').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });

	    $('#dialog-editmessage').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	    $('#dialog-broadcast').dialog({
        	autoOpen: false,
	        height: 500,
        	width: 700,
	        modal: true,
        	buttons: {
		            Save: function() {
        		        $('#fsingle').submit(); 
                		$(this).dialog('close');
            		    },
		            Cancel: function() {
                		$(this).dialog('close');
            		    }

	        }        
             });

	    $('#dialog-editInventory').dialog({
        	autoOpen: false,
	        height: 500,
        	width: 700,
	        modal: true,
        	buttons: {
		            Save: function() {
        		        $('#editInventory').submit(); 
                		$(this).dialog('close');
            		    },
		            Cancel: function() {
                		$(this).dialog('close');
            		    }

	        }        
             });


      $('#fsingle').submit( function() {

			    $.ajax({
    			    type: "POST",
    			    url: "../broadcast/save",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-bmessage').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

		    
        	            return false;

              });


   

      $('#editInventory').submit( function() {

			    alert('submitting');
			    $.ajax({
    			    type: "POST",
    			    url: "../inventory/update",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-editmessage').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

		    
        	            return false;

              });


        })



function fadeit(resp) {
		Ext.MessageBox.alert(resp.success,resp.message);
		var stopClose = function() {
			Ext.MessageBox.hide();
		};
		setTimeout(stopClose, 1500);
	}


function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}

function addToWatchlist(invid) {


			    $.ajax({
    			    type: "POST",
    			    url: "../inventory/addwatchlist",
			    cache: false,
    			    data: { id : invid },
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#dialog-message').dialog("open");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });



}

function broadcast(part,manufacturer,condition,price,quantity,mfgid) {

	//$('#fsingle #invid').val(invid);
	$('#fsingle #part').val(part);
	$('#fsingle #qty').val(quantity);
	$('#fsingle #price').val(price);
	$('#fsingle #condition').val(condition);
	$('#fsingle #manufacturerid').val(mfgid);
	$('#dialog-broadcast').dialog('open');
	
}

function editInv(invid,part,manufacturer,condition,price,quantity,mfgid) {
        

	$('#editInventory #invid').val(invid);
	$('#editInventory #part').val(part);
	$('#editInventory #qty').val(quantity);
	$('#editInventory #price').val(price);
	$('#editInventory #condition').val(condition);
	$('#editInventory #manufacturerid').val(mfgid);
	$('#dialog-editInventory').dialog('open');
	
}

function getFilter(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {
		
			alert($.toJSON(data));
			alert('filterid : ' + data.id);

			$('#editInventory #part').val(data.id);
			$('#name').val(data.name);
			$('#keywords').val(data.keywords);

			if(data.wtb) {
				$('#wtb').attr('checked', true);
			}
			if(data.wts) {
				$('#wts').attr('checked', true);
			}
			if(data.myVendors) {
				$('#myVendors').attr('checked', true);
			}
			$('input:radio[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('checked', true);
		
			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
			}
				$("#manufacturer").val(mfgArr);

			$("#continents option:first").attr('selected','selected');
		        $("#countrySelect").load("../continent/updateSelectFilter",{continentid: $('#continents').val(),prevcontinentid : prevSelect ,countryid: null, domain: 'broadcastFilter' , ajax: 'true'});
			prevSelect = $(this).val();

		


			//$('#dialog-form').binddata(data);
			$('#dialog-form').dialog('open');
			

                }
             );
	
 }





</script>


</head>
<body>
<div id="dialog-message" title="Watchlist"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Part added to Watchlist.
	</p> 
</div> 

<div id="dialog-bmessage" title="Broadcast"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast Created.
	</p> 
</div> 

<div id="dialog-editmessage" title="Inventory Update"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Part chnages saved.
	</p> 
</div> 

<div id="dialog-broadcast" title="Create Broadcast"> 
	<p>
            <g:form name="fsingle" >
							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>


								<div class="row">
									<label>BroadcastType:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
					                                    <g:select from="${['WTB','WTS']}" name="broadcasttype" value="" ></g:select>
										</span>
									</div>
								</div>

                  					    <g:render template="/tradeunitajax" var="tradeunit"  />


								<div class="row">
									<label>Description:</label>
									<div class="inputs">
										<span class="input_wrapper"><input class="text" name="description" type="text" /></span>
									</div>
								</div>
 
								<!--[if !IE]>end row<![endif]-->
								
							</fieldset>
							
							
	</g:form>
	</p>
</div>

<div id="dialog-editInventory" title="Edit Inventory"> 
	<p>
            <g:form  name="editInventory" >

 					    <g:hiddenField name="invid" value="" />
					    <g:hiddenField name="mfgid" value="" />
					    <g:render template="/tradeunitajax" var="item"  />

	    </g:form>

	</p>
</div>






				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul class="search_tabs">
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
<!--						<div id="product_list_menu">
							<a href="#" class="update"><span><span><em>Add New Part</em><strong></strong></span></a>
						</div>
	-->					

						<div id="left-filter"  >
							Put filter here
						</div>
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper" id="right-main">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

                   	        <g:sortableColumn property="part" params="${[searchterm: flash.searchterm]}" title="Part" />
                   	        <g:sortableColumn property="mfgname" params="${[searchterm: flash.searchterm]}" title="Mfg" />
                   	        <g:sortableColumn property="condition" params="${[searchterm: flash.searchterm]}" title="Condition" />
                   	        <g:sortableColumn property="price" params="${[searchterm: flash.searchterm]}" title="Price" />
                   	        <g:sortableColumn property="qty" params="${[searchterm: flash.searchterm]}" title="Qty" />
                   	        <g:sortableColumn property="companyName" params="${[searchterm: flash.searchterm]}" title="Company" />
5                   	        <g:sortableColumn property="countryName" params="${[searchterm: flash.searchterm]}" title="Country" />
                   	        <g:sortableColumn property="sortableLastUpdated" params="${[searchterm: flash.searchterm]}" title="Updated" />

<!--                        
                   	        <g:sortableColumn property="myvendorsonly" title="Myvendorsonly" />
                        
                    	        <g:sortableColumn property="searches" title="Searches" />
                        
                   	        <g:sortableColumn property="hits" title="Hits" />
                        
                   	        <g:sortableColumn property="wtb" title="Wtb" />

  -->
									
									<th>Actions</th>
								</tr>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		//println "TimeZone " + session.company?.address?.timezone
		//sdf.setTimeZone(TimeZone.getTimeZone(session.company.address.timezone));
%>

								
                    <g:each in="${inventoryList}" status="i" var="inventory">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                            <td id="part${i}" >${fieldValue(bean:inventory.item, field:'part')}</td>
                            <td id="${fieldValue(bean:inventory.item.manufacturer, field:'id')}" >${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'condition')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'price')}</td>
                            <td id="${i}" >${fieldValue(bean:inventory.item, field:'qty')}</td>
                            <td id="${i}" >${inventory.company.name}</td>
                            <td id="${i}" >${inventory.company.address.country.name}</td>
                        
                            <td>${sdf.format(inventory.lastUpdated)}</td>
<!--
                            <td>${fieldValue(bean:inventory, field:'searches')}</td>
                            <td>${fieldValue(bean:inventory, field:'hits')}</td>
                            <td>${fieldValue(bean:inventory, field:'wtb')}</td>

  -->
			    
									<td>
										<div class="actions_menu">
											<ul>
												<li><a href="javascript:void(0)" class="details" id="respond" onClick="javascript:broadcast('${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition')}','${fieldValue(bean:inventory.item, field:'price')}','${inventory.item.qty}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >Broadcast</a></li>

												<li><a href="javascript:void(0)" class="add" id="respond" onClick="javascript:addToWatchlist('${inventory.id}');" >Add To Watchlist</a></li>
											</ul>
										</div>
									</td>
 
			 

                    <input type="hidden" name="id" value="${inventory?.id}" />
				

			    </td>

                        
                        </tr>
                    </g:each>
								
								
							</table>
							</div>

					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						<g:paginate total="${inventoryList.size()}" />
					</div>

						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
					<!--[if !IE]>end pagination<![endif]-->

						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
