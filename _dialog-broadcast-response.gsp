<script language="Javascript" >

$(document).ready(function()
        {

  $('#dialog-response').dialog({ 
				dialogClass:'alertbroadcastresponse',
        autoOpen: false,
        height: 550,
        width: 905,
        modal: true,
	open:function(event, id) {
$('textarea.tinymce').tinymce({
    selector: "textarea.tiny",
    theme: "modern",
    width: 850,
    height: 500,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]


		});
	},
        buttons: {
            'Send Response': function() {

    		responseSubmit();
		tinyMCE.execCommand('mceRemoveControl',false,'mcetext');	
                $(this).dialog('close');

            },

            Cancel: function() {
		tinymce.execCommand('mceRemoveControl',false,'textarea');	
                $(this).dialog('close');
            }

        }        
    });

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 250,
        width: 350,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });




	 });


	function showResponse(bid,rid) {
        	$('#bid').val(bid);
        	$('#rid').val(rid);
		$('#modal_dialog_broadcast_response').modal('show');
		//var d = $('#dialog-response').position();
		//(window).scrollTop();
 	}

	function submitData(){

			    $.ajax({
    			    type: "POST",
    			    url: "/tdly/broadcastResponse/save",
			    cache: false,
    			    data: 'bid='+$('#bid').val()+'&rid='+$('#rid').val()+'&responsetext='+$('.Editor-editor').html(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
                                    $('#modal_dialog_broadcast_response').modal('hide');
				   $('#modal_dialog_broadcast_success').modal("show");
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

       	 }; 

</script>


<div class="modal fade broadcasts-popup" id="modal_dialog_broadcast_response" tabindex="-1" role="dialog" aria-labelledby="modal_dialog_broadcast_response" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Broadcast Response</h4>
              </div>
              <!--modal-header-->
              <div class="modal-body">
                    <g:form url="[action:'save', controller:'broadcastResponse']" method="post" class="form-horizontal"  name="formResponse"  >
			<g:hiddenField name="bid" value="" />
			<g:hiddenField name="rid" value="" />

			<fieldset>
				<!--[if !IE]>start forms<![endif]-->
				<div class="row">
                                <div class="form-group">
                                    <label for="m_p_type" class="col-sm-2 control-label">Response:</label>
                                    <div class="col-sm-8"><textarea name="responsetext" class="text-editor" rows="10 cols="125"></textarea></div>
                                </div>
				
				</div>
                                <div class="row text-right">
                                    <button type="button" class="btn btn-broadcast" onclick="submitData()">Submit</button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
			</fieldset>
					
							
	    </g:form>
              </div>
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
</div>

<div class="modal fade broadcasts-popup" id="modal_dialog_broadcast_success" tabindex="-1" role="dialog" aria-labelledby="modal_dialog_broadcast_success" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_dialog_broadcast_success">Broadcast Response Sent</h4>
              </div>
              <!--modal-header-->
              <div class="modal-body">
                    <p>Broadcast Response Sent.</p>
                    <div class="row text-right">
                                    
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
              </div>
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
</div>

<script src="${createLinkTo(dir:'assets',file:'partszap/js/editor.js')}"></script>
<script>
$(document).ready(function(){
	$('.text-editor').Editor();
});
</script>

