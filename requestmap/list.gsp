<%@page import="test2.Requestmap" %>
<html> 
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Requestmap List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Requestmap</g:link></span>
        </div>
        <div class="body">
            <h1>Requestmap List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="url" title="Url" />
                        
                   	        <g:sortableColumn property="configAttribute" title="Config Attribute" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${requestmapList}" status="i" var="requestmap">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${requestmap.id}">${fieldValue(bean:requestmap, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:requestmap, field:'url')}</td>
                        
                            <td>${fieldValue(bean:requestmap, field:'configAttribute')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Requestmap.count()}" />
            </div>
        </div>
    </body>
</html>
