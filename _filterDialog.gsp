<%@ page import="test2.BroadcastFilter" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Country" %>
<%@page import="test2.Cond" %>
<%
boolean allcomp = ((broadcastFilter?.companyType?.equals("all")) || (broadcastFilter?.companyType == null))

boolean myvendors = broadcastFilter?.myVendors
boolean myconnections = broadcastFilter?.myConnections
%>

							<g:form name="broadcastFilterForm" class="form-horizontal" >
								<g:hiddenField name="id" value="${broadcastFilter?.id}" />
	
								<div class="modal-body">
									<!--form-group-->
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Filter Name:</label>
                    <div class="col-sm-8">
											<g:textField name="name" value="${broadcastFilter?.name}" class="form-control" />
                    </div>
                  </div>
									<div class="form-group">
                    <label for="s_p_type" class="col-sm-2 control-label">Broadcast Type:</label>
                    <div class="col-sm-8">
                      <label><g:checkBox name="wtb" value="${broadcastFilter?.broadcasttype?.contains("WTB")}" />WTB</label>
											<label><g:checkBox name="wts" value="${broadcastFilter?.broadcasttype?.contains("WTS")}" />WTS</label>
                    </div>
                  </div>
									
									<div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Keyword:</label>
                    <div class="col-sm-8">
											<g:textField name="keyword" value="${broadcastFilter?.keyword?.equals("all")?"":broadcastFilter?.keyword}" class="form-control" />
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Condition:</label>
                    <div class="col-sm-8">
                      <input type="text" placeholder="Search By Condition" id="conditionS-filterinput" class="form-control">
                      <ul class="unstyled bcast-filter filter-list conditionS">
				<g:each in="${Cond.list()}">
                                                                                                                        <li>
                                                                                                                                <label for="conditionS-${it.id}"><input type="checkbox" id="conditionS-${it.id}"  name="conditionS" value="${it.id}"    />${it.name}</label>
                                                                                                                        </li>
                        </g:each>
                      </ul>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Partlist:</label>
                    <div class="col-sm-8">
											<g:select from="${['MyInventory','MyWatchlist']}" noSelection="${['All':'All']}" name="partlist" value="${broadcastFilter?.partlist}" class="form-control"></g:select>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Manufacturer:</label>
                    <div class="col-sm-8">
                        <input type="text" placeholder="Search By Manufacturer" id="manufacturer-filterinput" class="form-control">
                        <ul class="unstyled bcast-filter filter-list manufacturer">
<g:each in="${Manufacturer.list(max:10).unique()}">
                                                                                                                        <li><label for="manufacturer-${it.id}"><input type="checkbox" id="manufacturer-${it.id}"  name="manufacturer" value="${it.id}"    />${it.mfgname}</label></li>
                                                                                                                </g:each>
                        </ul>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Companies:</label>
                    <div class="col-sm-8">
                      <div>
                        <g:radio name="companyType" checked="${myvendors}" value="myVendors" id="vendors" /><label for="vendors">My Vendors</label>	
                        <g:radio name="companyType" checked="${!myvendors}" value="all"  id="scompanies" /><label for="scompanies">All/ Selected Companies</label>
                      </div>
                      <div>
                        <input type="text" placeholder="Search By Company" id="company-filterinput" class="form-control">
                        <ul class="unstyled bcast-filter filter-list company">
				<g:each in="${Company.list(max:10).unique()}">
                            <li>
                              <label for="company-${it.id}"><input type="checkbox" id="company-${it.id}"  name="company" value="${it.id}"    >${it.name} </label>
                            </li>
                                                                                                                </g:each>
                        </ul>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-group">
                    <label for="s_p_part" class="col-sm-2 control-label">Countries:</label>
                    <div class="col-sm-8">
											<input type="text" placeholder="Search By Country" id="country-filterinput" class="form-control">
                      <ul class="unstyled bcast-filter filter-list country">
				<g:each in="${Country.list(max:10).unique()}">
                            <li>
                              <label for="country-${it.id}"><input type="checkbox" id="country-${it.id}"  name="country" value="${it.id}"     >${it.name} </label>
                            </li>
                                                                                                                </g:each>
                      </ul>
                    </div>
                  </div>
                  
                <div class="modal-footer">
                    <p align="left" class="ajax-resp"></p>
                    <div class="form-group">
                      <div class="col-sm-offset-2 col-sm-8">
                         <button class="btn btn-primary btn-filter-save" type="button" id="filter-save-dialog-old">Save</button>
                         <button data-dismiss="modal" class="btn btn-primary btn-filter-cancel" type="button">Cancel</button>
                      </div>
                    </div>
                </div>
									
								</div>
	
            </g:form>

<!-- filter dialog end -->


<script>
	$('#conditionS-filterinput').filterList();
	$('#manufacturer-filterinput').filterList();
	$('#company-filterinput').filterList();
	$('#country-filterinput').filterList();
	
	$('.filterinput-wrap input').bind('keyup', function(){
		
		if($(this).val() == ''){
			
			$(this).parent().find('i.fa-search').show();
			$(this).parent().find('span').hide();
			
		} else {
			
			$(this).parent().find('i.fa-search').hide();
			$(this).parent().find('span').show();
			
		}
		
	});
	
	$('.filterinput-wrap .fa-times-circle').click(function(){
		
		$(this).parent().parent().find('i.fa-search').show();
		$(this).parent().parent().find('input').val('');
		$(this).parent().parent().find('ul.bcast-filter li').show();
		$(this).parent().hide();
		
	});
	
	$('#filter-save-dialog-old').click(function(){
		var form = $(this).closest('form');
		$.ajax({
            type: "POST",
            url: "../broadcastFltr/save",
            data: form.serialize(),
            error: function(data, textStatus, jqXHR) {
				alert("Error: " + textStatus);
            },
            success: function(data, textStatus, jqXHR) {
				location.reload( true);
            }
        });
        return false;
	});
	
</script>
