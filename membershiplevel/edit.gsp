

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Membershiplevel</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Membershiplevel List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Membershiplevel</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Membershiplevel</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${membershiplevelInstance}">
            <div class="errors">
                <g:renderErrors bean="${membershiplevelInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${membershiplevelInstance?.id}" />
                <input type="hidden" name="version" value="${membershiplevelInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="description">Description:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:membershiplevelInstance,field:'description','errors')}">
                                    <input type="text" id="description" name="description" value="${fieldValue(bean:membershiplevelInstance,field:'description')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="memlevel">Memlevel:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:membershiplevelInstance,field:'memlevel','errors')}">
                                    <input type="text" id="memlevel" name="memlevel" value="${fieldValue(bean:membershiplevelInstance,field:'memlevel')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
