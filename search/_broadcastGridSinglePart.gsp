<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>
<%@page import="test2.Company" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if (session.person?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
		}		
%>

<div class="tab-content-left">
	<form id="filterForm" name="signUpForm" action="../broadcast/filter" method="post">
	
		<input type="hidden" id="filterName" name="name" value="" />
    <input type="hidden" id="parttype" name="parttype" value="Single" />
    <input type="hidden" id="createdById" name="createdById" value="${filterParams?.createdById}" />
    <input type="hidden" id="myconnections" name="myconnections" value="<%=filterParams?.myconnections%>" />
		
		<div class="btn-group-sidebar">
			<div class="btn-group btn-group-sm" role="group" aria-label="sidebar button group">
				<button type="button" id="filterResults" class="btn btn-primary">
					<i class="fa fa-filter" aria-hidden="true"></i> Filter
				</button>
				<button type="button" data-toggle="modal" data-target="#modal_single_part_filter" class="btn btn-primary">
					<i class="fa fa-floppy-o" aria-hidden="true"></i> Save Filter
				</button>
			</div>
		</div>
		
		<!--side-bar-btn-group-->
		<div class="panel-group">
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_01">
						Filter Name <span class="icon" aria-hidden="true"></span>
					</a>
				</div>
				<div id="acc_collapse_01" class="panel-collapse collapse in">
					<div class="panel-body">
						<g:select class="form-control" id="filterdd" optionKey="id" from="${BroadcastFilter.findAllByPersonAndFilterTypeInList(session.person,['S','M'])}" optionValue="name" name="filterdd" onChange="getFilter(this.value)"  value="${filterid}" noSelection="[null:'Select Filter']"></g:select>
					</div>
				</div>
			</div>
			
			
			<!--panel-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_03" class="collapsed">KEYWORD <span class="icon" aria-hidden="true"></span></a>
				</div>
				<div id="acc_collapse_03" class="panel-collapse collapse">
					<div class="panel-body">
						<input type="text" name="keyword" value="${filterParams?.keyword}" class="form-control" placeholder="search">
					</div>
				</div>
			</div>
			
			<!--panel-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_04" class="collapsed">
						Condition <span class="icon" aria-hidden="true"></span>
					</a>
				</div>
				
				<div id="acc_collapse_04" class="panel-collapse collapse">
					<div class="panel-body">
						<input type="text" class="form-control filterlist" id="conditionS-filterinput-search" placeholder="Search by condition">
						<ul class="checkbox-list">
						<g:each in="${conditionMap}">
							<li>
								<label for="conditionS-${it.value.id}"><input type="checkbox" name="conditionS" value="${it.value.id}" id="conditionS-${it.value.id}" />${it.value.name} (${it.value.hit})</label>
							</li>
						</g:each>
						</ul>
					</div>
				</div>
			</div>
			
			<!--panel-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_05" class="collapsed">PartList <span class="icon" aria-hidden="true"></span></a>
				</div>
				<div id="acc_collapse_05" class="panel-collapse collapse">
					<div class="panel-body">
						<g:select class="form-control" from="${['MyInventory','MyWatchlist']}" noSelection="${['All':'All']}" name="partlist" value="${partlist}"></g:select>
					</div>
				</div>
			</div>
			
			<!--panel-->
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_06" class="collapsed">
						Manufacturer <span class="icon" aria-hidden="true"></span>
					</a>
				</div>
				
				<div id="acc_collapse_06" class="panel-collapse collapse">
					<div class="panel-body" style="overflow: auto; height: 180px;">
						<input type="text" id="manufacturer-filterinput-search" class="form-control" placeholder="Search by Manufacturer">
						<ul class="checkbox-list">
						<g:each in="${manufacturerMap}">
							<li><label for="manufacturer-${it.value.id}"><input type="checkbox"   name="manufacturer"  value="${it.value.id}"   id="manufacturer-${it.value.id}" />${it.value.name} (${it.value.hit})</label></li>
						</g:each>
						</ul>
					</div>
				</div>
			</div>
			<!--panel-->
			
			<g:if test="${filterParams?.myconnections == false}" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc2_collapse_04" class="collapsed">
						Companies <span class="icon" aria-hidden="true"></span>
					</a>
				</div>
				<div id="acc2_collapse_04" class="panel-collapse collapse">
					<div class="panel-body">
						<div class="radio">
							<label>
								<g:radio name="companyType" value="myVendors" checked="${myvendors}" /> My Vendors
							</label>
						</div>
						<div class="radio">
							<label>
								<g:radio name="companyType" value="all" checked="${allcomp}" /> Selected Companies
							</label>
						</div>
						
            <input type="text" class="form-control" id="company-filterinput-search" placeholder="Search by condition">
						<ul class="checkbox-list">
							<g:each in="${companyMap}">
							<li>
								<label for="company-${it.value.id}"><input type="checkbox" id="company-${it.value.id}" />${it.value.name} (${it.value.hit})</label>
							</li>
							</g:each>
						</ul>
					</div>
				</div>
			</div>
			</g:if>
			
			<g:if test="${filterParams?.myconnections == false}" >
			<div class="panel panel-default">
				<div class="panel-heading">
					<a data-toggle="collapse" href="#acc_collapse_07" class="collapsed">
						Countries <span class="icon" aria-hidden="true"></span>
					</a>
				</div>
				
				<div id="acc_collapse_07" class="panel-collapse collapse">
					<div class="panel-body" style="overflow: auto; height: 180px;">
						<input type="text" id="country-filterinput-search" class="form-control" placeholder="Search by Country">
						<ul class="checkbox-list">
						<g:each in="${countryMap}">
							<li><label for="country-${it.value.id}"><input type="checkbox" id="country-${it.value.id}" />${it.value.name} (${it.value.hit})</label></li>
						</g:each>
						</ul>
					</div>
				</div>
			</div>
			</g:if>
			
		</div>
		
		<!--panel-group-->
	</form>
</div>						

<g:render template="broadcastTableSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,filterParams:filterParams, broadcastTotal:broadcastTotal]}" />

<script>
	$(window).load(function(){
		
		$('#dialog-broadcast').hide();
		$('#dialog-broadcast-multiple').hide();
		$('#dialog-bmessage').hide();
		
		
	});
</script>

<script>
	$(document).ready(function(){
		setTimeout(function(){
			$('#searchresultwrap-penal h3').each(function(){
				
				if($(this).next().is(':visible')){
					$(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
				} else {
					$(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
				}
				// $(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
			});
		}, 500);
		
		$('ul.bcast-filter').each(function(){
			if($(this).find('li').length > 5) {
				$(this).css({
					'height' : '130px',
					'overflow-y' : 'scroll'
				});
			}
		});
		
		
		// $('#signal_parts h3.collapse-open').each(function(){
			// $(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
		// });
    
    $('#conditionS-filterinput-search').filterList();
    $('#manufacturer-filterinput-search').filterList();
    $('#company-filterinput-search').filterList();
    $('#country-filterinput-search').filterList();
	});
	
	
	
	
	$('.filterinput-wrap input').bind('keyup', function(){
		
		if($(this).val() == ''){
			
			$(this).parent().find('i.fa-search').show();
			$(this).parent().find('span').hide();
			
		} else {
			
			$(this).parent().find('i.fa-search').hide();
			$(this).parent().find('span').show();
			
		}
		
	});
	
	$('.filterinput-wrap .fa-times-circle').click(function(){
		
		$(this).parent().parent().find('i.fa-search').show();
		$(this).parent().parent().find('input').val('');
		$(this).parent().parent().find('ul.bcast-filter li').show();
		$(this).parent().hide();
		
	});
	
	$('#searchresultwrap-penal h3').click(function(){
			
			if($(this).find('span i').hasClass('fa-plus')){
				
				$(this).find('span i').removeClass('fa-plus').addClass('fa-minus');
			} else {
				$(this).find('span i').removeClass('fa-minus').addClass('fa-plus');
			}			
	});
</script>
