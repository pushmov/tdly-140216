
<%@ page import="test2.AutoUpload" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>AutoUpload List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New AutoUpload</g:link></span>
        </div>
        <div class="body">
            <h1>AutoUpload List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="ftpurl" title="Ftpurl" />
                        
                   	        <g:sortableColumn property="httpurl" title="Httpurl" />
                        
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="disableauto" title="Disableauto" />
                        
                   	        <th>Lastupdated</th>
                   	    
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${autoUploadInstanceList}" status="i" var="autoUploadInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${autoUploadInstance.id}">${fieldValue(bean:autoUploadInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:autoUploadInstance, field:'ftpurl')}</td>
                        
                            <td>${fieldValue(bean:autoUploadInstance, field:'httpurl')}</td>
                        
                            <td>${fieldValue(bean:autoUploadInstance, field:'company')}</td>
                        
                            <td>${fieldValue(bean:autoUploadInstance, field:'disableauto')}</td>
                        
                            <td>${fieldValue(bean:autoUploadInstance, field:'lastupdated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${autoUploadInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
