

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Staff</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Staff List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Staff</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Staff</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${staff}">
            <div class="errors">
                <g:renderErrors bean="${staff}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${staff?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="aol">Aol:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'aol','errors')}">
                                    <input type="text" id="aol" name="aol" value="${fieldValue(bean:staff,field:'aol')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="businesstype">Businesstype:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'businesstype','errors')}">
                                    <input type="text" id="businesstype" name="businesstype" value="${fieldValue(bean:staff,field:'businesstype')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="cellphone">Cellphone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'cellphone','errors')}">
                                    <input type="text" id="cellphone" name="cellphone" value="${fieldValue(bean:staff,field:'cellphone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="email">Email:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'email','errors')}">
                                    <input type="text" id="email" name="email" value="${fieldValue(bean:staff,field:'email')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="emprole">Emprole:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'emprole','errors')}">
                                    <input type="text" id="emprole" name="emprole" value="${fieldValue(bean:staff,field:'emprole')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="firstname">Firstname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'firstname','errors')}">
                                    <input type="text" id="firstname" name="firstname" value="${fieldValue(bean:staff,field:'firstname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="gtalk">Gtalk:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'gtalk','errors')}">
                                    <input type="text" id="gtalk" name="gtalk" value="${fieldValue(bean:staff,field:'gtalk')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastname">Lastname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'lastname','errors')}">
                                    <input type="text" id="lastname" name="lastname" value="${fieldValue(bean:staff,field:'lastname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="msn">Msn:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'msn','errors')}">
                                    <input type="text" id="msn" name="msn" value="${fieldValue(bean:staff,field:'msn')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="officephone">Officephone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'officephone','errors')}">
                                    <input type="text" id="officephone" name="officephone" value="${fieldValue(bean:staff,field:'officephone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="password">Password:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'password','errors')}">
                                    <input type="text" id="password" name="password" value="${fieldValue(bean:staff,field:'password')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="tollfreephone">Tollfreephone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'tollfreephone','errors')}">
                                    <input type="text" id="tollfreephone" name="tollfreephone" value="${fieldValue(bean:staff,field:'tollfreephone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="yahoo">Yahoo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:staff,field:'yahoo','errors')}">
                                    <input type="text" id="yahoo" name="yahoo" value="${fieldValue(bean:staff,field:'yahoo')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
