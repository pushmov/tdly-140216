<%@page import="test2.Company" %>
<%@page import="test2.MyVendor" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.Person" %>
<%@page import="test2.Inventory" %>
<%@page import="com.ucbl.util.PZUtil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>




    </head>
<g:render template="/vendor"  />
<div class="tablewrap">
<div class="eachtab" id="tab-1">
<div class="search_message">
    <div class="inbox_top2">
		       Sort by recent activity
		  </div>
		  <div class="clear"></div>
                    <g:each in="${companyList}" status="i" var="company">
				<g:if test="${company.id != session.company.id }">
		  <div class="inbox_main">
		    <div class="icon_img"><img src="images/company_icon.jpg" /></div>
			  <div class="con_m">
			       <div class="con_m_heading"><a href="../company/view?id=${company.id}" >${company.name} (${company.address.country.name})</a></div>
				   <div class="con_m_para11">${company.category}</div>
				   <div class="con_m_para1">${company.address.city}, ${company.address.state}</div>
<g:ifNotVendor company="${company}" keyword="${keyword}"  >
		           <div class="buttons1"><a href="javascript:void(0)"  id="respond" onClick="javascript:addvendor('${company.id}');"  >Follow&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></div>
</g:ifNotVendor>
			  </div>
			  
			  <div class="con_m1">
			       <div class="con_m_para11"><a href="../broadcast/search?companyId=${company.id}" >Broadcasts : ${Broadcast.countByCompany(company)}</a></div>
				   <div class="con_m_para11"><a href="../inventory/getInventoryC?cid=${company.id}" >Inventory : ${Inventory.countByCompany(company)}</a></div>
				   <div class="con_m_para11">
	 <a href="../person/followers?id=${company.id}" > Followers : ${MyVendor.countByVendor(company)} </a> 
	</div>
			  </div>
			  
			  <div class="con_m1">              
		          <div class="con_m_para11"><a href="../person/emplist?id=${company.id}" >Employees : ${Person.countByCompany(company)}</a></div>
		      </div>
			  
			  <div class="date_m"><%=PZUtil.format(company.dateCreated)%></div>
			  <div class="clear"></div>
		  </div>
	<div id="bresp"></div>
				</g:if>
                    </g:each>
		  
		  
		  <div  class="clear"></div>
                	<div class="pagination_n">
			<g:paginate controller="myVendor" action="searchcompany" params="[keyword: flash.keyword, searchtype: flash.searchtype]" total="${total}" />
			</div>
		  
		  
		  <div  class="clear"></div>
	 </div>
	 
	 <div  class="clear"></div>

	</div>
</div>
	
    </body>
</html>
