<%@page import="test2.Membershiplevel" %>
<%@page import="test2.Country" %>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Edit Company</title>
<script language="javascript" >
   $(document).ready(function() {
	$("#countrySelect").change(respondToCountrySelect);

tinymce.init({
    selector: "textarea.tiny",
    theme: "modern",
    width: 850,
    height: 500,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ]
 }); 


 });

function respondToCountrySelect(event) {
	alert('1');
       $.get('../country/updateSelect' , {selectedValue : $("#countrySelect").val()}, function(data) { 
        $("#stateSelect").html(data);
       });
 }

 function updateCompany() {
	
	$('#companyForm').submit();
 
 }

 
$(function() {
 companyForm = $('#companyForm');
companyForm = companyForm.validate({
onkeyup: false,
errorClass: 'invalid',
errorElement: 'label',	
validClass: 'success',			
onsubmit: true,


errorContainer: '#companyFormErr',
errorLabelContainer: 'div.errors ul',
wrapper: 'li',	
			
rules: {
name: { required: true },
category: { required: true },
address: { required: true },
"phone" : { required: true },
"city" : { required: true },
"postalcode" : { required: true },
"state" :  { required: true },
"country.id" :  { required: true },
"street1" :  { required: true },
membershiplevel: {required: true },
founded: { digits: true, },
noofemployees: { digits: true, },
},
messages: {
name: { required: 'Company Name cannot be blank' },
category: { required: 'Category cannot be blank' },
address: {required: 'Address cannot be blank' },
"phone" : { required: 'Phone cannot be blank'},
"city" : { required: 'City cannot be blank'},
"postalcode" : { required: 'Postalcode cannot be blank'},
"state" : { required: 'State cannot be blank'},
"country.id" : { required: 'Country cannot be blank'},
"street1" : { required: 'Address cannot be blank'},
membershiplevel: { required: ' cannot be blank'  },
founded: { digits: 'Property founded must be a valid number', },
noofemployees: { digits: 'Property noofemployees must be a valid number', },
}
});
});

</script>
    </head>

<br>
<br>
		<!--eachtab start -->
		<div class="eachtab" id="tab-1">
		    <h2 class="etext">Edit Company</h2>
            <div class="formsection formeditcompany">
<jqvalui:renderErrors render="true" errorContainer="companyFormErr" style="margin-bottom:10px"/>

			 <g:form name="companyForm" action="update" method="post" class="search_form" >
				 <input type="hidden" name="id" value="${company?.id}" />
			   <table width="100%" border="0" cellspacing="0" cellpadding="10" align="center">
				 <!--
				  <tr>
					<td  align="left" valign="top" class="gtext">Name</td>
					<td  align="left" valign="top" class="gtext">&nbsp;</td>
					<td align="left" valign="top" ><input type="text" name="name" id="name" class="inptinpt"  value="${fieldValue(bean:company,field:'name')}" /></td>
				  </tr>
				  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
					-->
				  <tr>
					<td colspan="3" align="left" valign="top">
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					  <tr>
						<td width="50%" align="left" valign="top">
						  <table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
								<td  align="left" valign="top" class="gtext">Name</td>
								<td  align="left" valign="top" class="gtext">&nbsp;</td>
								<td align="left" valign="top" ><input type="text" name="name" id="name" class="inptinpt"  value="${fieldValue(bean:company,field:'name')}" /></td>
								</tr>
								<tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Category</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="category" id="category" class="inptinpt" value="${fieldValue(bean:company,field:'category')}" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Address</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><textarea name="street1"    id="address" class="textareainpt">${company?.address?.street1?.encodeAsHTML()}</textarea></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Phone</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="phone"  value="${company?.address?.phone?.encodeAsHTML()}"   id="phone" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Toll Free</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="tollfreephone"  value="${company?.address?.tollfreephone}"   id="toll" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Fax</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="fax"  value="${company?.address?.fax?.encodeAsHTML()}"   id="fax" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Year Founder</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="founded"  value="${company?.founded?.encodeAsHTML()}"   id="yearf" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Employee Count</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="noofemployees"  value="${company?.noofemployees?.encodeAsHTML()}"   id="employee" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							</table>

						</td>
						<td align="left" valign="top">&nbsp;&nbsp;</td>
						<td align="left" valign="top" width="50%">
						   <table width="100%" border="0" cellspacing="0" cellpadding="0">
							  <tr>
								<td align="left" valign="top" class="gtext">Membership Level</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" >
									<div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="membershiplevel_msddHolder">
										<div id="filterselectM">
											<g:select id="membershiplevel" optionKey="id" from="${Membershiplevel.list()}" optionValue="description" name="membershiplevel" style="width:214px;"  value="${company?.membershiplevel?.id}" ></g:select>
										</div>
									</div>
									<div id="membershiplevel_msdd" class="dd2" style="width: 193px;">
										<div id="membershiplevel_title" class="ddTitle">
											<span id="membershiplevel_arrow" class="arrow" style="background-position: 0px 0px;"></span>
											<span class="ddTitleText" id="membershiplevel_titletext">
												<span class="ddTitleText">WTB_My Inventory</span>
											</span>
										</div>
										<div id="membershiplevel_child" class="ddChild" style="width: 191px;">
											<a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="membershiplevel_msa_0">
												<span class="ddTitleText">WTB_My Inventory</span>
											</a>
											<a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="membershiplevel_msa_1">
												<span class="ddTitleText">Demo 1</span>
											</a>
										</div>
									</div>
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Country</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" >
<g:select class="stdSelect" id="countrySelect" optionKey="id" from="${Country.list(sort:'name', order:'asc')}"  style="width:214px;"   optionValue="name" name="country"  value="${company?.address?.country?.id}"  noSelection="['':'-Please select your country-']" ></g:select>
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">State</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" >
<div id="stateSelect" >
<g:select class="stdSelect" optionKey="name" from="${flash.stateSelected}" optionValue="name" name="state" value="${company?.address?.state}"  style="width:214px;"  noSelection="['':'-Please select your State/Region-']" ></g:select>
</div>
								</td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">City</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="city" value="${company?.address?.city?.encodeAsHTML()}"  id="city" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Zip</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><input type="text" name="postalcode" value="${company?.address?.postalcode}"  id="zip" class="inptinpt" /></td>
							  </tr>
							  <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
							  <tr>
								<td align="left" valign="top" class="gtext">Timezone</td>
								<td align="left" valign="top" class="gtext"></td>
								<td align="left" valign="top" ><g:select name="timezone"
						          from="${application.timezones}"
						          value="${company.address.timezone}"
						          optionKey="key" 
							    optionValue="value"
								 /></td>
							  </tr>
							</table>
						</td>
					  </tr>
					 
					</table>
					</td>
				  </tr>
                                                         <tr>
                                                                <td align="left" valign="top" width="95" class="gtext">Company Information</td>
                                                                <td align="left" valign="top" width="2" class="gtext"></td>
                                                                <td align="left" valign="top" width="230">
                                                        <textarea name="bio" id="bio" class="tiny"  rows="40" cols="125" >${company.bio}</textarea>
                                                                </td>
                                                          </tr>
                                                          <tr><td colspan="3" height="12" align="left" valign="top"></td></tr>
                                                          <tr>
                                                                <td align="left" valign="top" width="95" class="gtext">Terms of Service</td>
                                                                <td align="left" valign="top" width="2" class="gtext"></td>
                                                                <td align="left" valign="top" width="230">
                                                                <textarea name="tos" id="terms" class="tiny"  rows="40" cols="125"   >${company.tos}</textarea>
                                                                </td>
                                                          </tr>
                                                          <tr>
                                                                <td align="left" valign="top" width="95" class="gtext"></td>
                                                                <td align="left" valign="top" width="2" class="gtext"></td>
                                                                <td align="left" valign="top" width="230">
  <div class="i-headerwrapper1">
                                                                                <ul>
                                                                                        <li>
																																													<!--<a href="javascript:void(0)" onClick="javascript:updateCompany();"  title="Home" class="active"><span>Update</span></a>-->
																																													<button type="button" class="btn btn-broadcast btn-update-company" onClick="javascript:updateCompany();">Update</button>
																																												</li>
                                                                                </ul>
                                                                                </div>



                                                                </td>
                                                          </tr>

				</table>

			 </g:form>
			</div>
		</div>
		<!--eachtab end -->
		
		<script>
			$(document).ready(function() {

				try {
					//var oHandler2 = $("#membershiplevel").msDropDown({mainCSS:'dd3'}).data("dd");
					// var oHandler2 = $("#websites2").msDropDown({mainCSS:'dd3'}).data("dd");
					// var oHandler2 = $("#websites2").msDropDown({mainCSS:'dd3'}).data("dd");
					// var oHandler2 = $("#websites2").msDropDown({mainCSS:'dd3'}).data("dd");
					
					//$("#ver").html($.msDropDown.version);
				} catch(e) {
					alert("Error: "+e.message);
				}
			});
		</script>


</html>
