

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Broadcast</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Broadcast List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Broadcast</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Broadcast</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcast}">
            <div class="errors">
                <g:renderErrors bean="${broadcast}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${broadcast?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                         
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="broadcasttype">Broadcasttype:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcast,field:'broadcasttype','errors')}">
                                    <g:select from="${['WTB','WTS']}" name="broadcasttype" value="${fieldValue(bean:broadcast,field:'broadcasttype')}" ></g:select>
                                </td>
                            </tr>  
                   
                  					    <g:render template="/tradeunit" var="tradeunit" bean="${broadcast.unit}" />
                        
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="description">Description:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:broadcast,field:'description','errors')}">
                                    <input type="text" id="description" name="description" value="${fieldValue(bean:broadcast,field:'description')}"/>
                                </td>
                            </tr>                         
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
