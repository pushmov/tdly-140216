<%@ page import="test2.MyConnection" %>
<%@page import="test2.Person" %>
<%@page import="test2.MyConnection" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
</head>

<div class="tablewrap">
<div class="eachtab" id="tab-1">
<div class="search_message">
    <div class="inbox_top2">
		       Sort by recent activity
		  </div>
		  <div class="clear"></div>

                    <g:each in="${myConnectionInstanceList}" status="i" var="myConnectionInstance">
		  <div class="inbox_main">
		    <div class="icon_img"><img src="images/icon_img.jpg" /></div>
			  <div class="con_m">
			       <div class="con_m_heading"><g:link controller="person" action="profile" id="${myConnectionInstance.contact.id}" >${myConnectionInstance.contact.firstname} ${myConnectionInstance.contact.lastname} (${myConnectionInstance.contact.address.country.name})</g:link></div>
				   <div class="con_m_para11">${myConnectionInstance.contact.company.name}</div>
				   <div class="con_m_para1">${myConnectionInstance.contact.address.city}, ${myConnectionInstance.contact.address.state}</div>
				   <div class="con_icon"><a href="#" ><img src="images/mailbox.png" alt="Mailbox" title="Mailbox" /></a></div>
              </div>
			  <div class="con_m1">
			       <div class="con_m_para11"><a href="../broadcast/search?createdById=${myConnectionInstance.contact.id}" >Broadcasts : ${Broadcast.countByCreatedby(myConnectionInstance.contact)}</a></div>
				   <div class="con_m_para11"><a href="../inventory/getInventory?pid=${myConnectionInstance.contact.id}" >Inventory : ${Inventory.countByCreatedBy(myConnectionInstance.contact)}</a></div>
				   <div class="con_m_para11"><a href="../myConnection/list?pid=${myConnectionInstance.contact.id}" >Connection : ${MyConnection.countByPerson(myConnectionInstance.contact)}</a></div>
			  </div>
			  
			  <div class="date_m"><input name="Input" type="checkbox" value="" /></div>
			  <div class="date_m">${myConnectionInstance.dateCreated}</div>
			  <div class="date_m"><g:remoteLink class="delete" controller="myConnection" action="delete" id="${myConnectionInstance?.id}" before="return confirm('Are you sure?');" ><img src="images/deletebutton.png" /></g:remoteLink></div>
			  <div class="clear"></div>
		  </div>
	</g:each>
	
<div class="pagination_n">
		       <div class="pagination_pre"><a href="#"></a></div>			    
			     <div class="pagination_num">
                  <ul>
						<li><a href="#" class="active">1 </a> </li>
						<li><a href="#" >2</a></li>						
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>		
						<li><a href="#">5</a></li>
						 
						
		         </ul>             
			     </div>
			   <div class="pagination_next"><a href="#"></a></div>
		  </div>
		  
		  <div  class="clear"></div>
	 </div>
	 
	 <div  class="clear"></div>

        </div>
            </div>
    </body>
</html>
