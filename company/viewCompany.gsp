							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
		
								<div class="forms">

                    
                        <div class="row"> 
                             
                            <td valign="top" class="value">${fieldValue(bean:company, field:'name')}</td>
                            
                        </div>
                        <div class="row">
                            <td valign="top" class="name">Category:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:company, field:'category')}</td>
                            
                        </div>

                        <div class="row">
                            <td valign="top" class="name">Membership:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:company.membershiplevel, field:'memlevel')}</td>
                            
                        </div>

					    <g:render template="/companyaddress_view" var="address" bean="${company.address}" />
                    
                        <div class="row">
                            <td valign="top" class="name">Timezone :</td>
                            
                            <td valign="top" class="value">${application.timezones.get(company.address.timezone)}</td>
                            
                        </div>

                    <input type="hidden" name="id" value="${company?.id}" />

                 
           								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><input name="" type="submit" value="Edit"/></span>
										<span class="button blue_button search_button"><input name="" type="submit" value="Add To MyVendors"/></span>
									</div>
								      </div>

                    
                </div>
								
							</fieldset>
