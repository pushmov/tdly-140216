

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>RFQQuotes List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New RFQQuotes</g:link></span>
        </div>
        <div class="body">
            <h1>RFQQuotes List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="fromid" title="Fromid" />
                        
                   	        <g:sortableColumn property="quotes" title="Quotes" />
                        
                   	        <g:sortableColumn property="respdate" title="Respdate" />
                        
                   	        <g:sortableColumn property="responsetext" title="Responsetext" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${RFQQuotesList}" status="i" var="RFQQuotes">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${RFQQuotes.id}">${fieldValue(bean:RFQQuotes, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:RFQQuotes, field:'company')}</td>
                        
                            <td>${fieldValue(bean:RFQQuotes, field:'fromid')}</td>
                        
                            <td>${fieldValue(bean:RFQQuotes, field:'quotes')}</td>
                        
                            <td>${fieldValue(bean:RFQQuotes, field:'respdate')}</td>
                        
                            <td>${fieldValue(bean:RFQQuotes, field:'responsetext')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${RFQQuotes.count()}" />
            </div>
        </div>
    </body>
</html>
