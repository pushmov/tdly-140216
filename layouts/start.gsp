<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Admin Panel</title>

        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'extjs/ext-all.css')}" />
	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'admin/admin.css')}"  />
	<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'admin/ie.css')}" /><![endif]-->
        <g:javascript src="ext-base.js" />				
        <g:javascript src="ext-all-debug.js" />				
        <g:layoutHead />
        <g:javascript library="application" />				


</head>

<body class="inner_page">
<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper">
		<div id="layout">
			<!--[if !IE]>start head<![endif]-->
			<div id="head">
				<div id="head_left">
					<div id="head_right">
						<!--[if !IE]>start site name<![endif]-->
						<div id="site_name">
							<h1><a href="/">PartsZap</a></h1>
							<h2></h2>
						</div>
						<!--[if !IE]>end site name<![endif]-->
						<!--[if !IE]>start user menu<![endif]-->
						<ul id="user_menu">
							<li class="first">Welcome John Smith</li>
							<li><a href="/test2/logout">Log out</a></li>
						</ul>
						<!--[if !IE]>end user menu<![endif]-->
					</div>
				</div>
			</div>
			<!--[if !IE]>end head<![endif]-->
			<!--[if !IE]>start main_menu<![endif]-->
			<div id="main_menu">
				<div id="main_menu_right">
					<div id="main_menu_left">
					
						<ul>
							<li><a href="/test2/broadcast/list" <g:if test="${params.controller == 'broadcast'}">id="selected"</g:if>  ><span><span>Broadcasts</span></span></a></li>
							<li><a href="/test2/inventory/list" <g:if test="${params.controller == 'inventory'}">id="selected"</g:if>  ><span><span>Inventory</span></span></a></li>
							<li><a href="/test2/myActivity/list" <g:if test="${params.controller == 'myActivity'}">id="selected"</g:if>  ><span><span>My Activity</span></span></a></li>
							<li><a href="/test2/myVendor/list" <g:if test="${params.controller == 'myVendor'}">id="selected"</g:if>  ><span><span>My Vendors</span></span></a></li>
							<li><a href="/test2/company/show"  <g:if test="${params.controller == 'company'}">id="selected"</g:if>  ><span><span>Company</span></span></a></li>
							<li><a href="/test2/person/list" <g:if test="${params.controller == 'person'}">id="selected"</g:if>  ><span><span>Employees</span></span></a></li>

							<li>
								<a href="#"><span><span>Cool Links</span></span></a>
								<div class="sub">
									<ul>
										<li><a href="#">General Settings</a></li>
										<li><a href="#">Link Categories</a></li>
										<li><a href="#">Manage Partners</a></li>
										<li><a href="#">Ad Banners Management</a></li>
										<li class="last"><a href="#">Front-end Templates</a></li>
									</ul>
								</div>
							</li>
						</ul>
					
					</div>
				</div>
			</div>
			<!--[if !IE]>end main_menu<![endif]-->
			
			<!--[if !IE]>start page<![endif]-->
			<div id="page">
					<g:layoutBody />

			</div>
			<!--[if !IE]>end page<![endif]-->
			
			<!--[if !IE]>start footer<![endif]-->
			<div id="footer">
					<p>
						Powered by YourApp v0.2 | &copy; 2008 Your Company. All rights reserved
					</p>
					<ul>
						<li><a href="#">Help</a></li>
						<li class="last"><a href="#">Contact Support</a></li>
					</ul>
			</div>
			<!--[if !IE]>end footer<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
