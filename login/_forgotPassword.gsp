<div class="modal fade broadcasts-popup" id="modal_forgot" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Forgot Password</h4>
			</div>
			
			<form action='/tdly/register/forgotPassword' method='POST' id="forgotPasswordForm" name="forgotPasswordForm" class="form-horizontal">
				<div class="modal-body">
					<h1>Reset Password Form</h1>
					<div class="form-group">
						<label for="username" class="col-sm-2 control-label"><g:message code='spring.security.ui.login.username'/></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="username" id="username">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-8">
							<button type="button" class="btn btn-primary btn-lg signupBtn" onclick="submitPassword();return false;">Reset Password</button>
						</div>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$('#username').focus();
});
</script>

