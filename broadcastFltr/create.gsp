

<%@ page import="test2.BroadcastFilter" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'broadcastFilter.label', default: 'BroadcastFilter')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${broadcastFilterInstance}">
            <div class="errors">
                <g:renderErrors bean="${broadcastFilterInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="keyword"><g:message code="broadcastFilter.keyword.label" default="Keywords" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'keyword', 'errors')}">
                                    <g:textField name="keyword" value="${broadcastFilterInstance?.keyword}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="myCountries"><g:message code="broadcastFilter.myCountries.label" default="My Countries" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'myCountries', 'errors')}">
                                    <g:checkBox name="myCountries" value="${broadcastFilterInstance?.myCountries}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="myVendors"><g:message code="broadcastFilter.myVendors.label" default="My Vendors" /></label>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'myVendors', 'errors')}">
                                    <g:checkBox name="myVendors" value="${broadcastFilterInstance?.myVendors}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="name"><g:message code="broadcastFilter.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: broadcastFilterInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${broadcastFilterInstance?.name}" />
                                </td>
                            </tr>
                        
                        
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
