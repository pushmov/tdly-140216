

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show VendorRequest</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">VendorRequest List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New VendorRequest</g:link></span>
        </div>
        <div class="body">
            <h1>Show VendorRequest</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:vendorRequest, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${vendorRequest?.company?.id}">${vendorRequest?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Createdby:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:vendorRequest, field:'createdby')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Createdon:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:vendorRequest, field:'createdon')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Vendor:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${vendorRequest?.vendor?.id}">${vendorRequest?.vendor?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${vendorRequest?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
