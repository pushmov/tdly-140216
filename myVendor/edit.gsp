

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit MyVendor</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">MyVendor List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New MyVendor</g:link></span>
        </div>
        <div class="body">
            <h1>Edit MyVendor</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${myVendor}">
            <div class="errors">
                <g:renderErrors bean="${myVendor}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${myVendor?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:myVendor,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${myVendor?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby">Createdby:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:myVendor,field:'createdby','errors')}">
                                    <input type="text" id="createdby" name="createdby" value="${fieldValue(bean:myVendor,field:'createdby')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdon">Createdon:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:myVendor,field:'createdon','errors')}">
                                    <input type="text" id="createdon" name="createdon" value="${fieldValue(bean:myVendor,field:'createdon')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="vendor">Vendor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:myVendor,field:'vendor','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="vendor.id" value="${myVendor?.vendor?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
