
<%@ page import="test2.Feed" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'feed.label', default: 'Feed')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="id" title="${message(code: 'feed.id.label', default: 'Id')}" />
                        
                            <th><g:message code="feed.broadcast.label" default="Broadcast" /></th>
                        
                            <th><g:message code="feed.connection.label" default="Connection" /></th>
                        
                            <g:sortableColumn property="type" title="${message(code: 'feed.type.label', default: 'Type')}" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${feedInstanceList}" status="i" var="feedInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${feedInstance.id}">${fieldValue(bean: feedInstance, field: "id")}</g:link></td>
                        
                            <td>${fieldValue(bean: feedInstance, field: "broadcast")}</td>
                        
                            <td>${fieldValue(bean: feedInstance, field: "connection")}</td>
                        
                            <td>${fieldValue(bean: feedInstance, field: "type")}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${feedInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
