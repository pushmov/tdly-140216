<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Edit Account</title>

				
    </head>
<br>
<br>
 <jqvalui:renderValidationScript
        for="test2.Person"
        form="personForm"
        errorClass="invalid"
        validClass="success"
        onsubmit="true"
        renderErrorsOnTop="true"
        />


		<!--eachtab start -->
		<div class="eachtab" id="tab-1">
		    <h2 class="etext">Edit Account</h2>
            <div class="formsection">
			 <g:form name="personForm" action="updateprofile" method="post" class="search_form" >
				 <input type="hidden" name="id" value="${person?.id}" />

				<g:render template="employeedetails" model="['referer':'page']" />

			 </g:form>
			</div>
		</div>
		<!--eachtab end -->


</html>
