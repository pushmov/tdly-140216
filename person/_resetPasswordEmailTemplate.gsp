Hi ${firstname},

A new kk consulting account has been created for you.

Click the url below to activate your account and select a password!

${url}

If the above URL does not work try copying and pasting it into your browser. If you continue to have problems, please feel free to contact us.

Regards,
kk consulting

