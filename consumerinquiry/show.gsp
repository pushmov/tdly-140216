

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Consumerinquiry</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Consumerinquiry List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Consumerinquiry</g:link></span>
        </div>
        <div class="body">
            <h1>Show Consumerinquiry</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Alternatephone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'alternatephone')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">City:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'city')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Comments:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'comments')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${consumerinquiryInstance?.company?.id}">${consumerinquiryInstance?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Consumeremail:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'consumeremail')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Country:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'country')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Date Created:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'dateCreated')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Firstname:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'firstname')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Lastname:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'lastname')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Partno:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'partno')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Phone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'phone')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Postalcode:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'postalcode')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">State:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:consumerinquiryInstance, field:'state')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${consumerinquiryInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
