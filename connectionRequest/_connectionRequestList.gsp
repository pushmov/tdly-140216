<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Person" %>
<%@page import="test2.MyConnection" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.Broadcast" %>
<%@page import="com.ucbl.util.PZUtil" %>


<%
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		if (session.person.address?.timezone)
                sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
%>
<div class="left_side">
     <div class="left_message">
         <!--     <div class="compose"><a href="#">Compose Message</a></div>  -->
                  <div class="sidemenu">
                  <ul>
                                                <li><a href="../broadcastResponse/list?messageType=inbox" class="${(messageType == 'inbox')?'active':''}" >Messages </a> </li>
                                                <li><a href="../connectionRequest/list?messageType=invite" class="${(messageType == 'invite')?'active':''}" >Invitations </a> </li>
                                                <li><a href="../broadcastResponse/list?messageType=sent" class="${(messageType == 'sent')?'active':''}"  >Sent</a></li>            
 
                      </ul>
            </div>
         </div>
        <div class="clear"></div>
<!--
         <div class="search_maindiv">

              <div class="search_main"><input class="search_inbox" type='text' name='search' value='Search'  onfocus="this.value=='Search'?this.value='':this.value=this.value;" onblur="this.value==''?this.value='Search':this.value=this.value;">
         </div>
         <div class="search_icon"><img src="../images/search_icon.png" /></div>
         </div>
 -->    
          <div class="clear"></div>
         </div>
         <div class="right_message">
<!--
                  <div class="inbox_top">
                       <div class="chekbox_m"><input name="" type="checkbox" value="" /></div>

                  <div class="buttons"><a href="#" >Delete</a></div>

                  <div class="message_num">
                      <div class="message_num_con">Messages</div>
                          <div class="message_num_bg"><a href="#" >5</a></div>
                          <div class="message_num_con">Invitations</div>
                          <div class="message_num_bg"><a href="#" >5</a></div>
                  </div>
                           <div class="clear"></div>
                  </div>
  -->
                  <div class="clear"></div>

                    <g:each in="${connectionRequestInstanceList}" status="i" var="connectionRequest">
  <div class="inbox_main">
                    <div class="icon_img"><img src="images/icon_img.jpg" /></div>
                          <div class="con_m">
                               <div class="con_m_heading"><g:link controller="person" action="profile" id="${connectionRequest.person?.id}" >${connectionRequest.person.firstname} ${connectionRequest.person.lastname} (${connectionRequest.person.company.address.country.name})</g:link></div>
                                   <div class="con_m_para11">${connectionRequest.person.title}</div>
                                   <div class="con_m_para1">${connectionRequest.person.company.address.city}, ${connectionRequest.person.company.address.state}</div>
				<div class="buttons1"><a href="javascript:void(0)" onClick="acceptConnection('${connectionRequest.id}', this )" id="showResp">&nbspAccept</a></div>
				<div class="buttons1"><a href="javascript:void(0)" onClick="deleteConnectionRequest('${connectionRequest.id}', this ,'${messageType}')" id="showResp">&nbspDelete</a></div>
                          </div>

                          <div class="con_m1">
                               <div class="con_m_para11"><a href="../broadcast/search?createdById=${connectionRequest.person?.id}" >Broadcasts : ${Broadcast.countByCreatedby(connectionRequest.person)}</a></div>
                                   <div class="con_m_para11"><a href="../inventory/getInventory?pid=${connectionRequest.person?.id}" >Inventory : ${Inventory.countByCreatedBy(connectionRequest.person)}</a></div>
                                   <div class="con_m_para11"><a href="../myConnection/list?pid=${connectionRequest.person?.id}" >Connection : ${MyConnection.countByPerson(connectionRequest.person)}</a></div>
                          </div>
                          <div class="clear"></div>
                  </div>
                    </g:each>
  <div  class="clear"></div>

                  <div class="pagination_n">
			<util:remotePaginate controller="connectionRequest" action="list" total="${connectionRequestInstanceTotal}"   params="[messageType:messageType,pagination:true]" update="tab-1" />
                  </div>

                  <div  class="clear"></div>
         </div>

         <div  class="clear"></div>

