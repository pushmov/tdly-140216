<script>

$(document).ready(function() {

 $('#dialog-message').dialog({
                autoOpen: false,
                height: 500,
                width: 700,
                modal: true,
                buttons: {
                            OK: function() {
                                $(this).dialog('close');
                            }

                }
             });

});

function moreMsg(id,obj,prop) {


$.get("${request.contextPath}/" + obj + "/moreMsg",{id : id, attr: prop},
                function(data,textStatus,jqXHR) {
                    $("#description").html(data.message);
                    $('#dialog-message').dialog('open');
                }
);

}
</script>

<div id="dialog-message" title="Information">
        <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                <div id="description" ></div>
        </p>
</div>

