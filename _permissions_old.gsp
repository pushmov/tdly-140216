

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="chkbcastinbox">Chkbcastinbox:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'chkbcastinbox','errors')}">
                                    <g:checkBox name="chkbcastinbox" value="${permissions?.chkbcastinbox}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="chkrfqinbox">Chkrfqinbox:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'chkrfqinbox','errors')}">
                                    <g:checkBox name="chkrfqinbox" value="${permissions?.chkrfqinbox}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="manageinventory">Manageinventory:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'manageinventory','errors')}">
                                    <g:checkBox name="manageinventory" value="${permissions?.manageinventory}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sendbroadcast">Sendbroadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'sendbroadcast','errors')}">
                                    <g:checkBox name="sendbroadcast" value="${permissions?.sendbroadcast}" ></g:checkBox>
                                </td>
                            </tr> 
