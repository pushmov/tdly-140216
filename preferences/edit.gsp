

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Preferences</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Preferences List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Preferences</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Preferences</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${preferencesInstance}">
            <div class="errors">
                <g:renderErrors bean="${preferencesInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${preferencesInstance?.id}" />
                <input type="hidden" name="version" value="${preferencesInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="communication">Communication:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:preferencesInstance,field:'communication','errors')}">
                                    <g:select optionKey="id" from="${Communication.list()}" name="communication.id" value="${preferencesInstance?.communication?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:preferencesInstance,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${preferencesInstance?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="seo">Seo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:preferencesInstance,field:'seo','errors')}">
                                    <g:checkBox name="seo" value="${preferencesInstance?.seo}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
