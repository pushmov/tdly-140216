

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Address</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Address List</g:link></span>
        </div>
        <div class="body">
            <h1>Create Address</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${address}">
            <div class="errors">
                <g:renderErrors bean="${address}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="fax">Fax:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'fax','errors')}">
                                    <input type="text" id="fax" name="fax" value="${fieldValue(bean:address,field:'fax')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="city">City:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'city','errors')}">
                                    <input type="text" id="city" name="city" value="${fieldValue(bean:address,field:'city')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="country">Country:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'country','errors')}">
                                    <input type="text" id="country" name="country" value="${fieldValue(bean:address,field:'country')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="phone">Phone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'phone','errors')}">
                                    <input type="text" id="phone" name="phone" value="${fieldValue(bean:address,field:'phone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="postalcode">Postalcode:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'postalcode','errors')}">
                                    <input type="text" id="postalcode" name="postalcode" value="${fieldValue(bean:address,field:'postalcode')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state">State:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'state','errors')}">
                                    <input type="text" id="state" name="state" value="${fieldValue(bean:address,field:'state')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="street1">Street1:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'street1','errors')}">
                                    <input type="text" id="street1" name="street1" value="${fieldValue(bean:address,field:'street1')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="street2">Street2:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:address,field:'street2','errors')}">
                                    <input type="text" id="street2" name="street2" value="${fieldValue(bean:address,field:'street2')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
