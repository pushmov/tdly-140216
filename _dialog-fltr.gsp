<%@ page import="test2.BroadcastFilter" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Company" %>
<%@page import="test2.Country" %>
   
<r:require module="multiselect" />
<r:script language="Javascript" >

	var prevSelect = null;

$(document).ready(function() {

  $('#country').multiselect();

  $('#dialog-filter').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            'Save Filter': function() {
		//submitFilter();              
                $(this).dialog('close');
            },
            Cancel: function() {
		clearForm();
                $(this).dialog('close');
            }

        },
	close : clearForm()
	
    });

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });



	      $('#broadcastFilterForm').live("submit",  function() {

                            alert($(this).serialize());
    
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFltr/save",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				location.reload( true);
    			    }
			    
                            });

        	            return false;

              });



  $("#add_reminder").click(function() {
        $.get("../broadcastFltr/create",
                function(data,textStatus,jqXHR) {
                        $('#dialog-filter').html(data)
                        $('#dialog-filter').dialog('open');

                }
             );

    });


  $("select#continents").change(function(){
      $("#countrySelect").load("../continent/updateSelectFilter",{continentid: $(this).val(),prevcontinentid : prevSelect ,countryid: $("#countries").val(), domain: 'broadcastFilter' , ajax: 'true'});
	prevSelect = $(this).val();
  });




        });

 function clearForm()
{
$(':input','#broadcastFilterForm')
.not(':button, :submit, :reset, :hidden')
.val('');
};

 function getFilter(id) {
	$.get("../broadcastFltr/get",{id: id},
		function(data,textStatus,jqXHR) {
			//$('#dialog-filter').html(data);
			//$('#dialog-filter').dialog('open');
      
      $('#modal_edit_filter #broadcastFilterForm').html(data);
      
			
                }
             );
	
 }

 function submitFilter() {
				alert($('#broadcastFilterForm').serialize());
                            $.ajax({
                            type: "POST",
                            url: "../broadcastFltr/save",
                            data: $('#broadcastFilterForm').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
                                location.reload( true);
                            }

                            });

                            return false;
 }
 
 $('.btn-filter-save').click(function(){
    submitFilter();
 });
 
 $('.btn-filter-cancel').click(function(){
    clearForm();
 });
  
 function getFilter_Bkp(id) {
	$.get("../broadcastFltr/get",{id: id},
		function(data,textStatus,jqXHR) {

			alert($.toJSON(data));

			$('#id').val(data.id);
			$('#name').val(data.name);
			$('#keyword').val(data.keyword);
			$('#partlist').val(data.partlist);

			if(data.myVendors) {
				//alert(data.myVendors);
				$('input[name="companyType"][value="myVendors"]').attr('checked', true);
			}
			if (data.keyword != 'all') {
				$('#filterForm #keyword').val(data.keyword);
			}

			if (data.broadcasttype != null) {
				var btArr = data.broadcasttype.split(',');

			
				if( $.inArray('WTB',btArr) != -1) {
					$('#wtb').attr('checked', true);
				}
				else {
					$('#wtb').attr('checked', false);
				}
			
				if( $.inArray('WTS',btArr) != -1) {
					$('#wts').attr('checked', true);
				}
				else {
					$('#wts').attr('checked', false);
				}

			}

			if (data.condition != null) {
				var conditionArr = data.condition.split(',');
				$("#conditionS").val(conditionArr);
			}

				$("#partlist").val(data.partlist);	

			$('input:radio[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('checked', true);
		
			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
			}
				$("#manufacturer").val(mfgArr);

			var companyArr = new Array(data.companies.length);	
			for(i=0;i<data.companies.length;i++) {
				companyArr[i] = data.companies[i].id;
			}
				$("#company").val(companyArr);


			var countryArr = new Array(data.countries.length);	
			for(i=0;i<data.countries.length;i++) {
				countryArr[i] = data.countries[i].id;
			}
			$("#countries").val(countryArr);

			$('#dialog-filter').dialog('open');

                }
             );
	
 }
 


</r:script>

    </head>
<div id="dialog-responseSave" title="Broadcast Response Sent"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 Broadcast/Alert Filter Saved.
	</p> 
</div>
<!--
<div id="dialog-filter" class="formwrapper">
  -->
	
<div class="modal fade broadcasts-popup" id="modal_create_filter" tabindex="-1" role="dialog" aria-labelledby="modal_create_filter" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_create_filter">Create Filter</h4>
              </div>
              <!--modal-header-->
							
							<div class="form-content">
								<g:render template="/filterDialogNew" />
							</div>
							
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
        </div>
				
				
				<div class="modal fade broadcasts-popup" id="modal_edit_filter" tabindex="-1" role="dialog" aria-labelledby="modal_edit_filter" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_edit_filter">Edit Filter</h4>
              </div>
              <!--modal-header-->
							
							<div class="form-content">
								<g:render template="/filterDialog" />
							</div>
						</div>
					</div>
				</div>
				
