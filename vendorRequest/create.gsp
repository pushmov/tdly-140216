

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create VendorRequest</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">VendorRequest List</g:link></span>
        </div>
        <div class="body">
            <h1>Create VendorRequest</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${vendorRequest}">
            <div class="errors">
                <g:renderErrors bean="${vendorRequest}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${vendorRequest?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdby">Createdby:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'createdby','errors')}">
                                    <input type="text" id="createdby" name="createdby" value="${fieldValue(bean:vendorRequest,field:'createdby')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="createdon">Createdon:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'createdon','errors')}">
                                    <input type="text" id="createdon" name="createdon" value="${fieldValue(bean:vendorRequest,field:'createdon')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="vendor">Vendor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:vendorRequest,field:'vendor','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="vendor.id" value="${vendorRequest?.vendor?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
