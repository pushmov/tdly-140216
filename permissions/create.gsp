

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Permissions</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Permissions List</g:link></span>
        </div>
        <div class="body">
            <h1>Create Permissions</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${permissions}">
            <div class="errors">
                <g:renderErrors bean="${permissions}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="chkbcastinbox">Chkbcastinbox:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'chkbcastinbox','errors')}">
                                    <g:checkBox name="chkbcastinbox" value="${permissions?.chkbcastinbox}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="chkrfqinbox">Chkrfqinbox:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'chkrfqinbox','errors')}">
                                    <g:checkBox name="chkrfqinbox" value="${permissions?.chkrfqinbox}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="manageinventory">Manageinventory:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'manageinventory','errors')}">
                                    <g:checkBox name="manageinventory" value="${permissions?.manageinventory}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="person">Person:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'person','errors')}">
                                    <g:select optionKey="id" from="${Person.list()}" name="person.id" value="${permissions?.person?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="sendbroadcast">Sendbroadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:permissions,field:'sendbroadcast','errors')}">
                                    <g:checkBox name="sendbroadcast" value="${permissions?.sendbroadcast}" ></g:checkBox>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
