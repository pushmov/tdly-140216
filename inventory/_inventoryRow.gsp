<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
		//println "TimeZone " + session.company?.address?.timezone
		if (session.company?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.company?.address?.timezone));
		}
%>

<tr id='${inventory.id}' >
	<td id="part${i}" >${fieldValue(bean:inventory.item, field:'part')}</td>
	<td id="${fieldValue(bean:inventory.item.manufacturer, field:'id')}" >${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}</td>
	<td id="${i}" >${fieldValue(bean:inventory.item, field:'condition.name')}</td>
	<td id="${i}" >${fieldValue(bean:inventory.item, field:'price')}</td>
	<td id="${i}" >${fieldValue(bean:inventory.item, field:'qty')}</td>
	<td>${sdf.format(inventory.lastUpdated)}</td>
<!--                        
                        
                            <td>${fieldValue(bean:inventory, field:'myvendorsonly')}</td>
                            <td>${fieldValue(bean:inventory, field:'searches')}</td>
                        
                            <td>${fieldValue(bean:inventory, field:'hits')}</td>
                        
                            <td>${fieldValue(bean:inventory, field:'wtb')}</td>

  -->
	<td>
		<div class="action-control">
			<a href="javascript:void(0)" class="details" id="respond" onClick="javascript:broadcast('${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition.id')}','${fieldValue(bean:inventory.item, field:'price')}','${inventory.item.qty}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >
				<i class="fa fa-wifi fa-lg"></i>Broadcast
			</a>
			
			<g:isSelf person="${inventory.createdBy}" >
			<a href="javascript:void(0)" class="details" id="respond" onClick="javascript:editInv('${inventory.id}','${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition.id')}','${fieldValue(bean:inventory.item, field:'price')}','${inventory.item.qty}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >
				<i class="fa fa-edit fa-lg"></i>Edit
			</a>
			</g:isSelf>
			
			<a href="javascript:void(0)" class="add" id="respond" onClick="javascript:addToWatchlist('${inventory.id}');" >
				<i class="fa fa-video-camera fa-lg"></i>Add To Watchlist
			</a>
			
			<g:isSelf person="${inventory.createdBy}">
				<a href="javascript:void(0)" class="add" id="respond" onClick="javascript:deleteInventory('${inventory.id}', this);">
					<i class="fa fa-trash-o fa-lg"></i>Delete
				</a>
			</g:isSelf>
		</div>
			<input type="hidden" name="id" value="${inventory?.id}" />
	</td>
</tr>
