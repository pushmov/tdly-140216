<%@page import="test2.Manufacturer" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Broadcast List</title>

    </head>
    <body> 
        <div class="body">
				
				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link   url="[controller:'broadcast',action:'list']"   ><span><span>Broadcasts</span></span></g:link></li>
								<li ><g:link url="[controller:'broadcast',action:'create']"   ><span><span>Create</span></span></g:link></li>
								<li><g:link   id="selected_search_tab"  url="[controller:'broadcast',action:'searchinit']"   ><span><span>Search</span></span></g:link></li>
<!--								<li><g:link   url="[action:'mybroadcasts']"   ><span><span>My Broadcasts</span></span></g:link></li>  -->
								<li ><g:link     url="[controller:'privatebroadcastinbox',action:'list']"   ><span><span>MyVendor Broadcasts</span></span></g:link></li>
							</ul>
						</div>
							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h4>Search Broadcasts</h4>

								<!--[if !IE]>start row<![endif]-->
								<div class="row">
								</div>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${company}">
            <div class="errors">
                <g:renderErrors bean="${company}" as="list" />
            </div>
            </g:hasErrors>

<g:form action="search" method="post" class="search_form" >

								<div class="row">
									<label>Keyword:</label>
									<div class="inputs">
										<span class="input_wrapper"><input class="text" type="text" id="name" name="searchterm" value=""/></span>
									</div>
								</div>

								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>

</g:form>

								<div class="row">
								<h4>Advanced Broadcast Search</h4>
								</div>

            <g:form action="advsearch" method="post"  class="search_form" >

								<div class="row">
									<label>Part:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input  class="text"   type="text" id="part" name="part" value=""/>
										</span>
									</div>
								</div>


								<div class="row">
									<label>Condition:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
                                    					<g:select from="${['New','Refurbished']}" name="condition" value="" ></g:select>
										</span>
									</div>
								</div>
								<div class="row">
									<label>Manufacturer:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
											<g:select from="${Manufacturer.list()}" optionKey="mfgname" optionValue="mfgname" name="mfg" value="" ></g:select>
										</span>
									</div>
								</div>
								<div class="row">
									<label>Company:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input  class="text"   type="text" id="name" name="name" value=""/>
										</span>
									</div>
								</div>
								<div class="row">
									<label>Membershiplevel:</label>
									<div class="inputs">
										<span class="input_wrapper"><input  class="text"   type="text" id="membershiplevel" name="membershiplevel" value=""/></span>
									</div>
								</div>
								<div class="row">
									<label>City:</label>
									<div class="inputs">
										<span class="input_wrapper"><input  class="text"   type="text" id="city" name="city" value=""/>
</span>
									</div>
								</div>
								<div class="row">
									<label>State:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input  class="text"   type="text" id="state" name="state" value=""/>

										</span>
									</div>
								</div>
								<div class="row">
									<label>Country:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input  class="text"   type="text" id="country" name="country" value=""/>
										</span>
									</div>
								</div>
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>

								<!--[if !IE]>end row<![endif]-->
								
								</div>
								<!--[if !IE]>end forms<![endif]-->
									
								
								<!--[if !IE]>start tooltip<![endif]-->
								<br><br><br><br><br>
								<div class="tooltip">
									<div class="tooltip_top">
										<div class="tooltip_bottom">
											<span class="pointer"></span>
											<p class="first">
												Actual search results wouldn't have tabs, but this is a template after all. Enter Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque lobortis lacus euismod urna. Praesent sed tortor. Suspendisse in lacus facilisis tellus tempus venenatis. Vivamus dolor arcu, ultrices in, 
											</p>
											<p>
												<strong>*Note :</strong> Clicking on the "More Advanced Geographic Filters"  button will open an ajax frame for further filtering so information is not lost and there are no popups used, this way. 
											</p>
										</div>
									</div>
								</div>
								<!--[if !IE]>end tooltip<![endif]-->
								
								
								
							</fieldset>
							
							
						</g:form>
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
        </div>
    </body>
</html>
