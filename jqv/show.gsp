
<%@ page import="tdly.Jqv" %>
<!doctype html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'jqv.label', default: 'Jqv')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-jqv" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-jqv" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list jqv">
			
				<g:if test="${jqvInstance?.hello}">
				<li class="fieldcontain">
					<span id="hello-label" class="property-label"><g:message code="jqv.hello.label" default="Hello" /></span>
					
						<span class="property-value" aria-labelledby="hello-label"><g:fieldValue bean="${jqvInstance}" field="hello"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${jqvInstance?.world}">
				<li class="fieldcontain">
					<span id="world-label" class="property-label"><g:message code="jqv.world.label" default="World" /></span>
					
						<span class="property-value" aria-labelledby="world-label"><g:fieldValue bean="${jqvInstance}" field="world"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${jqvInstance?.id}" />
					<g:link class="edit" action="edit" id="${jqvInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
