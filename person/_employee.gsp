<%@page import="test2.Role" %>	
<%
response.setHeader( "Pragma", "no-cache" );
response.setHeader( "Cache-Control", "no-cache" );
response.setDateHeader( "Expires", 0 );
%>
<g:hasErrors bean="${person}">
<div class="errors">
<g:renderErrors bean="${person}" as="list" />
</div>
</g:hasErrors>
<g:hasErrors bean="${person?.address}">
<div class="errors">
<g:renderErrors bean="${person?.address}" as="list" />
</div>
</g:hasErrors>
<g:form action="saveemployee" name="person" id="person" >
<div id="response">
<g:render template="employeedetails" bean="${person}" />
</div>
</g:form>