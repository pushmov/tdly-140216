<head>
<title><g:message code='spring.security.ui.resetPassword.title'/></title>
<meta name='layout' content='pz'/>
</head>

<div class="tablewrap">
<br/>
<br/>
<div class="eachtab" id="tab-1">
 <div class="formwrapper">
	<h3><g:message code='spring.security.ui.resetPassword.description'/></h4>
	<br/>
<table border="0" cellpadding="0" cellspacing="0" width="385">


	<g:form action='resetPassword' name='resetPasswordForm' autocomplete='off'>
	<g:hiddenField name='t' value='${token}'/>
 <g:hasErrors bean="${command}">
            <div class="errors">
                <g:renderErrors bean="${command}" as="list" />
            </div>
           </g:hasErrors>
                                <tr><td colspan="3" height="15"></td></tr>

             <tr>
                                        <td class="gtext" align="left" valign="top" width="100">Password</td>
                                        <td class="gtext" align="left" valign="top" width="2">&nbsp;:&nbsp;</td>
                                        <td align="left" valign="top" width="248">
                                                <input type="text" id="price"  class="inptinpt"  name="password" value="${command?.password}" />
                                        </td>
                                  </tr>
                                <tr><td colspan="3" height="15"></td></tr>


             <tr>
                                        <td class="gtext" align="left" valign="top" width="100">Password (Confirm)</td>
                                        <td class="gtext" align="left" valign="top" width="2">&nbsp;:&nbsp;</td>
                                        <td align="left" valign="top" width="248">
                                                <input type="text" id="price"  class="inptinpt"  name="password2" value="${command?.password2}" />
                                        </td>
                                  </tr>
                                <tr><td colspan="3" height="15"></td></tr>
                                  <tr>
                                        <td align="left" valign="top" width="100" class="gtext">&nbsp;</td>
                                        <td align="left" valign="top" width="7" class="gtext">&nbsp;</td>
                                        <td align="left" valign="top" width="248">
                                         <input  id="bsingle" value="Create" type="submit"  name="create" class="createbtn" />
                                        </td>
                                  </tr>


	</g:form>
</table>
	</div>
</div>
</div>
</div>
<script>
$(document).ready(function() {
	$('#password').focus();
});
</script>

