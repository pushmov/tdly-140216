<%@page import="test2.MyVendor" %>
<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Inventory" %>
<%@page import="test2.MyVendor" %>
<%@page import="test2.Broadcast" %>
<%@page import="com.ucbl.util.PZUtil" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

<title>Admin Panel</title>
</head>
<g:render template="/vendor"  />

<%
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
                if (session.person?.address?.timezone)
                sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));


%>
<main class="content" role="main">
	<div class="main-content vendor">
		<div class="container">
			
				<g:if test="${myVendorList?.size() > 0}"
				<div class="inbox_top2">Sort by <span class="drop"><span class="drop_txt">recent activity</span> <i class="fa fa-caret-down"></i>
					<ul class="dropmenu">
						<li><a href="#">Drop Menu 1</a></li>
						<li><a href="#">Drop Menu 2</a></li>
						<li><a href="#">Drop Menu 3</a></li>
						<li><a href="#">Drop Menu 4</a></li>
					</ul>
					</span>
				</div>
				</g:if>
				<g:else>
					<p>MyVendors is a feature to add companies to your Vendor list and Follow them. <br>
					Start by searching for companies or adding companies from Broadcast page.</p>
				</g:else>
			
			<div class="container">
				<g:each in="${myVendorList}" status="i" var="myVendor">
				<div class="row row-item clearfix">
					<div class="col-sm-12 col-md-1 col-lg-1">
						<div class="icon-img"><img src="images/icon_img.jpg" /></div>
					</div>
					<div class="col-sm-12 col-md-4 col-lg-4">
						<div class="con_m_heading"><a href="../company/view?id=${myVendor?.vendor?.id}" >${myVendor?.vendor?.name} (${myVendor.vendor.address.country.name})</a></div>
						<div class="con_m_para11">${myVendor.vendor.category}</div>
						<div class="con_m_para1">${myVendor.vendor.address.city}, ${myVendor.vendor.address.state}</div>
					</div>
					<div class="col-sm-12 col-md-4 col-lg-4">
						<div class="con_m_para11">
							<a href="../broadcast/search?companyId=${myVendor.vendor.id}" >Broadcasts : ${Broadcast.countByCompany(myVendor.vendor)} </a>
						</div>
						<div class="con_m_para11"><a href="../inventory/getInventoryC?cid=${myVendor.vendor.id}" >Inventory : ${Inventory.countByCompany(myVendor.vendor)}</a></div>
						<div class="con_m_para11"><a href="../person/followers?id=${myVendor.vendor.id}" > Followers : ${MyVendor.countByVendor(company)} </a></div>
						<div class="con_m1"></div>
						<div class='buttons1'>
							<button type="button" class="btn btn-broadcast btn-unfollow" id="respond" onClick="javascript:removeVendor('${myVendor.id}','');">UnFollow</button>
							<!--<a href='javascript:void(0)'  id='respond' onClick=javascript:removeVendor('${myVendor.id}','');  >UnFollow</a>-->
						</div>
					</div>
					<div class="col-sm-12 col-md-3 col-lg-3">
						<div class="date_m"><%=PZUtil.format(myVendor.dateCreated)%></div>
					</div>
				</div>
				</g:each>
				
				
				<div class="pagination_n"><g:paginate total="${myVendorTotal}" /></div>
			</div>
		</div>
	</div>
</main>
<!--inner-contrightpan end -->
<script>
	$(document).ready(function () {
		$(".drop").hover(
			function () {
			 $('ul.dropmenu').slideDown('medium');
			}, 
			function () {
				$('ul.dropmenu').slideUp('medium');
			}
		);
		
		$('.dropmenu li a').click(function(){
			
			$('.drop_txt').html($(this).text());
			$('ul.dropmenu').slideUp('medium');
			
		});
	});
</script>
			
</html>