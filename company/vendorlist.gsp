

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Company List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Company</g:link></span>
        </div>
        <div class="body">
            <h1>Company List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	                                
                   	        <th>Company</th>
                   	        <th>Level</th>
                   	        <th>Country</th>
                   	        <th>Created</th>
                   	        <th>WTB Responses</th>
                   	        <th>WTS Responses</th>
                   	        <th>RFQ Responses</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${vendorList}" >
                        <tr>

<%
	def Long cid = it[0]
%>			  
                            <td>${it[1]}</td>
                        
                            <td>${it[2]}</td>
                            <td>${it[3]}</td>
                        
                            <td>${it[4]}</td>
                        
                            <td>${it[5]}</td>

                            <td>${it[6]}</td>
                            <td>${it[7]}</td>
                            <td>
                            <g:form name="myForm" url="[action:'deletevendor',controller:'company']">
				<input type=hidden name=id value="<%=cid%>" > 
				<input type=submit name=submit value="Delete Vendor" >
	
			    </g:form>

                            </td>
                            
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
