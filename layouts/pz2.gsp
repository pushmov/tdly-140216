	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>test</title>
	<g:javascript library="jquery" plugin="jquery" /> 



    <!-- MASTER STYLESHEET-->
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/style.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/dropdown.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/ezmark.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/dd.css')}" />
<link href='http://fonts.googleapis.com/css?family=Amaranth:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <![endif]-->

        <g:layoutHead />
	<jqui:resources/>

<!-- <script src="/tdly/plugins/jquery-ui-1.8.15/jquery-ui/js/jquery-ui-1.8.15.custom.min.js" type="text/javascript" >  -->
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery-ui-1.8.16.custom.css')}" />

        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />
        <jqvalui:renderValidationScript for="test2.BroadcastFilter"  />	
	<g:javascript library="pz/tiny_mce/tiny_mce"  />
	<g:javascript library="pz/tiny_mce/jquery.tinymce"  />

        <g:javascript library="pz/jquery.databind" />				
        <g:javascript library="pz/jquery.json-2.2" />				
        <g:javascript library="pz/jquery.populate" />				
        <g:javascript library="pz/populate-page-functions" />				
        <g:javascript library="pz/jquery.parseQuery" />	
        <g:javascript library="pz/jquery.multiselect" />				
        <g:javascript library="pz/jquery.serializeObject" />				
        <g:javascript library="pz/jquery.dd" />				
        <g:javascript library="pz/jquery.ezmark.min" />				
        <g:javascript library="pz/stuHover" />				

			

	  <tooltip:resources/>	




<script language="Javascript" >

	$(document).ready(function() {

		$( "#tabs" ).tabs();

    $('#mainsearch-message').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });


/*
             $('#mainsearchform').submit( function() {

                            
			    $.ajax({
    			    type: "POST",
    			    url: "../search/search",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    }
			    
                            });

		    
        	            return false;

              });
*/

              $('#mainsearch').click( function() {
			var data = $("#mainsearchform").serialize();
			var qstr = $.parseQuery(data);

			if (qstr.searchtype == "Parts") {
				$("#mainsearchform").attr("action","../search/searchparts");
			}
			if (qstr.searchtype == "Broadcasts") {
				$("#mainsearchform").attr("action","../broadcast/search");
			}
			if (qstr.searchtype == "Inventory") {
				$("#mainsearchform").attr("action","../inventory/search");
			}
			if (qstr.searchtype == "Company") {
				$("#mainsearchform").attr("action","../myVendor/searchcompany");
			}
			if (qstr.searchtype == "People") {
				$("#mainsearchform").attr("action","../person/searchpeople");
			}


    			$("#mainsearchform").submit();
			
              });


		try {
			oHandler = $("#websites2").msDropDown({mainCSS:'dd2'}).data("dd");
			oHandler = $("#websites3").msDropDown({mainCSS:'dd2'}).data("dd");
			oHandler = $("#websites4").msDropDown({mainCSS:'dd2'}).data("dd");
			oHandler = $("#websites5").msDropDown({mainCSS:'dd2'}).data("dd");
			//alert($.msDropDown.version);
			//$.msDropDown.create("body select");
			$("#ver").html($.msDropDown.version);
		} catch(e) {
			alert("Error: "+e.message);
		}

//$('div.i-bodycontentwrapper div.eachtab').hide(); // Hide all divs
//$('div.i-bodycontentwrapper div.eachtab:first').show(); // Show the first div
//$('div.i-bodycontentwrapper ul.tabbtn li:first').addClass('active'); // Set the class of the first link to active

//$('div.i-bodycontentwrapper ul.tabbtn li a').click(function(){ //When any link is clicked

//$('div.i-bodycontentwrapper ul.tabbtn li').removeClass('active'); // Remove active class from all links
//$(this).parent().addClass('active'); //Set clicked link class to active

//var currentTab = $(this).attr('href'); // Set variable currentTab to value of href attribute of clicked link
/*if ( currentTab == '#singletab') {
	alert('single');
	$('#tabs').tabs('select', 1);
	$('#tabs-1').show();
	$('#tabs-2').hide();

}
else {
	alert('multiple');
	$('#tabs').tabs('select', 2);
	$('#tabs-2').show();
	$('#tabs-1').hide();
}
*/
//$('.i-bodycontentwrapper div.eachtab').hide(); // Hide all divs
//$(currentTab).show(); // Show div with id equal to variable currentTab
//$(currentTab).css({visibility: "visible",display: "none"}).show(400);
//return true;
//});



	})



</script>
</head>
<body>
<div class="i-header">
	<!--i-headerwrapper start -->
	<div class="i-headerwrapper">
		<img src="images/logo.png" alt="" class="logo"/>
		<ul>
		<li><a href="../broadcast/list" <g:if test="${(params.controller == 'broadcast') || (params.controller == 'broadcastFilter')}">class="active"</g:if>  ><span><span>Broadcasts</span></span></a></li>
		<li><a href="../inventory/list"  <g:if test="${params.controller == 'inventory'}">class="active"</g:if>  ><span><span>Inventory</span></span></a></li>
		<li><a href="../alert/list" <g:if test="${params.controller == 'alert'}">class="active"</g:if>  ><span><span>Alerts</span></span></a></li>
		<li><a href="../broadcastResponse/list" <g:if test="${params.controller == 'broadcastResponse'}">class="active"</g:if>  ><span><span>Inbox</span></span></a></li>
		<li><a href="../myActivity/list" <g:if test="${params.controller == 'myActivity'}">class="active"</g:if>  ><span><span>My Activity</span></span></a></li>

		<li>
			<a href="#" title="Tour" class="top_link"><span>My Network</span></a>
			 <ul class="sub">
			           <li><a href="../myVendor/list" > My Vendors</a></li>
				   <li><a href="../myConnection/list">My Connections</a></li>
		         </ul>  
		</li>

		<li><a href="../person/view"  <g:if test="${((params.controller == 'company') || (params.controller == 'person'))}">class="active"</g:if>  ><span><span>Account</span></span></a></li>
<!--		<li><a href="../preferences/wtb" <g:if test="${params.controller == 'preferences'}">class="active"</g:if>  ><span><span>Preferences</span></span></a></li>   -->
                <li><g:form name="mainsearchform" controller="search" action="search" method="post" ><g:select from="${['Parts','Broadcasts','Inventory','Company','People']}" name="searchtype" value="${flash.searchtype}" ></g:select><input name="searchterm" value="${flash.searchterm}"><input id="mainsearch" value="Search" type="button" /></g:form>
</li>
		</ul>
	</div>
	<!--i-headerwrapper end -->
</div>
<!--i-header end -->
<!--i-bodycontent start -->
<div class="lightshade">
	<div class="i-bodycontent">
		<!--i-bodycontentwrapper start -->
		<div class="i-bodycontentwrapper">

            
					<g:layoutBody />

		</div>
		<!--i-bodycontentwrapper end -->
	</div>
</div>
<!--i-bodycontent end -->
<!--i-footertop start -->
<div class="i-footertop">
	<!--i-footertopwrapper start -->
	<div class="i-footertopwrapper">
		<!--foot start -->
		<div class="foot">
	   <ul>
	       	<li class="link">
			 <div class="footdiv">
			   <h2>Links</h2>
			   <ul>
				<li><a href="#">Home</a></li>         
				<li><a href="#">Signup</a></li>          
				<li><a href="#">Tour</a></li>          
				<li><a href="#">Login</a></li>                  
				<li><a href="#">Support</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="pricing">
			 <div class="footdiv">
			   <h2>Pricing</h2>
			   <ul>
				<li><a href="#">Donec in risus sed</a></li>         
				<li><a href="#">Magna bibendum</a></li>          
				<li><a href="#">Auctor nec non lectus</a></li>          
				<li><a href="#">Sed ut arcu a enim</a></li>                  
				<li><a href="#">Aliquam vulputate</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="tour">
			 <div class="footdiv">
			   <h2>Tour</h2>
			   <ul>
				<li><a href="#">Vivamus tincidunt tortor</a></li>         
				<li><a href="#">Quis est tincidunt</a></li>          
				<li><a href="#">Lacinia. Praesent qua</a></li>          
				<li><a href="#">Commodo ut hendrerit</a></li>                  
				<li><a href="#">Ultricies, pretium eget</a></li>
			   </ul>
			 </div>
			</li>
			
			<li class="sign">
			 <div class="footdiv">
			   <h2>Sign Up/ Login</h2>
			   <p>Aenean non neque pellentesque 
				purus tempor egestas a quis 
				massa. Duis feugiat.</p>
			 </div>
			 <div class="spacer"></div>
			 <a href="#" class="signup">Sign Up/ Login</a>
			</li>
			
			<li class="support">
			 <div class="footdiv">
			   <h2>Support</h2>
			   <table width="187" border="0" cellspacing="0" cellpadding="0" class="suptable">
				  <tr>
					<td align="left" valign="top" width="59"><strong>Address:</strong></td>
					<td align="left" valign="top" width="128"><span>Nullam scelerisque 
					   porta nisi, ultrices 
					   sollicitudin lorem.</span></td>
				  </tr>
				  <tr><td colspan="2" height="10"></td></tr>
				  <tr>
					<td align="left" valign="top"><strong>Call:</strong></td>
					<td align="left" valign="top"><span>+1 234 5678</span></td>
				  </tr>
				   <tr><td colspan="2" height="10"></td></tr>
				  <tr>
					<td align="left" valign="top"><strong>Email:</strong></td>
					<td align="left" valign="top"><span><a href="#">help@tradely.com</a></span></td>
				  </tr>
				</table>

			 </div>
			</li>
		</ul>  
	 </div>
		<!--foot end -->
	</div>
	<!--i-footertopwrapper end -->
</div>
<!--i-footertop end -->

<!--i-footerbot start -->
<div class="i-footerbot">
 
</div>
<!--i-footerbot end -->


<script type="text/javascript">
$(function() {
	$('.defaultP input').ezMark();
	$('.customP input[type="checkbox"]').ezMark({checkboxCls: 'ez-checkbox-green', checkedCls: 'ez-checked-green'})
});	
</script>

</body>
</html>

