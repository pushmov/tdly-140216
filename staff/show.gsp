

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Staff</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Staff List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Staff</g:link></span>
        </div>
        <div class="body">
            <h1>Show Staff</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Aol:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'aol')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Businesstype:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'businesstype')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Cellphone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'cellphone')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Email:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'email')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Emprole:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'emprole')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Firstname:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'firstname')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Gtalk:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'gtalk')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Lastname:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'lastname')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Msn:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'msn')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Officephone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'officephone')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Password:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'password')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Tollfreephone:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'tollfreephone')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Yahoo:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:staff, field:'yahoo')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${staff?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
