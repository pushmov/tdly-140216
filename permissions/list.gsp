

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Permissions List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Permissions</g:link></span>
        </div>
        <div class="body">
            <h1>Permissions List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="chkbcastinbox" title="Chkbcastinbox" />
                        
                   	        <g:sortableColumn property="chkrfqinbox" title="Chkrfqinbox" />
                        
                   	        <g:sortableColumn property="manageinventory" title="Manageinventory" />
                        
                   	        <th>Person</th>
                   	    
                   	        <g:sortableColumn property="sendbroadcast" title="Sendbroadcast" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${permissionsList}" status="i" var="permissions">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${permissions.id}">${fieldValue(bean:permissions, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:permissions, field:'chkbcastinbox')}</td>
                        
                            <td>${fieldValue(bean:permissions, field:'chkrfqinbox')}</td>
                        
                            <td>${fieldValue(bean:permissions, field:'manageinventory')}</td>
                        
                            <td>${fieldValue(bean:permissions, field:'person')}</td>
                        
                            <td>${fieldValue(bean:permissions, field:'sendbroadcast')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Permissions.count()}" />
            </div>
        </div>
    </body>
</html>
