
<%@ page import="test2.Country" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'country.label', default: 'Country')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.name.label" default="Name" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "name")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.continent.label" default="Continent" /></td>
                            
                            <td valign="top" class="value"><g:link controller="continent" action="show" id="${countryInstance?.continent?.id}">${countryInstance?.continent?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.fips104.label" default="Fips104" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "fips104")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.iso2.label" default="Iso2" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "iso2")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.iso3.label" default="Iso3" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "iso3")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.ison.label" default="Ison" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "ison")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.internet.label" default="Internet" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "internet")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.capital.label" default="Capital" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "capital")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.mapreference.label" default="Mapreference" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "mapreference")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.nationalitysingular.label" default="Nationalitysingular" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "nationalitysingular")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.nationalityplural.label" default="Nationalityplural" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "nationalityplural")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.currency.label" default="Currency" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "currency")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.currencycode.label" default="Currencycode" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "currencycode")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.population.label" default="Population" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "population")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.title.label" default="Title" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "title")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="country.comment.label" default="Comment" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: countryInstance, field: "comment")}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${countryInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
