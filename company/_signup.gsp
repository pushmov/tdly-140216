<%@page import="test2.Membershiplevel" %>
<%@page import="test2.Country" %>

<div class="modal fade broadcasts-popup" id="modal_register" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Sign Up Form</h4>
			</div>
			
			<g:form name="signupform" action="savecompany" method="post" class="form-horizontal" >
			<div class="modal-body">
				<h1>Signup Form</h1>
				
				<g:render template="signup_err" />
				<jqvalui:renderErrors render="true" errorContainer="personErr" style="margin-bottom:10px"/>
				
				<div class="form-group">
					<label for="firstname" class="col-sm-3 control-label">First Name:</label>
					<div class="col-sm-9">
						<input type="text" name="firstname" class="form-control" value="${person?.firstname}" />
					</div>
				</div>
				
				<div class="form-group">
					<label for="lastname" class="col-sm-3 control-label">Last Name:</label>
					<div class="col-sm-9">
						<input name="lastname" type="text" class="form-control" value="${person?.lastname}" />
					</div>
				</div>
				
				<div class="form-group">
					<label for="email" class="col-sm-3 control-label">Email:</label>
					<div class="col-sm-9">
						<input name="email" type="text" class="form-control" value="${person?.email}" />
					</div>
				</div>
				
				<div class="form-group">
					<label for="company.name" class="col-sm-3 control-label">Company:</label>
					<div class="col-sm-9">
						<input type="text" name="company.name" class="form-control" value="${company?.address?.phone?.encodeAsHTML()}"  />
					</div>
				</div>
				
				<div class="form-group">
					<label for="phone" class="col-sm-3 control-label">Phone:</label>
					<div class="col-sm-9">
						<input type="text" name="phone" class="form-control" value="${company?.address?.phone?.encodeAsHTML()}" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-3 control-label">Country:</label>
					<div class="col-sm-9">
						<g:select id="countrySelect" optionKey="id" class="form-control" from="${Country.list(sort:'name', order:'asc')}" optionValue="name" name="address.country.id" value="${company?.address?.country?.id?.encodeAsHTML()}" noSelection="['':'-Please select your country-']" ></g:select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-12">
						<button type="button" name="register" class="btn btn-primary btn-lg signupBtn" onClick="javascript:submitSignup();">Register</button>
					</div>
				</div>
			
			</div>
			</g:form>
			
		</div>
	</div>
</div>