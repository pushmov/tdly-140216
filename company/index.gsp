<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Partszap</title>

<r:require module="home"/>

<script src="${createLinkTo(dir:'assets',file:'home/js/jquery-1.11.2.min.js')}"></script>
<script src="${createLinkTo(dir:'assets',file:'home/js/custom.js')}"></script>

<r:require modules="jquery-validation-ui" />
<r:layoutResources/>
<r:external uri="/js/pz/jquery-ui.js" />

<script src="${createLinkTo(dir:'assets',file:'plugins/bootstrap/bootstrap.min.js')}"></script> 

<link href="${createLinkTo(dir:'assets',file:'home/css/bootstrap.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'home/css/style.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'home/css/responsive.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'css/customstyle.css')}" rel="stylesheet">

<link href='http://fonts.googleapis.com/css?family=Roboto:400,300italic,400italic,500,500italic,700,700italic,900,900italic,100,300,100italic' rel='stylesheet' type='text/css'>


</head>

<body>
<r:layoutResources/>

<script language="javascript" >
$(document).ready(function() {
$('#dialog-login').dialog({
        autoOpen: false,
    height: 500,
        width: 700,
    modal: true,
 });
$('#dialog-forgotPassword').dialog({
        autoOpen: false,
    height: 300,
        width: 400,
    modal: true,
 });
$('#dialog-forgotPasswordConfirm').dialog({
        autoOpen: false,
    height: 300,
        width: 400,
    modal: true,
 });
 });

function authAjax()
{
var formdata = $('#loginForm').serialize();
var dataUrl = '${postUrl?postUrl:"/tdly/j_spring_security_check"}';
$('#loginForm').find('.loader-wrapper').html('<div class="text-center callout"><img src="/tdly/images/ajax-loader.gif"></div>');
jQuery.ajax({
type : 'POST',
url :  dataUrl ,
data : formdata,
success : function(response,textStatus)
{
	$('#loginForm').find('.callout').remove();
	//emptyForm();
    if(response.success) {
		var redirectUrl="${ createLink(action:'list' ,controller:'broadcast') }";
        window.location.assign(redirectUrl);
    } else {
		$('#errorLoginMsg').html(response.error);
	}
},
error : function(
XMLHttpRequest,
textStatus,
errorThrown) {
}
});
}

function submitPassword() {
var formdata = $('#forgotPasswordForm').serialize();
var dataUrl = '/tdly/register/forgotPassword'
jQuery.ajax({
type : 'POST',
url :  dataUrl ,
data : formdata,
success : function(response,textStatus)
{
//emptyForm();
   if(response.success)
   {
        alert('success');
        $('#dialog-forgotPassword').dialog('close');
        $('#dialog-forgotPasswordConfirm').dialog('open');

   }
   else
   {
        alert(response.error);
        $('#errorLoginMsg').html(response.error);
   }
},
error : function(
XMLHttpRequest,
textStatus,
errorThrown) {
}
});
}

function forgotPassword() {
//$('#dialog-login').dialog('close');
$('#modal_login').modal('hide');
//$('#dialog-forgotPassword').dialog('open');
$('#modal_forgot').modal('show');
}
</script>

<div class="container">
	<div class="header clearFix">
       <a href="#"><img src="/tdly/assets/home/images/logo.jpg" width="187" height="29" alt="" /></a>
       <div class="topNav">
       	   <div class="mobMenuIcon">
           		<span></span>
                <span></span>
                <span></span>
           </div>
           <ul class="mobMenu">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#" data-toggle="modal" data-target="#modal_register">Signup</a></li>
                <li><a href="#">Tour</a></li>
                <li><a href="#" data-toggle="modal" data-target="#modal_login">Login</a></li>
                <li><a href="#">Pricing</a></li>
                <li><a href="#">Support</a></li>
           </ul>
       </div>
    </div>
</div>

<div class="bannerOuter">
	<div class="container clearFix">
    	<div class="video">
        	<iframe width="525" height="327" src="https://www.youtube.com/embed/YvR8LGOUpNA" frameborder="0" allowfullscreen></iframe>
        </div>
        <div class="bannerCon">
            <h2>Broadcast WTB, WTS,</h2>
            <p>Search hard to Find Parts, Upload Inventory, Integrate with Website API, Connect with the Website Professional Network</p>
            <p><a href="#" class="signupBtn">SIGN UP FREE<br /> (No Credit Card Required)</a></p>
        </div>
    </div>
</div>

<div class="container ct-services">
	<div class="row">
			<div class="col-sm-4">
							<div class="service ico1">
									<h2>Single / Multipart Broadcast</h2>
									<p>Broadcast to Public / Your<br /> Private Network</p>
									<p><a href="#">Read More</a></p>
							</div><!--/service-->
			
							<div class="service ico2">
									<h2>Creat MyVendor<br /> Company List</h2>
									<p>Create people Connections (MyConnections)</p>
									<p><a href="#">Read More</a></p>
							</div><!--/service-->  
			</div>
			<div class="col-sm-4">
								<div class="service ico3">
									<h2>Single / Multipart Broadcast</h2>
									<p>Broadcast to Public / Your<br /> Private Network</p>
									<p><a href="#">Read More</a></p>
								</div><!--/service-->
							 
							<div class="service ico4">
									<h2>Easy System Integration</h2>
									<p>Inventory API, Broadcast<br /> API and much more</p>
									<p><a href="#">Read More</a></p>
							</div>
			</div>
			<div class="col-sm-4">
							<div class="service ico5">
									<h2>Single / Multipart Broadcast</h2>
									<p>Broadcast to Public / Your<br /> Private Network</p>
									<p><a href="#">Read More</a></p>
							</div><!--/service-->
							 
							<div class="service ico6">
									<h2>Easy System Integration</h2>
									<p>Inventory API, Broadcast<br /> API and much more</p>
									<p><a href="#">Read More</a></p>
							</div>
			</div>
	</div>
    
    
    
</div><!--/wrapper-->

<div class="container">
	<div class="tesmOuter">
    	<h2>What Our Customers Say</h2>
        <div class="tesm">
   	    	<img src="/tdly/assets/home/images/tesm-pic.jpg" width="164" height="161" alt="" />
          <div class="quote">
           	<p>"Website helped me to make new Trade Connections Locate hard to find parts and Market my Inventory"</p>
             <img src="/tdly/assets/home/images/tesm-curve.png" width="64" height="18" alt="" />
          </div><!--/quote-->
          <h3>- John Doe, VP ABC</h3>
        </div><!--/tesm-->
    </div><!--/tesmOuter-->
</div>

<footer class="footer">
  <div class="footer-pannel">
    <div class="container">
      <div class="row">
        <div class="footer-block">
          <h4>Links</h4>
          <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Signup</a></li>
            <li><a href="#">Tour</a></li>
            <li><a href="#">Login</a></li>
            <li><a href="#">Support</a></li>
          </ul>
        </div>
        <div class="footer-block bullet-block">
          <h4>Pricing</h4>
          <ul>
            <li><a href="#">Nam libero tempore </a></li>
            <li><a href="#">Solutanobis est</a></li>
            <li><a href="#">Eligendioptio cumque</a></li>
            <li><a href="#">Nihilimpedit quo minus id </a></li>
            <li><a href="#">Placeat facere enim.</a></li>
          </ul>
        </div>
        <div class="footer-block bullet-block">
          <h4>Tour</h4>
          <ul>
            <li><a href="#">Modi tempora incidunt</a></li>
            <li><a href="#">Neque porro quisquam est</a></li>
            <li><a href="#">Eligendioptio cumque</a></li>
            <li><a href="#">Nisi ut aliquid ex ea </a></li>
            <li><a href="#">Sed ut perspiciatis unde</a></li>
          </ul>
        </div>
        <div class="footer-block">
          <h4>Sign up / Login</h4>
          <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
          <input type="button" class="btn btn-md btn-primary signup" value="SIGN UP/ LOGIN">
        </div>
        <div class="footer-block">
          <h4>Support</h4>
          Address<br>
          Neque porro quisquam est, qui dolorem.<br>
          Call<br>
          +1 234 5678<br>
          Email<br>
          <a href="mailto:help@test56tyg.com">help@test56tyg.com</a> </div>
      </div>
    </div>
  </div>
  <div class="copy-right">
    <div class="container">
      <div class="row">Copyright © Website. All Rights Reserved.  Terms of Service    Privacy Policy</div>
    </div>
  </div>
</footer>

<g:render template="signup" />
<g:render template="/login/auth" />
<g:render template="/login/forgotPassword" />
<div id="dialog-forgotPasswordConfirm" title="Password Reset Confirm">
        <p>
                <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
                Password Reset Email sent.
        </p>
</div>

<%
 if (session.person) {
	response.sendRedirect("broadcast/list")
 }
%>
 
</body>
</html>
