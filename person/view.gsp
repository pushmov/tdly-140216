
<%@page import="test2.Person" %>
        <meta name="layout" content="pz" />
		<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">
		<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

<script>
        var person;
	$(document).ready(function() {

	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 150,
        	width: 250,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	  

  $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

    		$("#resetPasswordForm").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });




       });

function submitPerson() {
                    $.ajax({
                            type: "POST",
                            url: "../person/update",
                            data: $('#person').serialize(),
                            error: function(data, textStatus, jqXHR) {
                                //alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
					$('#dialog-employee').dialog('close');
					window.location.reload();
                            }

                    });


}

function editPerson(id) {

/*
	$.get("../person/showajax",{id: id, returnTemplate: 'view'},

		function(data,textStatus,jqXHR) {
			
		$("#dialog-employee").html(data);
		        
			$("#dialog-employee").dialog("open");
		}

        );
*/
		$('#modal_edit_employee').modal('show');
		
		$.ajax({
                            type: "GET",
                            url: "../person/showajax",
                            cache: false,
                            data: {id: id, returnTemplate: 'row'},
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
                                        $("#response").html(data);
                                        setupFormValidation();
                                        //person.resetForm();
                                        //$("#dialog-employee").dialog("open");
										
										

                            }

                });


}

function openResetPassword() {
		        
	$("#dialog-resetPassword").dialog("open");

}


function addConnection(id) {

			    $.ajax({
    			    type: "POST",
    			    url: "../connectionRequest/save",
			    cache: false,
    			    data: { id : id , message : 'Hi I would like to add you to my connections Thanks' },
			    error: function(data, textStatus, jqXHR) {
				   $('#respMessage').val(data.msg);
				   var foo = $('#dialog-message').dialog("open");
				   foo.html(data.msg);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $('#respMessage').val(data.msg);
				   var foo = $('#dialog-message').dialog("open");
				   foo.html(data.msg);
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
        			
    			    }
			    
                            });

}

$(document).on('click', '#saveemployee', function(){
	$.ajax({
		type: "POST",
        url: "../person/saveemployee",
        data: $("#person").serialize(),
        success: function(data, textStatus, jqXHR) {
			if(data.indexOf("errors") != -1)
				$('#dialog-employee').html(data);
            else {
				$('#dialog-employee').dialog('close');
                window.location.reload();
            }
        },
        error: function(data, textStatus, jqXHR) {
			alert("Error: " + data);
        }
    });
});




</script>


<main class="content" role="main">
	<div class="main-content person-view">
		<div class="container">
		<g:render template="countryJs"  bean="${person}"   />
		<g:render template="employeeview" />
		</div>
	</div>
</main>