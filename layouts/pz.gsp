<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js sidebar-large lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js sidebar-large lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js sidebar-large lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js sidebar-large">
<head>
<!-- BEGIN META SECTION -->
<meta charset="utf-8">
<title>test</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="" name="description" />
<meta content="themes-lab" name="author" />
<link rel="shortcut icon" href="assets/img/favicon.png">
<!-- END META SECTION -->
<!-- BEGIN MANDATORY STYLE -->

<link href="${createLinkTo(dir:'assets',file:'css/icons/icons.min.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'css/bootstrap.min.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'css/plugins.min.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'css/style.min.css')}" rel="stylesheet">
<link href="#" rel="stylesheet" id="theme-color">
<link href="${createLinkTo(dir:'assets',file:'css/customstyle.css')}" rel="stylesheet">
<link href="${createLinkTo(dir:'assets',file:'css/custom.css')}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
<!-- END  MANDATORY STYLE -->

<!-- BEGIN MANDATORY SCRIPTS --> 
<script src="${createLinkTo(dir:'assets',file:'plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js')}"></script>
<script src="${createLinkTo(dir:'assets',file:'plugins/jquery-1.11.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/jquery-migrate-1.2.1.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/jquery-ui/jquery-ui-1.10.4.min.js')}"></script> 

<script src="${createLinkTo(dir:'assets',file:'plugins/bootstrap/bootstrap.min.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/bootstrap-select/bootstrap-select.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/mmenu/js/jquery.mmenu.min.all.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/charts-sparkline/sparkline.min.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/breakpoints/breakpoints.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/numerator/jquery-numerator.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/jquery.cookie.min.js')}" type="text/javascript"></script> 
<!-- END MANDATORY SCRIPTS --> 


<script language="Javascript" >
	$(document).ready(function() {
		
		$('.pagination_n').wrap('<div class="pagination_w"></div>');

		$( "#tabs" ).tabs();
       $(".dropdown1 img.flag").addClass("flagvisibility1");

            $(".dropdown1 dt a").live("click",function() {
                $(".dropdown1 dd ul").toggle();
            });

            $(".dropdown1 dd ul li a").live("click",function() {
                var text = $(this).html();
                $(".dropdown1 dt a span").html(text);
		$.get("../profile/ddmenu?menu="+ text,
                                function(data,textStatus,jqXHR) {
                                        $("#searchddmenu").html(data);
                                }
                );
                $(".dropdown1 dd ul").hide();
            });

            function getSelectedValue(id) {
                return $("#" + id).find("dt a span.value").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown1"))
                    $(".dropdown1 dd ul").hide();
            });
						
						function getParameterByName(name) {
								name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
								var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
										results = regex.exec(location.search);
								return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
						}
						
			function submit_search_form(){
				var data = $("#mainsearchform").serialize();
				var searchtype = $("#searchtype").val();
                if (searchtype == "Parts") {
					$("#mainsearchform").attr("action","${request.contextPath}/search/searchparts");
                }
                if (searchtype == "Broadcasts") {
					$("#mainsearchform").attr("action","${request.contextPath}/broadcast/search");
                }
                if (searchtype == "Inventory") {
					$("#mainsearchform").attr("action","${request.contextPath}/inventory/search");
                }
                if (searchtype == "Company") {
					$("#mainsearchform").attr("action","${request.contextPath}/myVendor/searchcompany");
                }
                if (searchtype == "People") {
					$("#mainsearchform").attr("action","${request.contextPath}/person/searchpeople");
                }
				$("#mainsearchform").submit();
			}

			$('form#mainsearchform').on('submit', submit_search_form);

	})
function showMore(){
$(function(){

// The height of the content block when it's not expanded
var adjustheight = 50;
// The "more" link text
var moreText = "+  More";
// The "less" link text
var lessText = "- Less";
    
// The section added to the bottom of the "more-less" div

$(".showmore .moreblock").each(function(){ 
if ($(this).height() > adjustheight){
//$(".showmore").append('<p class=continued>[&hellip;]</p><a href=# class=adjust></a>');
$(this).css('height', adjustheight).css('overflow', 'hidden');
$(this).parent(".showmore").append('<p class=continued>[&hellip;]</p><a href=# class=adjust></a>');
$(this).parent(".showmore").find("a.adjust").text(moreText);
$(this).parent(".showmore").find(".adjust").toggle(function() {
$(this).parents("div:first").find(".moreblock").css('height', 'auto').css('overflow', 'visible');
$(this).parents("div:first").find("p.continued").css('display', 'none');
$(this).text(lessText);
}, function() {
$(this).parents("div:first").find(".moreblock").css('height', adjustheight).css('overflow', 'hidden');
$(this).parents("div:first").find("p.continued").css('display', 'block');
$(this).text(moreText);
});
}
});

});
}

</script>
<!-- old assets start here -->



<r:require module="pz"/>

        <g:layoutHead />
<r:layoutResources/>
    <!-- MASTER STYLESHEET-->
		
				<!--  new block for assets resources -->
				<link rel="stylesheet" href="${createLinkTo(dir:'assets',file:'assets.css')}" />
				
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/demo.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/table.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/pro_dropdown_2.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/ezmark.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/dd.css')}" />
<link href='http://fonts.googleapis.com/css?family=Amaranth:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <![endif]-->

<!-- <script src="/tdly/plugins/jquery-ui-1.8.15/jquery-ui/js/jquery-ui-1.8.15.custom.min.js" type="text/javascript" >  -->
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/styleinner.css')}" />
 <!--       <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery-ui-1.8.16.custom.css')}" />  -->
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery-ui.css')}" /> 
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery.wysiwyg.css')}" />
        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'pz/jquery.wysiwyg.modal.css')}" />
        <!--<link rel="stylesheet" href="${createLinkTo(dir:'css',file:'bootstrap/css/font-awesome.css')}" />-->
<r:external uri="/js/pz/jquery-ui.js" />
<r:external uri="/js/pz/tiny_mce/jquery.tinymce.min.js" disposition="defer" />
<r:external uri="/js/pz/tiny_mce/tinymce.min.js" disposition="defer" />
<r:external uri="/js/pz/jquery.cookie.min.js" disposition="defer" />
<r:external uri="/js/pz/jquery.collapsible.min.js" disposition="defer" />
<r:external uri="/js/pz/highlight.pack.js" disposition="defer" />
<r:external uri="/js/pz/jquery.dd.js" disposition="defer" />
<r:external uri="/js/pz/jquery.ezmark.min.js" disposition="head" />
<r:external uri="/js/pz/enhance.js" disposition="head" />
<r:external uri="/js/pz/jquery.fileinput.js" disposition="head" />
<r:external uri="/js/pz/jquery.customInput.js" disposition="head" />
<r:external uri="/js/pz/jquery.checkbox.js" disposition="head" />
<r:external uri="/js/pz/jquery.filterList.min.js" disposition="head" />


</head>
<body data-page="profil">

<nav class="navbar navbar-inverse navbar-fixed-top profile-header" role="navigation">
  <div class="container-fluid">
  	<button aria-controls="navbar" aria-expanded="true" data-target="#navbar" data-toggle="collapse" class="navbar-toggle" type="button">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
     <div class="clearfix"></div>       
  	<div id="navbar" class="navbar-collapse collapse" aria-expanded="true">
    
    <div class="nav-search">
			<g:form name="mainsearchform" controller="search" action="search" method="get">
      <div class="input-group">
        <div class="input-group-btn search-panel">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
						<span id="search_concept">${(flash.searchtype == null)?"Parts":flash.searchtype}</span>
						<i class="fa fa-angle-down p-r-10"></i>
					</button>
          <ul class="dropdown-menu" role="menu">
						<g:if test="${(flash.searchtype != "Parts") && (flash.searchtype != null) }" >
            <li><a href="#parts" data-val="Parts">Parts</a></li>
						</g:if>
						<g:if test="${flash.searchtype != "Broadcasts" }" >
            <li><a href="#broadcasts" data-val="Broadcasts">Broadcasts</a></li>
						</g:if>
						<g:if test="${flash.searchtype != "Inventory" }" >
            <li><a href="#inventory" data-val="Inventory">Inventory</a></li>
						</g:if>
						<g:if test="${flash.searchtype != "Company" }" >
            <li><a href="#company" data-val="Company">Company</a></li>
						</g:if>
						<g:if test="${flash.searchtype != "People" }" >
            <li><a href="#people" data-val="People">People</a></li>
						</g:if>
          </ul>
        </div>
				<g:hiddenField name="searchtype" value="${flash.searchtype?flash.searchtype:"Parts"}" />
        <input type="text" class="form-control search-bar" name="keyword" id="search" placeholder="Search" value="${flash.keyword}">
        <span class="input-group-btn">
        <button name="searbtn" id="mainsearch" class="btn btn-default search-btn" type="submit"><span class="glyphicon glyphicon-search"></span></button>
        </span>
			</div>
			</g:form>
    </div>
		
		<script>
			$('.nav-search ul.dropdown-menu > li > a').click(function(){
				$('.nav-search #search_concept').text($(this).text());
				$('.nav-search #searchtype').val($(this).attr('data-val'));
			});
		</script>
    
    <div class="navbar-top-menu">
    
      <ul class="nav navbar-nav">
        <li class="dropdown"> 
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">broadcasts <i class="fa fa-angle-down "></i></a>
          <ul class="dropdown-menu">
            <li><g:link id="selected_search_tab" url="[controller:'broadcast', action:'list']">All Broadcasts</g:link></li>
            <li><g:link url="[controller:'broadcast', action:'myconnections']">MyConnection Broadcasts</g:link></li>
            <li><g:link url="[controller:'broadcast', action:'create']">Create</g:link></li>
            <li><g:link id="selected_search_tab" url="[controller:'broadcastFltr', action:'list']">Filters</g:link></li>
          </ul>
				</li>
				<li class="dropdown"> 
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">inventory <i class="fa fa-angle-down "></i></a>
          <ul class="dropdown-menu">
            <li><g:link id="selected_search_tab" url="[controller:'inventory', action:'list']">List</g:link></li>
            <li><g:link url="[controller:'inventory', action:'upload']">Upload</g:link></li>
            <li><g:link url="[controller:'inventory', action:'watchlist']">Watchlist</g:link></li>
          </ul>
				</li>
        <li><a href="${request.contextPath}/alert/list">alerts</a></li>
        <li><a href="${request.contextPath}/broadcastResponse/list">inbox</a></li>
        <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">my network <i class="fa fa-angle-down "></i></a>
          <ul class="dropdown-menu">
						<li><a href="${request.contextPath}/myVendor/list" >My Vendors</a></li>
						<li><a href="${request.contextPath}/myConnection/list">My Connections</a></li>
          </ul>
        </li>
      </ul>
    </div>
    
    <div class="notification-bar"> 
      <!-- BEGIN TOP NAVIGATION MENU -->
      <ul class="nav navbar-nav pull-right header-menu">
        <!-- BEGIN NOTIFICATION DROPDOWN -->
        <li class="dropdown" id="notifications-header"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="glyph-icon flaticon-notifications"></i> <span class="badge badge-danger badge-header">6</span> </a>
          <ul class="dropdown-menu">
            <li class="dropdown-header clearfix">
              <p class="pull-left">Notifications</p>
            </li>
            <li>
              <ul class="dropdown-menu-list withScroll" data-height="220">
                <li> <a href="#"> <i class="fa fa-star p-r-10 f-18 c-orange"></i> Steve have rated your photo <span class="dropdown-time">Just now</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-heart p-r-10 f-18 c-red"></i> John added you to his favs <span class="dropdown-time">15 mins</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-file-text p-r-10 f-18"></i> New document available <span class="dropdown-time">22 mins</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-picture-o p-r-10 f-18 c-blue"></i> New picture added <span class="dropdown-time">40 mins</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-bell p-r-10 f-18 c-orange"></i> Meeting in 1 hour <span class="dropdown-time">1 hour</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-bell p-r-10 f-18"></i> Server 5 overloaded <span class="dropdown-time">2 hours</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-comment p-r-10 f-18 c-gray"></i> Bill comment your post <span class="dropdown-time">3 hours</span> </a> </li>
                <li> <a href="#"> <i class="fa fa-picture-o p-r-10 f-18 c-blue"></i> New picture added <span class="dropdown-time">2 days</span> </a> </li>
              </ul>
            </li>
            <li class="dropdown-footer clearfix"> <a href="#" class="pull-left">See all notifications</a> <a href="#" class="pull-right"> <i class="fa fa-cog"></i> </a> </li>
          </ul>
        </li>
        <!-- END NOTIFICATION DROPDOWN --> 
        <!-- BEGIN MESSAGES DROPDOWN -->
        <li class="dropdown" id="messages-header"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="glyph-icon flaticon-email"></i> <span class="badge badge-primary badge-header"> 8 </span> </a>
          <ul class="dropdown-menu">
            <li class="dropdown-header clearfix">
              <p class="pull-left"> Messages </p>
            </li>
            <li class="dropdown-body">
              <ul class="dropdown-menu-list withScroll" data-height="220">
                <li class="clearfix"> <span class="pull-left p-r-5"> <img src="/tdly/assets/img/avatars/avatar3.png" alt="avatar 3"> </span>
                  <div class="clearfix">
                    <div> <strong>Alexa Johnson</strong> <small class="pull-right text-muted"> <span class="glyphicon glyphicon-time p-r-5"></span>12 mins ago </small> </div>
                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                  </div>
                </li>
                <li class="clearfix"> <span class="pull-left p-r-5"> <img src="/tdly/assets/img/avatars/avatar4.png" alt="avatar 4"> </span>
                  <div class="clearfix">
                    <div> <strong>John Smith</strong> <small class="pull-right text-muted"> <span class="glyphicon glyphicon-time p-r-5"></span>47 mins ago </small> </div>
                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                  </div>
                </li>
                <li class="clearfix"> <span class="pull-left p-r-5"> <img src="/tdly/assets/img/avatars/avatar5.png" alt="avatar 5"> </span>
                  <div class="clearfix">
                    <div> <strong>Bobby Brown</strong> <small class="pull-right text-muted"> <span class="glyphicon glyphicon-time p-r-5"></span>1 hour ago </small> </div>
                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                  </div>
                </li>
                <li class="clearfix"> <span class="pull-left p-r-5"> <img src="/tdly/assets/img/avatars/avatar6.png" alt="avatar 6"> </span>
                  <div class="clearfix">
                    <div> <strong>James Miller</strong> <small class="pull-right text-muted"> <span class="glyphicon glyphicon-time p-r-5"></span>2 days ago </small> </div>
                    <p>Lorem ipsum dolor sit amet, consectetur...</p>
                  </div>
                </li>
              </ul>
            </li>
            <li class="dropdown-footer clearfix"> <a href="mailbox.html" class="pull-left">See all messages</a> <a href="#" class="pull-right"> <i class="fa fa-cog"></i> </a> </li>
          </ul>
        </li>
        <!-- END MESSAGES DROPDOWN --> 
				
        <!-- BEGIN USER DROPDOWN -->
        <li class="dropdown" id="user-header">
				<a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
					<span class="username">${session.person?.firstname} ${session.person?.lastname}</span>
						<i class="fa fa-angle-down p-r-10"></i>
				</a>
          <ul class="dropdown-menu">
            <li>
							<g:link  id="selected_search_tab" url="[controller:'person' , action:'view']"><i class="glyph-icon flaticon-account"></i>My Profile</g:link>
						</li>
            <li>
							<g:link  id="selected_search_tab" url="[controller:'company' , action:'show']"><i class="glyph-icon flaticon-account"></i>My Company</g:link>
						</li>
            <li>
							<g:link  id="selected_search_tab" url="[controller:'person' , action:'list']"><i class="glyph-icon flaticon-account"></i>My Employees</g:link>
						</li>
            <li class="dropdown-footer clearfix">
							<a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
								<i class="glyph-icon flaticon-fullscreen3"></i>
							</a>
							<a href="lockscreen.html" title="Lock Screen">
								<i class="glyph-icon flaticon-padlock23"></i>
							</a>
							<g:link url="[controller:'logout']">
								<i class="fa fa-power-off"></i>
							</g:link>
						</li>
          </ul>
        </li>
        <!-- END USER DROPDOWN -->
      </ul>
      <!-- END TOP NAVIGATION MENU --> 
    </div>
    </div>
  </div>
</nav>
	
<!--i-bodycontent start -->
<div id="wrapper-inner">
	<g:layoutBody />
	<r:layoutResources disposition="defer" />
</div>
<!--i-bodycontent end -->

<!--i-footerbot end -->
<footer class="footer">
  <div class="footer-pannel">
    <div class="container">
      <div class="row">
        <div class="footer-block">
          <h4>Links</h4>
          <ul>
            <li><a href="#">Home</a></li>
            <li><a href="#">Signup</a></li>
            <li><a href="#">Tour</a></li>
            <li><a href="#">Login</a></li>
            <li><a href="#">Support</a></li>
          </ul>
        </div>
        <div class="footer-block bullet-block">
          <h4>Pricing</h4>
          <ul>
            <li><a href="#">Nam libero tempore </a></li>
            <li><a href="#">Solutanobis est</a></li>
            <li><a href="#">Eligendioptio cumque</a></li>
            <li><a href="#">Nihilimpedit quo minus id </a></li>
            <li><a href="#">Placeat facere enim.</a></li>
          </ul>
        </div>
        <div class="footer-block bullet-block">
          <h4>Tour</h4>
          <ul>
            <li><a href="#">Modi tempora incidunt</a></li>
            <li><a href="#">Neque porro quisquam est</a></li>
            <li><a href="#">Eligendioptio cumque</a></li>
            <li><a href="#">Nisi ut aliquid ex ea </a></li>
            <li><a href="#">Sed ut perspiciatis unde</a></li>
          </ul>
        </div>
        <div class="footer-block">
          <h4>Sign up / Login</h4>
          <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
          <input type="button" class="btn btn-md btn-primary signup" value="SIGN UP/ LOGIN">
        </div>
        <div class="footer-block">
          <h4>Support</h4>
          Address<br>
          Neque porro quisquam est, qui dolorem.<br>
          Call<br>
          +1 234 5678<br>
          Email<br>
          <a href="mailto:help@myindiainsurance.com">help@myindiainsurance.com</a> </div>
      </div>
    </div>
  </div>
  <div class="copy-right">
    <div class="container">
      <div class="row">Copyright © Website. All Rights Reserved.  Terms of Service    Privacy Policy</div>
    </div>
  </div>
</footer>

<div class="modal fade broadcasts-popup loading-popup" id="modal-loader" tabindex="-1" role="dialog" aria-labelledby="loading" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title" id="loading">Loading</h4>
			</div>
			<!--modal-header-->
			<div class="modal-body">
				<p><img src="/tdly/images/ajax-loader.gif" /> Loading...</p>
			</div>
		</div>
	<!--modal-content-->
	</div>
	<!--modal-dialog-->
</div>

<script type="text/javascript">
$(function() {
	$('.defaultP input').ezMark();
	$('.customP input[type="checkbox"]').ezMark({checkboxCls: 'ez-checkbox-green', checkedCls: 'ez-checked-green'})
});	
</script>
<script src="${createLinkTo(dir:'assets',file:'plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'plugins/nprogress/nprogress.js')}"></script> 
<script src="${createLinkTo(dir:'assets',file:'js/application.js')}"></script>
</body>
</html>

