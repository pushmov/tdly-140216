<%@ page import="tdly.Jqv" %>



<div class="fieldcontain ${hasErrors(bean: jqvInstance, field: 'hello', 'error')} ">
	<label for="hello">
		<g:message code="jqv.hello.label" default="Hello" />
		
	</label>
	<g:textField name="hello" value="${jqvInstance?.hello}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: jqvInstance, field: 'world', 'error')} ">
	<label for="world">
		<g:message code="jqv.world.label" default="World" />
		
	</label>
	<g:textField name="world" value="${jqvInstance?.world}"/>
</div>

