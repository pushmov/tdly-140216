<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if (session.person?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
		}
			
%>
<!-- <div id=tableData>
	<div id="filter">
 -->
			<div class="tab-content-right">
				<div class="tabToggle">
					<a href="#" class="arrow-icon"></a>
				</div>
				
				<div class="tab-filter-info">
					<dl class="dl-horizontal clearfix">
						<dt>Condition:</dt><dd><g:each in="${condition}" >${it},&nbsp;</g:each></dd>
						<dt>Country :</dt><dd><g:each in="${countrySel}" >${it},&nbsp;</g:each></dd>
						<dt>Company :</dt><dd><g:each in="${companySel}" >${it},&nbsp;</g:each></dd>
						<dt>Manufacturer :</dt><dd><g:each in="${manufacturerSel}" >${it},&nbsp;</g:each></dd>
						<dt>Partlist :</dt><dd>${partlistSel?.equals("all")?"":partlistSel}</dd>
					</dl>
				</div>
		
				<g:render template="broadcastRowsSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,filterParams:filterParams, broadcastTotal:broadcastTotal]}" />
				
				<script>
					$('.tabToggle a').on('click', function(event){
					
						event.preventDefault();
						$(this).toggleClass('is-active');
						var contentBlock = $(this).closest('.tab-content-block').toggleClass('is-open');
					});
				</script>
	
		<div class="inner-contrightpan" style="display:none">
			<span class="collapse-icon collapse-icon-left"><i class="fa fa-angle-double-left fa-lg"></i></span>
				<table width="90%" border="0" cellspacing="0" cellpadding="0" align="center" >
				   <tr>
					<td width="12%" align="left" valign="top" >Condition</td>
					<td width="1%"  align="left" valign="top" >:</td>
					<td width="70%"  align="left" valign="top" class="asstext"><g:each in="${condition}" >${it},&nbsp;</g:each></td>
				  </tr>
				  <tr>
					<td align="left" valign="top" >Country</td>
					<td align="left" valign="top" >:</td>
					<td align="left" valign="top" class="asstext"><g:each in="${countrySel}" >${it},&nbsp;</g:each> </td>
				  </tr>
				  <tr>
					<td align="left" valign="top" >Company</td>
					<td align="left" valign="top" >:</td>
					<td align="left" valign="top" class="asstext"><g:each in="${companySel}" >${it},&nbsp;</g:each></td>
				  </tr>
				   <tr>
					<td align="left" valign="top" >Manufacturer</td>
					<td align="left" valign="top" >:</td>
					<td align="left" valign="top" class="asstext"><g:each in="${manufacturerSel}" >${it},&nbsp;</g:each></td>
				  </tr>
				   <tr>
					<td align="left" valign="top" >Partlist</td>
					<td align="left" valign="top" >:</td>
					<td align="left" valign="top" class="asstext">${partlistSel?.equals("all")?"":partlistSel}</td>
				  </tr>

				</table>
                <div class="spacer"></div>
<div id="tableRow" >
          <g:render template="broadcastRowsSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,filterParams:filterParams, broadcastTotal:broadcastTotal]}" />
</div>
		</div>
			<!--inner-contrightpan end -->


			
			<script>
				$('.collapse-icon-left').click(function(){
					
					if(!$(this).hasClass('collapse-close')) {
						$('.inner-contleftpan').animate({
							width:"0"
						}, 1000, function(){
							$('.collapse-icon-left').addClass('collapse-close');
							$('.collapse-icon-left').find('i').removeClass('fa-angle-double-left').addClass('fa-angle-double-right');
							$('.inner-contrightpan').css({'width':'96%'});
						});
					} else {
						
						$('.inner-contrightpan').css({'width':'79%'});
						
						$('.inner-contleftpan').animate({
							width:"+=17%"
						}, 1000, function(){
							
							$('.collapse-icon-left').removeClass('collapse-close');
							$('.collapse-icon-left').find('i').removeClass('fa-angle-double-right').addClass('fa-angle-double-left');
							
						});
					}
				});
				
				// $('.collapse-close').click(function(){
					
					
					
				// });
			</script>