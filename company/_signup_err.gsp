        <div id="signup_err" class="alert-danger">
           <g:hasErrors bean="${rep?.objects?.get('person')}">
            <div class="errors">
                <g:renderErrors bean="${rep?.objects?.get('person')}" as="list" />
            </div>
           </g:hasErrors>
           <g:hasErrors bean="${rep?.objects?.get('company')}">
            <div class="errors">
                <g:renderErrors bean="${rep?.objects?.get('company')}" as="list" />
            </div>
           </g:hasErrors>
           <g:hasErrors bean="${rep?.objects?.get('address')}">
            <div class="errors">
                <g:renderErrors bean="${rep?.objects?.get('address')}" as="list" />
            </div>
           </g:hasErrors>
        </div>

