<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy hh:mm a");
//		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<script>
$(document).ready(function()
        {

        showMore();

});
</script>

<div class="table-responsive" data-test="a">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<th>Filter</th>
				<util:remoteSortableColumn  action="customAlert"  property="broadcast.dateCreated" title="Date" update="tabs-1" />
				<util:remoteSortableColumn  action="customAlert" property="broadcast.broadcasttype" title="Type"  update="tabs-1" />
				<util:remoteSortableColumn  action="customAlert"  property="broadcast.unit.part" title="Part"   update="tabs-1"  />
				<util:remoteSortableColumn  action="customAlert"   property="broadcast.unit.manufacturer" title="Mfg"   update="tabs-1" />
				<util:remoteSortableColumn  action="customAlert"   property="broadcast.unit.condition" title="Condition"   update="tabs-1" />
				<util:remoteSortableColumn  action="customAlert"   property="broadcast.unit.price" title="Price"  update="tabs-1"  />
				<util:remoteSortableColumn  action="customAlert"   property="broadcast.unit.qty" title="Qty"  update="tabs-1"  />
				<th>Description</th>
				<util:remoteSortableColumn  action="customAlert"   property="broadcast.company.name" title="Company"  update="tabs-1"  />
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${broadcastList}" status="i" var="broadcast">
			<tr class="${(i % 2) == 0 ? 'first' : 'second'}">
				<td><g:each in="${alertMap.getAt(broadcast)}" var="filter"><a href="javascript:void(0)"  id="respond" onClick="getFilter(${filter.id});" >${filter.name}&nbsp;&nbsp;</a></g:each></td>
				<td style="min-width:85px" >${sdf.format(broadcast.dateCreated)}</td>
				<td>${broadcast.broadcasttype}</td>
				<td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
				<td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
				<td>${fieldValue(bean:broadcast.unit, field:'condition.name')}</td>
				<td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
				<td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
				<td>
					<div class="showmore">
						<div class="moreblock htmldscr">${broadcast.description}</div>
					</div>
				</td>
				<td><a href="../company/view?id=${broadcast?.company?.id}">${broadcast.company.name}</a></td>
				<td>
					<div class="action-control">
						<g:if test="${broadcast.company.id != session.company.id}">
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${broadcast.id}','${broadcast.createdby.id}');" >
							<i class="fa fa-reply fa-lg"></i> Respond
						</a>
						</g:if>
						<g:else>
						<pre></pre>
						</g:else>
						<g:link class="delete" controller="alert" action="delete" id="${broadcast?.id}">
							<i class="fa fa-trash-o fa-lg"></i>Delete
						</g:link>
					</div>
				</td>
			</tr>
			</g:each>
		</tbody>
	</table>
</div>

<div class="pagination">
	<ul>
		<util:remotePaginate action="customAlert" controller="alert" total="${alertTotal}" params="[pagination:true]" update="tab-1" />
	</ul>
</div>

	<script>
		$(document).ready(function(){
			
			$('.htmldscr').each(function(){
			
				var html = $(this).text();
				$(this).html(html);
			});
			
		});
</script>