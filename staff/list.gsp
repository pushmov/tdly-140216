

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Staff List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Staff</g:link></span>
        </div>
        <div class="body">
            <h1>Staff List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="aol" title="Aol" />
                        
                   	        <g:sortableColumn property="businesstype" title="Businesstype" />
                        
                   	        <g:sortableColumn property="cellphone" title="Cellphone" />
                        
                   	        <g:sortableColumn property="email" title="Email" />
                        
                   	        <g:sortableColumn property="emprole" title="Emprole" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${staffList}" status="i" var="staff">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${staff.id}">${fieldValue(bean:staff, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:staff, field:'aol')}</td>
                        
                            <td>${fieldValue(bean:staff, field:'businesstype')}</td>
                        
                            <td>${fieldValue(bean:staff, field:'cellphone')}</td>
                        
                            <td>${fieldValue(bean:staff, field:'email')}</td>
                        
                            <td>${fieldValue(bean:staff, field:'emprole')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${Staff.count()}" />
            </div>
        </div>
    </body>
</html>
