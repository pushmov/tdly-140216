<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>
<%@page import="test2.Cond" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if (session.person?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
		}
			
%>

			<div class="tab-content-right">
				<div class="tabToggle">
					<a href="#" class="arrow-icon"></a>
				</div>
				
				<div class="tab-filter-info">
					<dl class="dl-horizontal clearfix">
						<dt>Broadcast Type Filter :</dt><dd><g:each in="${broadcastType}" >${it},&nbsp;</g:each></dd>
						<dt>Condition Filter :</dt><dd><g:each in="${condition}" >${it},&nbsp;</g:each></dd>
						<dt>Country Filter :</dt><dd><g:each in="${countrySel}" >${it},&nbsp;</g:each></dd>
						<dt>Company Filter :</dt><dd><g:each in="${companySel}" >${it},&nbsp;</g:each></dd>
						<dt>Manufacturer Filter :</dt><dd><g:each in="${manufacturerSel}" >${it},&nbsp;</g:each></dd>
						<dt>Partlist :</dt><dd>${partlistSel?.equals("all")?"":partlistSel}</dd>
					</dl>
				</div>
				
				<div class="tab-utility-button">
					<button type="button" data-toggle="modal" data-target="#modal_single_part_broadcasts" class="btn btn-primary btn-lg">Broadcast</button>
				</div>
				
				<div id="tableRow">
					<g:render template="broadcastRowsSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,filterParams:filterParams, broadcastTotal:broadcastTotal]}" />
				</div>
			</div>
			<!--inner-contrightpan end -->
			
			
			<!-- Popup for single part broadcasts-->
        <div class="modal fade broadcasts-popup" id="modal_single_part_broadcasts" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Create Broadcast</h4>
              </div>
              <!--modal-header-->
							
						<div id="createsingle" class="form-content"></div>	
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
        </div>
      <!-- Popup for single part broadcasts-->
			
			<script>
				$('.tabToggle a').on('click', function(event){
					
					event.preventDefault();
					$(this).toggleClass('is-active');
					var contentBlock = $(this).closest('.tab-content-block').toggleClass('is-open');
				});
				
				$(document).ready(function() {
					$('#fsingle-editor').Editor();
					
					
					$('button[data-target="#modal_single_part_broadcasts"]').click(function(){
						
					$('#createsingle').html('');
	
						$.get('createsingle', function(res){
							$('#createsingle').html(res);
						});
						
					});
				});
			</script>

