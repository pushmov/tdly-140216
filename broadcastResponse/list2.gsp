<%! import groovy.xml.MarkupBuilder %> 
<%
def writer = new StringWriter()
	def xml = new MarkupBuilder(writer)
    	xml.regions(name: "Countries"){
            coun(name:"USA")
    		coun(name:"Canada", id: 1)
    	}
	def countrydata = writer.toString()


	writer = new StringWriter()
	def venxml = new MarkupBuilder(writer)
    	venxml.roottag(name: "Vendors"){
            vendor(name:"Jumpha Computers")
    		vendor(name:"Globalnet Hardware") 
    	}
	def vendordata = writer.toString()

%>
 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<gui:resources components="['dialog','richEditor']"/>
	<script>
	function assignBid(bid) {
		dialog3Form.bid.value = bid
	}
	</script>


    </head>
    <body> 
        <div class="body">
	<div id="bresp"></div>
            <div class="list">
                <table>
			  <tr>
			     <td>
					<table>
						<tr>
							<td>
								<richui:treeView id="country" xml="${countrydata}" />
							</td>
						</tr>
						<tr>
							<td>
								<richui:treeView id="vendor" xml="${vendordata}" />
							</td>
						</tr>
					</table>
			     </td>
			     <td valign="top">
            <div class="list" id="blist">

					<table valign="top">
                        <tr valign="top">
                   	        <g:sortableColumn property="broadcast.created" title="Broadcast Date" />
                   	        <g:sortableColumn property="broadcast.created" title="Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="company" title="Company" />
                   	        <g:sortableColumn property="fromid" title="Contact Name" />
                   	        <g:sortableColumn property="respdate" title="Response Date" />
                   	        <g:sortableColumn property="respdate" title="Country" />
					  <th>Response text</th>                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastResponseList}" status="i" var="broadcastResponse">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'broadcasttype')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcastResponse.company, field:'name')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'fromid')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'country')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'responsetext')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
			<util:remotePaginate controller="broadcastResponse" action="bcastresponse" total="${BroadcastResponse.count()}"
                                   update="blist"/>
            </div>




					</table>
				</td>
			  </tr>	
		    </table>


            </div>
        </div>
    </body>
</html>
