<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.MyVendor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

	<script>

    		var vid;


	function addVendor(id) {
		vid = id;
		Ext.Msg.show({
		   title:'Save Changes?',
		   msg: 'Would you also like to add this company to your vendor list?',
		   buttons: Ext.Msg.YESNO,
		   fn: addToMyList
		  // animEl: 'elId'
		});

	}
	function addToMyList(btn) {
		var addToMyVendor;
		if (btn == 'yes') {
			addToMyVendor = 'true';
		}
		else {
			addToMyVendor = 'false';
		}

		
		var conn = new Ext.data.Connection();
		conn.request({
		    url: '/test2/vendorRequest/acceptVendor?addToMyVendor=' + addToMyVendor + '&id=' + vid,
		    method: 'POST',
		    success: function(responseObject) {
		        //showHistoryDialog(responseObject.responseText);
				Ext.Msg.alert('Status', 'Done.');

		    },
		     failure: function() {
		         Ext.Msg.alert('Status', 'Unable to show history at this time. Please try again later.');
		     }
		});
		
	}
Ext.onReady(function(){
}); 

	</script>



</head>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.company.address.timezone));
	        
		
%>

				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

                   	        <g:sortableColumn property="category" title="Category" />
                   	        <g:sortableColumn property="name" title="Name" />
                   	        <g:sortableColumn property="country" title="Country" />
                   	        <g:sortableColumn property="state" title="State" />
                   	        <g:sortableColumn property="city" title="City" />

									<th><span>Date Created</span></th>
                   	        <g:sortableColumn property="message" title="Message" />
									
									<th>Actions</th>
								</tr>
								
                    <g:each in="${vendorRequestList}" status="i" var="vendorRequest">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
	                            <td>${fieldValue(bean:vendorRequest.vendor, field:'category')}</td>
	                            <td>${fieldValue(bean:vendorRequest.vendor, field:'name')}</td>
	                            <td>${fieldValue(bean:vendorRequest.vendor.address, field:'country')}</td>
	                            <td>${fieldValue(bean:vendorRequest.vendor.address, field:'state')}</td>
	                            <td>${fieldValue(bean:vendorRequest.vendor.address, field:'city')}</td>
						<td>${sdf.format(vendorRequest.dateCreated)}</td>
        	                    <td>${fieldValue(bean:vendorRequest, field:'message')}</td>
				<td>
			<div class="actions_menu">
				<ul>
					<li><g:remoteLink class="delete" controller="vendorRequest" action="delete" id="${vendorRequest?.id}" before="return confirm('Are you sure?');" ><img width=22 height=22 style="float:left" src="../images/delete.png" />&nbspDelete</g:remoteLink></li>
				</ul>
			</div>

				</td>



                        </tr>
                    </g:each>

								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>

												                <g:paginate total="${MyVendor.count()}" />

						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
