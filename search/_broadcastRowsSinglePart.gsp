<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if (session.person?.address?.timezone)
		sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
%>

<div class="table-responsive">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<util:remoteSortableColumn property="part" action="filter" params="${filterParams}" title="Part" update="tableRow" />
				<util:remoteSortableColumn property="mfgname" action="filter" params="${filterParams}" title="Mfg" update="tableRow"  />
				<util:remoteSortableColumn property="condition" action="filter" params="${filterParams}" title="Condition" update="tableRow"  />
				<util:remoteSortableColumn property="price" action="filter" params="${filterParams}" title="Price" update="tableRow"  />
				<util:remoteSortableColumn property="qty" action="filter" params="${filterParams}" title="Qty" update="tableRow"  />
				<util:remoteSortableColumn property="companyName" action="filter" params="${filterParams}" title="Company" update="tableRow"  />
				<util:remoteSortableColumn property="countryName" action="filter" params="${filterParams}" title="Country" update="tableRow"  />
				<util:remoteSortableColumn property="sortableLastUpdated" action="filter" params="${filterParams}" title="Updated" update="tableRow"  />
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<g:each in="${broadcastList}" status="i" var="inventory">
			<tr class="${(i % 2) == 0 ? 'first' : 'second'}">
				<td id="part${i}" >${fieldValue(bean:inventory.item, field:'part')}</td>
				<td id="${fieldValue(bean:inventory.item.manufacturer, field:'id')}" >${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}</td>
				<td id="${i}" >${fieldValue(bean:inventory.item, field:'condition.name')}</td>
				<td id="${i}" >${fieldValue(bean:inventory.item, field:'price')}</td>
				<td id="${i}" >${fieldValue(bean:inventory.item, field:'qty')}</td>
				<td id="${i}" >${inventory.company.name}</td>
				<td id="${i}" >${inventory.company.address.country.name}</td>
				<td>${sdf.format(inventory.lastUpdated)}</td>
				<td>
					<div class="action-control">
						<a href="javascript:void(0)" class="details" id="respond" onClick="javascript:broadcast('${fieldValue(bean:inventory.item, field:'part')}','${fieldValue(bean:inventory.item.manufacturer, field:'mfgname')}','${fieldValue(bean:inventory.item, field:'condition.id')}','${fieldValue(bean:inventory.item, field:'price')}','${inventory.item.qty}','${fieldValue(bean:inventory.item.manufacturer, field:'id')}');" >
							<i class="fa fa-wifi fa-lg"></i>Broadcast
						</a>
						<a href="javascript:void(0)" class="add" id="respond" onClick="javascript:addToWatchlist('${inventory.id}');" >
							<i class="fa fa-video-camera fa-lg"></i>To Watchlist
						</a>
					</div>
				</td>
				
				<input type="hidden" name="id" value="${inventory?.id}" />
				
			</tr>
			</g:each>
		</tbody>
	</table>
</div>

<div class="pagination_n">
	<util:remotePaginate total="${broadcastTotal}" action="filter" params="${filterParams}" update="tableRow"  />
</div>
