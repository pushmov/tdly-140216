<%@page import="test2.Person" %>
<%@page import="com.ucbl.util.PZUtil" %>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="layout" content="pz" />
		<title>Show Company</title>

<r:script>
	$(document).ready(function() {

	    $('#dialog-message').dialog({
        	autoOpen: false,
	        height: 250,
        	width: 350,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });


	  $('#dialog-employee').dialog({
        	autoOpen: false,
	        height: 600,
        	width: 650,
	        modal: true,
        	buttons: {
	            SaveFilter: function() {
	
    			$("#person").submit();
                	$(this).dialog('close');

	            },
	            Cancel: function() {
        	        $(this).dialog('close');
            	    }

	        }        
	   });

  $('#dialog-resetPassword').dialog({
        autoOpen: false,
        height: 400,
        width: 650,
        modal: true,
        buttons: {
            SaveFilter: function() {

    		$("#resetPasswordForm").submit();
                $(this).dialog('close');

            },

            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });


           $('#person').submit( function() {
    
		    $.ajax({
			    type: "POST",
    			    url: "../person/update",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        //alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
					//alert('success');
				    $('#employeeview').html(data);
				   //$('#dialog-responseSave').dialog("open");
    			    }
			    
                    });

       	            return false;

            });


       });


function editPerson(id) {

	$("returnTemplate").val("dialog");

	$.get("../person/showajax",{id: id},

		function(data,textStatus,jqXHR) {
			
			//$("#dialog-employee").replaceWith(data)
		        
			$("#dialog-employee").dialog("open");
		}

        );

}

function openResetPassword() {
		        
	$("#dialog-resetPassword").dialog("open");

}



</r:script>

<r:require module="network" />

  </head>
<g:render template="/dialog-broadcast-response" />
<g:render template="/moreBroadcast" />

<div id="dialog-message" title="Connection Request">

</div>

<!-- BEGIN MAIN SIDEBAR -->
  <button type="button" class="navbar-toggle left-menu" data-toggle="collapse" data-target="#main-menu" aria-expanded="true" aria-controls="main-menu">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <div class="clearfix"></div>
						
	<nav id="sidebar-inner">
    <div id="main-menu" class="navbar-collapse collapse" aria-expanded="true"> 
      <div class="profil-sidebar-element">
          <h4 class="pull-left">BROADCAST</h4>
          <div class="sidebar-number pull-right"><a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${broadcastCount}</a></div>
          <div class="clearfix"></div>
          <h4 class="pull-left">INVENTORY COUNT</h4>
          <div class="sidebar-number pull-right"><a href="${request.contextPath}/inventory/getInventory?pid=${person.id}" >${inventoryCount}</a></div>
          <div class="clearfix"></div>
         </div>
         
         <div class="sidebar-block">
      <div class="sidebar-title">Mutual Connections</div>
      <div class="col-md-12">
      	<ul class="connections-block">
        	<li><img src="/tdly/assets/img/profil_page/mutual-connection-img01.jpg" alt=""></li>
            <li><img src="/tdly/assets/img/profil_page/mutual-connection-img02.jpg" alt=""></li>
            <li><img src="/tdly/assets/img/profil_page/mutual-connection-img03.jpg" alt=""><span class="connections-more"><a href="#">more</a></span></li>
        </ul> 
        <div class="clearfix p-t-10">
            <button class="btn btn-skyblue btn-mini">Add to Connection</button>
         </div>
         
         
      </div>
      <div class="clearfix"></div>
      </div>
      
<!--
      <div class="sidebar-block mutual-connections">
        <div class="sidebar-title">Connections</div>
        
					<g:each in="${connections}" status="i" var="con">
          <div class="mutual-block">
          <div class="col-md-12">
            <ul class="connections-block">
              <li><img src="/tdly/assets/img/profil_page/connection-img01.jpg" alt=""></li>
            </ul>
            <h4>${con?.contact?.firstname} ${con?.contact?.lastname}</h4>
            <p>${con?.contact?.company?.name} <br>
              ${con?.contact?.company?.address?.city} <br>
              ${con?.contact?.company?.address?.state}, <br>
              ${con?.contact?.company?.address?.country?.name}</p>
            
          </div>
          <div class="col-md-12 border-top p-t-20 "></div>
          </div>
					</g:each>
        
        <div class="clearfix"></div>
      </div>
 -->
      
      <div class="sidebar-block">
      <div class="sidebar-title">Personal</div>
      
    	 <div class="mutual-block">
         <div class="col-md-12">
            <h4>${person.firstname} ${person.lastname}</h4>
            <p>${person.title}</p>
          </div>  
            <div class="col-md-12 border-top p-t-10 "></div>
          <div class="col-md-12">
            <p>Office: ${person?.address?.phone}<br>Cell: ${person?.cellphone}</p>
          </div>  
            <div class="col-md-12 border-top p-t-10 "></div>
            <div class="col-md-12">
            <p>${person?.email} <br>
            Yahoo: ${person?.yahoo} <br>
            Skype: ${person?.skype} <br>
            Aol: ${person?.aol} <br>
            gtalk: ${person.gtalk} <br>
            Msn : ${person?.msn} </p>
          </div>
          <div class="col-md-12 border-top p-t-10 "></div>
          <div class="col-md-12">
          	<p>Broadcasts : <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${broadcastCount}</a> <br>
			Inventory: <a href="${request.contextPath}/inventory/getInventory?pid=${person.id}" >${inventoryCount}</a> </p>
            <div class="clearfix"></div>
          </div>
          
          
        </div>
        <div class="clearfix"></div>
      </div>
      
      <div class="sidebar-block">
      <div class="sidebar-title">Company</div>
    	 <div class="mutual-block">
         <div class="col-md-12">
            <h4>${person?.company?.name}</h4>
            <p>${person?.company?.membershiplevel.memlevel}<br>
            ${person?.company?.address?.city} <br>
            ${person?.company?.address?.state}, <br>
            ${person?.company?.address?.country?.name}<br>
            Broadcast: <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${companyBroadcastCount}</a> <br>
            Inventory: <a href="${request.contextPath}/inventory/getInventoryC?cid=${person?.company?.id}" >${companyInventoryCount}</a> </p>
          </div>  
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>
  </nav>
  <!-- END MAIN SIDEBAR --> 
	
	<!-- BEGIN MAIN CONTENT -->
  <div id="main-content-inner" class="page-profil">
    <div class="row">
      <div class="clearfix">
        <div class="col-md-12">
          <div class="col-md-3 profil-sidebar"> <img src="/tdly/assets/img/profil_page/me.jpg" class="img-responsive" alt="profil"> </div>
          <div class="col-md-9 profil-content profile-block">
            <div class="profile-heading">
              <h2>${person.firstname} ${person.lastname}</h2>
              <div class="tag-line">(United States)</div>
            </div>
            <div class="profil-presentation">
              <h3>HI! I'M Nulla Pariatur</h3>
              <p>"Nunc vulputate blandit Integer eget laoreet odio, id aliquet risus. Etiam sed tincidunt ligula, eget sodales urna. Sed scelerisque porttitor condimentum. Mauris lobortis mi lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra."</p>
            </div>
          </div>
        </div>
      </div>
      <div class="clearfix">
        <div class="col-md-12 m-t-20">
          <h4>MY CONNECTIONS</h4>
          <div class="col-md-12 border-top m-t-10"></div>
          <div class="col-md-12 p-t-20"></div>
          <div class="row p-t-20">
					
					
            <div class="col-md-6 my-connection">
              <div class="row">
								
								<g:each in="${connections}" status="i" var="con">
                <div class="col-lg-6">
                  <div class="row" >
                    <div class="col-md-4"> <img class="img-rounded img-responsive" alt="review 1" src="/tdly/assets/img/profil_page/profile-img02.jpg"> </div>
                    <div class="col-md-8 section-box">
                      <h4>${con?.contact?.firstname} ${con?.contact?.lastname}</h4>
                      <p>${con?.contact?.company?.address?.city} ${con?.contact?.company?.address?.state}, ${con?.contact?.company?.address?.country?.name}</p>
                    </div>
                  </div>
                </div>
								
								<g:if test="${(i+1) % 4 == 0}">
									</div></div><div class="col-md-6 my-connection"><div class="row">
								</g:if>
								
								</g:each>
								
								
              </div>
            </div>
						
            <div class="col-md-12 border-bottom p-t-20"></div>
            <div class="col-md-12 p-t-20">
              <div class="col-md-12">
                <div class="pull-right clearfix ">
                  <button class="btn btn-primary btn-sm more-btn" type="button">More <span class="fa arrow"></span> </button>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 border-bottom p-t-20"></div>
            <div class="clearfix col-md-12">
              <div class="dropdown sort-by p-t-20">
                <button class="btn btn-primary dropdown-toggle blockelement" type="button" data-toggle="dropdown">Sort by recent activity <i class="fa fa-angle-down p-r-10"></i></button>
                <ul class="dropdown-menu">
                  <li><a href="#">HTML</a></li>
                  <li><a href="#">CSS</a></li>
                  <li><a href="#">JavaScript</a></li>
                </ul>
              </div>
            </div>
						
						<!-- start feed -->
						
            <div class="clearfix p-b-20">
						<g:each in="${feedList}" status="i" var="feed">
              <div class="col-md-6 right-border">
                <div class="row p-t-20">
                  <div class="col-md-2"><img src="/tdly/assets/img/profil_page/profile-img02.jpg" alt="review 1" class="img-rounded img-responsive"></div>
                  <div class="col-md-10">
										
										<div class="single-title">
										<g:if test="${feed.type == 'B'}" >
                    Single Part Broadcast: ${feed?.broadcast?.broadcasttype}<br>
										</g:if>
										<g:if test="${feed.type == 'M'}" >
										WTB (Multi Part)<br>
										</g:if>
											<span><%=PZUtil.format(feed?.broadcast?.dateCreated)%></span>
										</div>
									</div>
								</div>
                <div class="col-md-12 border-bottom p-t-20"></div>
                <div class="col-md-12 p-t-20">
                  <div class="row">
									<g:if test="${feed.type == 'B'}" >
                    <%=feed.broadcast.description%>
									</g:if>
									<g:if test="${feed.type == 'M'}" >
										<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets </p>
									</g:if>
                    <div class="col-md-12 border-bottom p-t-10"></div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 p-t-20"></div>
                    <ul class="link-list">
                      <li><a href="#">Part : ${feed?.broadcast?.unit?.part}</a></li>
                      <li>|</li>
                      <li><a href="#">Manufacturer : ${feed?.broadcast?.unit?.manufacturer?.mfgname}</a></li>
                      <li>|</li>
                      <li><a href="#">Condition : ${feed?.broadcast?.unit?.condition?.name}</a></li>
                      <li>|</li>
                    </ul>
                    <ul class="link-list">
                      <li><a href="#">Price : $${feed?.broadcast?.unit?.price}</a></li>
                      <li>|</li>
                      <li><a href="#">Quantity : ${feed?.broadcast?.unit?.qty}</a></li>
                    </ul>
                    <div class="clearfix p-t-20 m-t-10">
                      <button type="button" class="btn btn-primary btn-sm more-btn btn-success">Respond <span class="fa arrow"></span> </button>
                      <button type="button" class="btn btn-primary btn-sm more-btn">More <span class="fa arrow"></span> </button>
                    </div>
                    <div class="col-md-12 border-bottom p-t-20"></div>
                  </div>
                </div>
              </div>
							
							<g:if test="${(i+1) % 2 == 0}">
								</div><div class="clearfix p-t-10">
							</g:if>
							
						</g:each>
						
            </div>
						
						
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END MAIN CONTENT --> 
  <div class="clearfix"></div>

<div class="tablewrap" style="display:none">


<br/>
<br/>
<div class="eachtab" id="tab-1">
<div class="box">
		
		
		<!-- Left Menu Start -->
<div class="left_side">
  
	       <div id="basic-accordian" >
  <div id="test-header" class="accordion_headings header_highlight" >Personal</div>
  <div id="test-content">  
    <div class="accordion_child">
    	<div class="accordion_con">${person.firstname} ${person.lastname}<br />
${person.title}<br />
Office: ${person?.address?.phone}<br />
Cell: ${person?.cellphone}<br />
${person?.email} <br />
Yahoo: ${person?.yahoo} <br />
Skype: ${person?.skype} <br />
Aol: ${person?.aol} <br />
gtalk: ${person.gtalk} <br />
Msn : ${person?.msn} <br />
Broadcasts : <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${broadcastCount}</a> <br />
Inventory: <a href="${request.contextPath}/inventory/getInventory?pid=${person.id}" >${inventoryCount}</a> <br />
		</div>
		
    </div>    
  </div>
  <!--End of each accordion item--> 

  <div id="test1-header" class="accordion_headings" >Company</div>  
  <div id="test1-content">  
    <div class="accordion_child">
    	<div class="accordion_con">${person?.company?.name}<br />
${person?.company?.membershiplevel.memlevel}<br />
${person?.company?.address?.city} ${person?.company?.address?.state}, ${person?.company?.address?.country?.name}<br />
Broadcast: <a href="${request.contextPath}/broadcast/search?createdById=${person.id}">${companyBroadcastCount}</a> <br />
Inventory: <a href="${request.contextPath}/inventory/getInventoryC?cid=${person?.company?.id}" >${companyInventoryCount}</a> <br />
		</div>
    </div>
  </div>
  <!--End of each accordion item--> 

  <div id="test2-header" class="accordion_headings" >Connections</div>   
  <div id="test2-content">   	 
    <div class="accordion_child">
<g:each in="${connections}" status="i" var="con">
        <div class="accordion_con">${con?.contact?.firstname} ${con?.contact?.lastname}<br />
                                                                ${con?.contact?.company?.name} <br />
                                                            <span>${con?.contact?.company?.address?.city} ${con?.contact?.company?.address?.state}, ${con?.contact?.company?.address?.country?.name}</span>
		</div>
                                        </g:each>
    </div>
  </div>
 <!--End of each accordion item-->
</div>
	
	 <div class="clear"></div>
	 </div>
<!-- Left Menu End -->
		
		
			
	 
	 
	 <div class="right_side">
	  
	   <div class="list_menu">
      <ul>
        <li><a href="#">All Updates</a>
          <ul>
            <li><a href="#">Dummy Text</a></li>
            <li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
			<li><a href="#">Dummy Text</a></li>
          </ul>
        </li>      
      </ul>  	
	</div>
	    <h1>Sort By Recent Activity</h1>
	<g:each in="${feedList}" status="i" var="feed">	
		<g:if test="${feed.type == 'B'}" >
		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">Single Part Broadcast:  ${feed?.broadcast?.broadcasttype} </div>
			     <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		     <li>Part : ${feed?.broadcast?.unit?.part}</li>
		     <li>Manufacturer : ${feed?.broadcast?.unit?.manufacturer?.mfgname}</li>
		     <li>Condition : ${feed?.broadcast?.unit?.condition?.name}</li>
		     <li>Price : $${feed?.broadcast?.unit?.price}</li>
		     <li>Quantity : ${feed?.broadcast?.unit?.qty}</li>
		  </ul>
			
			<div class="date_p"><%=PZUtil.format(feed?.broadcast?.dateCreated)%></div>
			<div class="clear"></div>
			<div class="profile_rightcon">
				<%=feed.broadcast.description%>
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:if>
		<g:if test="${feed.type == 'M'}" >
		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">WTB (Multi Part)</div>
				 <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		    <li>Part : IBM888</li>
		     <li>Manufacturer : IBM</li>
			 <li>Condition : Refurnished</li>
			 <li>Price : $ 1000</li>
			 <li>Quantity : 200</li>
			
		    </ul>
			
			<div class="date_p">Mar 12</div>
			
			<div class="clear"></div>
			<div class="profile_rightcon">
			     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:if>

		<div class="profile">
		<div class="profile_left">
			 <div class="profile_text"> 
			     <div class="profile_n">WTB (Multi Part)</div>
				 <div class="respond"><a href="#">Respond</a></div>
			 </div>
			 <div class="clear"></div>
		</div>
		<div class="profile_right">
		  <ul>
		    <li>Part : IBM888</li>
		     <li>Manufacturer : IBM</li>
			 <li>Condition : Refurnished</li>
			 <li>Price : $ 1000</li>
			 <li>Quantity : 200</li>
			
		    </ul>
			
			<div class="date_p">Mar 12</div>
			
			<div class="clear"></div>
			<div class="profile_rightcon">
			     Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
			</div>
			<div class="more_p"><a href="#">More</a></div>
			</div>
		<div  class="clear"></div>
		</div>
		</g:each>
		
	
	      <div class="pagination_n">
		       <div class="pagination_pre"><a href="#"></a></div>			    
			     <div class="pagination_num">
                  <ul>
						<li><a href="#" class="active">1 </a> </li>
						<li><a href="#" >2</a></li>						
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>		
						<li><a href="#">5</a></li>
						 
						
		         </ul>             
			     </div>
			   <div class="pagination_next"><a href="#"></a></div>
		  </div>
	 
	
	<div class="clear"></div>
		</div>
  <div  class="clear"></div>
</div>
</div>
</div>
