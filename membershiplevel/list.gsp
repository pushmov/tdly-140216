

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Membershiplevel List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Membershiplevel</g:link></span>
        </div>
        <div class="body">
            <h1>Membershiplevel List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="description" title="Description" />
                        
                   	        <g:sortableColumn property="memlevel" title="Memlevel" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${membershiplevelInstanceList}" status="i" var="membershiplevelInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${membershiplevelInstance.id}">${fieldValue(bean:membershiplevelInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:membershiplevelInstance, field:'description')}</td>
                        
                            <td>${fieldValue(bean:membershiplevelInstance, field:'memlevel')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${membershiplevelInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
