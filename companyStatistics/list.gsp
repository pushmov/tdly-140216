

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>CompanyStatistics List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New CompanyStatistics</g:link></span>
        </div>
        <div class="body">
            <h1>CompanyStatistics List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="rfqbroadcast" title="Rfqbroadcast" />
                        
                   	        <g:sortableColumn property="rfqresponse" title="Rfqresponse" />
                        
                   	        <g:sortableColumn property="wtbbroadcast" title="Wtbbroadcast" />
                        
                   	        <g:sortableColumn property="wtbresponse" title="Wtbresponse" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${companyStatisticsList}" status="i" var="companyStatistics">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${companyStatistics.id}">${fieldValue(bean:companyStatistics, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:companyStatistics, field:'company')}</td>
                        
                            <td>${fieldValue(bean:companyStatistics, field:'rfqbroadcast')}</td>
                        
                            <td>${fieldValue(bean:companyStatistics, field:'rfqresponse')}</td>
                        
                            <td>${fieldValue(bean:companyStatistics, field:'wtbbroadcast')}</td>
                        
                            <td>${fieldValue(bean:companyStatistics, field:'wtbresponse')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${CompanyStatistics.count()}" />
            </div>
        </div>
    </body>
</html>
