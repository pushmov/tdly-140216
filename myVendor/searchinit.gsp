<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta name="layout" content="pz" />

<title>Admin Panel</title>
<link media="screen" rel="stylesheet" type="text/css" href="css/admin.css"  />
<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="css/ie.css" /><![endif]-->

</head>

				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						

            <g:form action="searchcompany" method="post"  class="search_form" >
						
							<!--[if !IE]>start fieldset<![endif]-->
							<fieldset>
								<!--[if !IE]>start forms<![endif]-->
								<div class="forms">
								<h4>Search Company</h4>

								<div class="row">
									<label>Company:</label>
									<div class="inputs">
										<span class="input_wrapper"><input type="text" id="name" name="searchterm" value=""/>
</span>
									</div>
								</div>

								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>

								<!--[if !IE]>end row<![endif]-->
								
								</div>
								<!--[if !IE]>end forms<![endif]-->
									
								
								<!--[if !IE]>start tooltip<![endif]-->
								<br><br><br><br><br>
								<div class="tooltip">
									<div class="tooltip_top">
										<div class="tooltip_bottom">
											<span class="pointer"></span>
											<p class="first">
												Actual search results wouldn't have tabs, but this is a template after all. Enter Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Pellentesque lobortis lacus euismod urna. Praesent sed tortor. Suspendisse in lacus facilisis tellus tempus venenatis. Vivamus dolor arcu, ultrices in, 
											</p>
											<p>
												<strong>*Note :</strong> Clicking on the "More Advanced Geographic Filters"  button will open an ajax frame for further filtering so information is not lost and there are no popups used, this way. 
											</p>
										</div>
									</div>
								</div>
								<!--[if !IE]>end tooltip<![endif]-->
								
								
								
							</fieldset>
							
							
						</g:form>
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
