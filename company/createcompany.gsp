<%@page import="test2.Membershiplevel" %>

<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="layout" content="pz" />
<title> SaaS WP Theme II</title> 
<!--[if IE]>
<script src="../js/html5.js"></script>
<![endif]-->
<meta name='robots' content='noindex,nofollow' />

<script type='text/javascript' src='../js/jquery/jquery.js?ver=1.4.2'></script> 
<script type='text/javascript' src='../js/easySlider1.5.js?ver=3.0'></script>
<script type='text/javascript' src='../js/saas.js?ver=3.0'></script>
<script type='text/javascript' src='../js/jquery.cookie.min.js?ver=3.0'></script>
<script type='text/javascript' src='../js/saas.twitter.js?ver=3.0'></script>

</head>
<body>
            <g:form action="savecompany" method="post" class="signupform" >
            <g:if test="${flash.message}">
            <div class="message">gg ${flash.message}</div>
            </g:if>

            <g:hasErrors bean="${person}">
            <div class="errors">
                <g:renderErrors bean="${person}" as="list" />
            </div>
            </g:hasErrors>
            <g:hasErrors bean="${person.company}">
            <div class="errors">
                <g:renderErrors bean="${person.company}" as="list" />
            </div>
            </g:hasErrors>
									
		<h5>Login <em>(* Indicates required information)</em></h5>
		<ul>

			<li>

                                    <label for="email">Email:</label>
                                    <input type="text" id="email" name="email" value="${person.email}"/>
					<em>*</em>
			</li>
			<li>

                                    <label for="passwd">Password:</label>
                                    <input type="text" id="passwd" name="passwd" value="${person.passwd}"/>
					<em>*</em>
			</li>
			<li>

                                    <label for="passwd">Confirm Password:</label>
                                    <input type="text" id="passwd" name="passwd2" value="${person.passwd2}"/>
					<em>*</em>
			</li>
		</ul>
	
		<h5>Company Info</h5>
		<ul>
			<li>
				<label for="company">Company Name:</label>
				<input class="company" maxlength="100" id="company" name="companyname" value="${person.company.name}" type="text" />
				<em>*</em>
			</li>
			<li>
				<label for="company">Subscription:</label>
                                <g:select id="membershiplevel" optionKey="id" from="${Membershiplevel.list(sort:'memlevel', order:'asc')}" optionValue="memlevel" name="membershiplevel" value="${membershiplevel?.id}"  noSelection="['':'-Please select a subscription-']" ></g:select>
				<em>*</em>
			</li>

			<li>
				<label for="company">Category:</label>
                                <g:select from="${['Broker','Reseller','Distributor']}" name="category" value="${person.company.category}" ></g:select>
				<em>*</em>
			</li>

			<li>
				<label for="firstName">First Name:</label>
				<input maxlength="50" id="firstName" name="firstname" value="${person.firstname}" type="text" />
				<em>*</em>
<!--				
				<label class="mi" for="middleName">MI:</label>
				<input class="mi" maxlength="10" id="middleName" name="middlename" type="text" />
 -->
			</li>
			
			<li>
				<label for="lastName">Last Name:</label>
				<input maxlength="50" id="lastName" name="lastname" value="${person.lastname}" type="text" />
				<em>*</em>
			</li>
			
			

					    <g:render template="/companyaddress" var="address" bean="${person.company.address}" />

			<li class="a-center">
				<button type="submit"><span class="button"><span>Sign Up!</span></span></button>

			</li>



		</ul>
	
		</g:form>

<script type='text/javascript' src='../js/jquery/jquery.form.js?ver=2.02m'></script>
</body>
</html>
