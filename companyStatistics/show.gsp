

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show CompanyStatistics</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">CompanyStatistics List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New CompanyStatistics</g:link></span>
        </div>
        <div class="body">
            <h1>Show CompanyStatistics</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${companyStatistics?.company?.id}">${companyStatistics?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Rfqbroadcast:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'rfqbroadcast')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Rfqresponse:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'rfqresponse')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wtbbroadcast:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'wtbbroadcast')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wtbresponse:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'wtbresponse')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wtsbroadcast:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'wtsbroadcast')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Wtsresponse:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:companyStatistics, field:'wtsresponse')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${companyStatistics?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
