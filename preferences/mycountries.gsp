<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>My Countries</title> 
        <script>

document.observe('dom:loaded', function() {
   $("continents").observe("change", respondToSelect);
   });

   
 function respondToSelect(event)
 {
       new Ajax.Updater("countrySelect",
          "/pz/continent/updateSelect",
          {method:'get', parameters: {selectedValue : $F("continents")} }
         );


 }

function saveCountries() {

Ext.Ajax.request({
	url : '../preferences/saveCountries' , 
	params : { continent : $F("continents"), countries : $F("countries") },
	method: 'POST',
	success: function ( result,request ) { 
		var jsonData = doJSON(result.responseText);
		Ext.MessageBox.alert('Success', jsonData.msg	); 
	},
	failure: function ( result, request) { 
		var jsonData = doJSON(result.responseText);
		Ext.MessageBox.alert('Failed', jsonData.msg); 
	} 
});
}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}


	</script>
    </head>
    <body> 
				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link    url="[action:'wtb']"   ><span><span>WTB</span></span></g:link></li>
								<li ><g:link  url="[action:'wts']"   ><span><span>WTS</span></span></g:link></li>
								<li><g:link id="selected_search_tab"   url="[action:'mycountries']"   ><span><span>My Countries</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						<div id="product_list_menu">
							<!--[if !IE]>start product list tabs<![endif]-->
							<!--[if !IE]>end product list tabs<![endif]-->
							<a href="javascript:void(0)" class="update" onClick="javascript:createBroadcast();" ><span><span><em>Create </em><strong></strong></span></a>
						</div>
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>

<div id="success" >
</div>

<div id="error" >
</div>

<g:formRemote name="countryform" url="[controller:'preferences',action:'saveCountries']" update="[success:'message',failure:'error']">

<div align="center">
<td>
<select multiple name="optone" size="6" id="continents" >
<option value=" " selected="selected"> </option>
<%
	def continents = Continent.list()
	continents.each {
%>
		<option value="${it.id}">${it.name}</option>
<%
	}
%>
</select>
</td>
<td>
<div id="countrySelect">
<select multiple name="opttwo" size="10" id="countries" >
</select>
</div>
</td>
								</tr>
<tr>
<td>
<input type="button" name="go" value="Save" onClick="javascript:saveCountries();" />
</td>
</tr>
</div>
</g:formRemote >
<!-- onchange="setOptions(document.myform.optone.options[document.countryform.optone.selectedIndex].value);" -->


	
								

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
        </div>
    </body>
</html>
