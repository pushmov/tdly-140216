
<%@ page import="test2.UploadStatistics" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'uploadStatistics.label', default: 'UploadStatistics')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.id.label" default="Id" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "id")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.user.label" default="User" /></td>
                            
                            <td valign="top" class="value"><g:link controller="person" action="show" id="${uploadStatisticsInstance?.user?.id}">${uploadStatisticsInstance?.user?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.comments.label" default="Comments" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "comments")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.company.label" default="Company" /></td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${uploadStatisticsInstance?.company?.id}">${uploadStatisticsInstance?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.dateCreated.label" default="Date Created" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${uploadStatisticsInstance?.dateCreated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.invErrors.label" default="Inv Errors" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "invErrors")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.lastUpdated.label" default="Last Updated" /></td>
                            
                            <td valign="top" class="value"><g:formatDate date="${uploadStatisticsInstance?.lastUpdated}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.overwriteInventory.label" default="Overwrite Inventory" /></td>
                            
                            <td valign="top" class="value"><g:formatBoolean boolean="${uploadStatisticsInstance?.overwriteInventory}" /></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.partsAdded.label" default="Parts Added" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "partsAdded")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.partsUpdated.label" default="Parts Updated" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "partsUpdated")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.result.label" default="Result" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "result")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.type.label" default="Type" /></td>
                            
                            <td valign="top" class="value">${fieldValue(bean: uploadStatisticsInstance, field: "type")}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name"><g:message code="uploadStatistics.xlsFile.label" default="Xls File" /></td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <g:hiddenField name="id" value="${uploadStatisticsInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" action="edit" value="${message(code: 'default.button.edit.label', default: 'Edit')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
