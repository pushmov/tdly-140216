<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>
<%@page import="test2.Company" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if (session.person?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
		}		
%>

<script>
	$('#signal_parts h3').click(function(){
		console.log($(this).next().is(':visible'));
		if($(this).next().is(':visible')){
			$(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
		} else {
			$(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
		}
	});
	
</script>

								<!-- inner-contleftpan start -->
								<div class="tab-content-left">
								<form id="filterForm" name="signUpForm" action="../broadcast/filter" method="post">
									
									<input type="hidden" id="filterName" name="name" value="" />
									<input type="hidden" id="parttype" name="parttype" value="Single" />
									<input type="hidden" id="createdById" name="createdById" value="${filterParams?.createdById}" />
									<input type="hidden" id="myconnections" name="myconnections" value="<%=filterParams?.myconnections%>" />

                  <div class="btn-group-sidebar">
                    <div class="btn-group btn-group-sm" role="group" aria-label="sidebar button group">
                    <button type="button" id="filterResults" class="btn btn-primary">
                      <i class="fa fa-filter" aria-hidden="true"></i> Filter
                    </button>
                    <button type="button" data-toggle="modal" data-target="#modal_single_part_filter" class="btn btn-primary">
                      <i class="fa fa-floppy-o" aria-hidden="true"></i> Save Filter
                    </button>
                    </div>
                  </div>
                  <!--side-bar-btn-group-->

                  <div class="panel-group">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_01">
                          Filter Name <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc_collapse_01" class="panel-collapse collapse in">
                        <div class="panel-body">
													<g:select class="form-control" optionKey="id" from="${BroadcastFilter.findAllByPersonAndFilterTypeInList(session.person,['S','M'])}" optionValue="name" name="filter" onChange="getFilter(this.value)" value="${filterid}" noSelection="[null:'Select Filter']"></g:select>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_02" class="collapsed">
                            Type <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc_collapse_02" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="checkbox margin-top-0">
                            <label>
                              <input type="checkbox" id=wtb name="wtb" value="WTB" ${(broadcastType?.contains("WTB"))?"checked":""}  > WTB
                            </label>
                          </div>
                          <div class="checkbox">
                            <label>
                              <input type="checkbox" id=wts name="wts" value="WTS" ${(broadcastType?.contains("WTS"))?"checked":""} > WTS
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_03" class="collapsed">KEYWORD <span class="icon" aria-hidden="true"></span></a>
                      </div>
                      <div id="acc_collapse_03" class="panel-collapse collapse">
                        <div class="panel-body">
                          <input type="text" name="keyword"  id="keyword"   value="${filterParams?.keyword}" class="form-control" placeholder="search">
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_04" class="collapsed">
                            Condition <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc_collapse_04" class="panel-collapse collapse">
                        <div class="panel-body">
													<input type="text" class="form-control filterlist" id="conditionS-filterinput" placeholder="Search by condition">
                          <ul class="checkbox-list  checkbox-condition">
														<g:each in="${conditionMap}">
															<li>
																<label for="conditionS-${it.value.id}"><input type="checkbox" id="conditionS-${it.value.id}"  name="conditionS" value="${it.value.id}"    />${it.value.name} (${it.value.hit})</label>
															</li>
														</g:each>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_05" class="collapsed">PartList <span class="icon" aria-hidden="true"></span></a>
                      </div>
                      <div id="acc_collapse_05" class="panel-collapse collapse">
                        <div class="panel-body">
													<g:select   id="partlist"   class="form-control" from="${['MyInventory','MyWatchlist']}" noSelection="${['All':'All']}" name="partlist" value="${partlist}"></g:select>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_06" class="collapsed">
                            Manufacturer <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc_collapse_06" class="panel-collapse collapse">
                        <div class="panel-body" style="overflow: auto; height: 180px;">
													<input type="text" id="manufacturer-filterinput" class="form-control" placeholder="Search by Manufacturer">
                          <ul class="checkbox-list  checkbox-manufacturer">
														<g:each in="${manufacturerMap}">
															<li><label for="manufacturer-${it.value.id}"><input type="checkbox" id="manufacturer-${it.value.id}"  name="manufacturer" value="${it.value.id}"    />${it.value.name} (${it.value.hit})</label></li>
														</g:each>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
										
<%
boolean allcomp = ((companyType?.equals("all")) || (companyType == null))
boolean myvendors = (companyType?.equals("myVendors"))
boolean myconnections = (companyType?.equals("myConnections"))
%>

										<g:if test="${filterParams?.myconnections == false}" >
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_07" class="collapsed">
                            Companies <span class="icon" aria-hidden="true"></span>
                        </a>
                      </div>
                      <div id="acc_collapse_07" class="panel-collapse collapse">
                        <div class="panel-body">
                          <div class="radio">
                            <label>
															<g:radio name="companyType" value="myVendors" checked="${myvendors}" /> My Vendors
                            </label>
                          </div>
                          <div class="radio">
                            <label>
															<g:radio name="companyType" value="all" checked="${allcomp}" /> All/ Selected Companies
                            </label>
                          </div>
													<input type="text" class="form-control" id="company-filterinput" placeholder="Search by Company">
                          <ul class="checkbox-list checkbox-company">
														<g:each in="${companyMap}">
                            <li>
                              <label for="company-${it.value.id}"><input type="checkbox" id="company-${it.value.id}"  name="company" value="${it.value.id}"    >${it.value.name} (${it.value.hit})</label>
                            </li>
														</g:each>
                          </ul>
                        </div>
                      </div>
                    </div>
										</g:if>
										
<%
	boolean allcntry = ((countryType?.equals("all")) || (countryType == null))
	boolean mycntry = (countryType?.equals("myCountries"))
%>
										<g:if test="${filterParams?.myconnections == false}" >
                    <!--panel-->
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <a data-toggle="collapse" href="#acc_collapse_08" class="collapsed">Countries <span class="icon" aria-hidden="true"></span></a>
                      </div>
                      <div id="acc_collapse_08" class="panel-collapse collapse">
                        <div class="panel-body" style="overflow: auto; height: 180px;">
                          <input type="text" class="form-control" id="country-filterinput" placeholder="Search countries">
                          <ul class="checkbox-list checkbox-country">
														<g:each in="${countryMap}">
                            <li>
                              <label for="country-${it.value.id}"><input type="checkbox" id="country-${it.value.id}"  name="country" value="${it.value.id}"     >${it.value.name} (${it.value.hit})</label>
                            </li>
														</g:each>
                            
                          </ul>
                        </div>
                      </div>
                    </div>
                    <!--panel-->
										</g:if>
										
                  </div>
                  <!--panel-group-->
									
								</form>
                </div>
								
								<div class="modal fade broadcasts-popup" id="modal_single_part_filter" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Create New Reminder</h4>
											</div>
											<!--modal-header-->
											
											<div class="modal-body">
												<div class="form-group">
													<label for="part" class="col-sm-2 control-label">Filter Name:</label>
													<div class="col-sm-8">
														<input type="text" class="form-control" name="name" id="filter_fname" >
													</div>
												</div>
											</div>
											
											<div class="modal-footer">
												<p class="ajax-resp" align="left"></p>
												<div class="form-group">
													<div class="col-sm-2">
														<div class="loader hide">
															<div class="text-left"><img src="/tdly/images/ajax-loader.gif" >Loading...</div>
														</div>
													</div>
													<div class="col-sm-8">
														 <button type="button" id="save-single-filter" class="btn btn-primary">Save Filter</button>
														 <button type="button" class="btn btn-primary modal-dismiss" data-dismiss="modal">Cancel</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<script>
									
									
									$('#save-single-filter').click(function(){
										$('.loader').removeClass('hide');
										var form = $('#filterForm');
										$.ajax({
											type: 'POST',
											url: '../broadcastFltr/save',
											data: form.serialize() + "&name=" + $('#filter_fname').val(),
											success: function(response){
												$('.loader').addClass('hide');
												alert(response.msg);
												$('.modal-dismiss').click();
											}
										})
									});
								</script>

<form style="display:none" id="filterFormold" name="signUpForm" action="../broadcast/filter"
              method="post"  >
                <input type="hidden" id="filterName" name="name" value="" />
                <input type="hidden" id="parttype" name="parttype" value="Single" />
                <input type="hidden" id="createdById" name="createdById" value="${filterParams?.createdById}" />
        <input type="hidden" id="myconnections" name="myconnections" value="<%=filterParams?.myconnections%>" />

			<div id="inner_for_single" class="inner-contleftpan">
				<!--inner-contleftToppanel start -->
                
				<div class="inner-contleftToppanel">
				
					<ul>
					<li>
						<div class="button">
							<!--<a href="#" id="filterResults"><span>Filter</span></a>-->
							<button type="button" id="filterResults" class="btn btn-broadcast btn-broadcast-filter">Filter</button>
						</div>
					</li>
					<li>
						<div class="button">
							<!--<a href="#"  id="saveFilterButton1"   ><span>Save Filter</span></a>-->
							<button type="button" id="saveFilterButton1" class="btn btn-broadcast btn-broadcast-filter">Save Filter</button>
						</div>
					</li>
					</ul>
					
					<div class="spacer"></div>
					
				<!-- greenarea starts-->
                <div id="signal_parts" class="filter-penal">
									<div class="greenarea">
										<h3 class="page_collapsible collapse-open" id="body-section1">Filter Name:<span><i class="fa fa-minus fa-lg"></i></span></h3>
										<div class="container" style="display: none;">
	                       <div class="content">
						     <div class="greenbox">
								 <table width="100%" border="0" cellspacing="0" cellpadding="0">
									  <tbody><tr>
										<td colspan="2" align="left" valign="top">
										<div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="filterdd_msddHolder">
<div id="filterselect">
	<g:select id="filterdd" style="width:96%;"    optionKey="id" from="${BroadcastFilter.findAllByPersonAndFilterTypeInList(session.person,['S','M'])}" optionValue="name" name="filterdd" onChange="getFilter(this.value)"  value="${filterid}" noSelection="[null:'Select Filter']"  ></g:select>
	
	</div>
										</div>
<div id="filterdd_msdd" class="dd2" style="width: 193px;"><div id="filterdd_title" class="ddTitle"><span id="filterdd_arrow" class="arrow" style="background-position: 0px 0px;"></span><span class="ddTitleText" id="filterdd_titletext"><span class="ddTitleText">WTB_My Inventory</span></span></div><div id="filterdd_child" class="ddChild" style="width: 191px;"><a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="filterdd_msa_0"><span class="ddTitleText">WTB_My Inventory</span></a><a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="filterdd_msa_1"><span class="ddTitleText">Demo 1</span></a></div></div>
 
									</td>
									  </tr>
									  <tr>
										<td colspan="2" align="left" valign="top" height="10"></td>
									  </tr>
									  <tr>
										<td align="left" valign="top" width="30%">

									      <input type="hidden" name="_wtb" /><p> 
                                          <input type="checkbox" safari="1"  name="wtb" value="WTB" ${(broadcastType?.contains("WTB"))?"checked":""} id="wtb"   /><label for="wtb">WTB</label></p>
										
										</td>
										<td align="left" valign="top" width="50%">
									      <input type="hidden" name="_wts" /> <p><input type="checkbox" safari="1"  name="wts" value="WTS" ${(broadcastType?.contains("WTS"))?"checked":""} id="wts"   /><label for="wtb">WTS</label></p>
										</td>
									  </tr>
									</tbody></table>
							  </div>
							</div>
						</div>
						</div>
                      
				    <!-- greenarea ends-->
				<!-- greenarea starts-->
				        <div class="greenarea">
						  <h3 class="page_collapsible collapse-open" id="body-section2">Keyword:<span><i class="fa fa-minus fa-lg"></i></span></h3>
						  <div class="container" style="display: none;">
	                       <div class="content">
						    <div class="greenbox">
							 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tbody><tr>
									<td colspan="2" align="left" valign="top">
									<g:textArea class="inptbox"  name="keyword" value="${filterParams?.keyword}" />
								    </td>
								  </tr>
								</tbody></table>
						  </div>
						   </div>
						  </div>
						</div>
				    <!-- greenarea ends-->

	<!-- greenarea starts-->
				        <div class="greenarea">
						  <h3 class="page_collapsible collapse-open" id="body-section3">Condition<span><i class="fa fa-minus fa-lg"></i></span></h3>
						   <div class="container" style="display: none;">
	                        <div class="content">
						     <div class="greenbox">
							 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tbody><tr>
									<td colspan="2" align="left" valign="top">
									<div class="filterinput-wrap">
										<i class="fa fa-search fa-lg"></i>
										<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
										<input type="text" class="filterlist" placeholder="Search By Condition">
										<ul class="unstyled bcast-filter conditionS">
											<g:each in="${conditionMap}">
											<li><input type="checkbox" id="conditionS-${it.value.id}" /><label for="conditionS-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
											</g:each>
										</ul>
									</div>
									<!--
<select id="condition" name="conditionS" multiple="multiple"  style="width:96%;"  >^M
<g:each in="${conditionMap}">^M
<option ${conditionSel?.contains(it.value.name)?"selected":""}  value="${it.value.id}">${it.value.name} (${it.value.hit})</option>
</g:each>^M
</select>^M
-->
								    </td>
								  </tr>
								</tbody></table>
						  </div>
						    </div>
						   </div>
						</div>
				    <!-- greenarea ends-->

       <!-- greenarea starts-->
                                        <div class="greenarea">
                                                  <h3 class="collapsible collapse-open" id="body-section4">Partlist<span><i class="fa fa-minus fa-lg"></i></span></h3>
                                                   <div class="container" style="display: none;">
                                <div class="content">
                                                     <div class="greenbox">
                                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                  <tbody><tr>
                                                                        <td colspan="2" align="left" valign="top">
                                                                         <div class="ddOutOfVision" style="height:0px;overflow:hidden;position:absolute;" id="partlist_msddHolder">
<g:select from="${['MyInventory','MyWatchlist']}" noSelection="${['All':'All']}" name="partlist" value="${partlist}"  style="width:96%;"   ></g:select>

                                                                        </div>
 <div id="partlist_msdd" class="dd2" style="width: 193px;"><div id="partlist_title" class="ddTitle"><span id="partlist_arrow" class="arrow" style="background-position: 0px 0px;"></span><span class="ddTitleText" id="partlist_titletext"><span class="ddTitleText">New</span></span></div><div id="partlist_child" class="ddChild" style="width: 191px;"><a href="javascript:void(0);" class="selected enabled" style="[object CSSStyleDeclaration]" id="partlist_msa_0"><span class="ddTitleText">New</span></a><a href="javascript:void(0);" class="enabled" style="[object CSSStyleDeclaration]" id="partlist_msa_1"><span class="ddTitleText">Demo 1</span></a></div></div>
  
                                                                    </td>
                                                                  </tr>
                                                                </tbody></table>
                                                  </div>
                                                    </div>
                                                   </div>
                                                </div>
                                    <!-- greenarea ends-->

       <!-- greenarea starts-->
                <div class="greenarea">
                <h3 class="page_collapsible collapse-open" id="body-section5">Manufacturer<span><i class="fa fa-minus fa-lg"></i></span></h3>
                <div class="container" style="display: none;">
                <div class="content">
                <div class="greenbox">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tbody><tr>
                <td colspan="2" align="left" valign="top">
									<div class="filterinput-wrap">
										<i class="fa fa-search fa-lg"></i>
										<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
										<input type="text" class="filterlist" id="manufacturer-filterinput" placeholder="Search By Manufacturer">
										<ul class="unstyled bcast-filter manufacturer">
											<g:each in="${manufacturerMap}">
											<li><input type="checkbox" id="manufacturer-${it.value.id}" /><label for="manufacturer-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
											</g:each>
										</ul>
									</div>
								<!--
                <select id="manufacturer" multiple="multiple"  name="manufacturer"  style="width:96%;"  >
                <g:each in="${manufacturerMap}">
                <option ${manufacturerSel?.contains(it.value.name)?"selected":""}  value="${it.value.id}">${it.value.name} (${it.value.hit})</option>
                </g:each>
                </select>
								-->
                
                
                </td>
                </tr>
                </tbody></table>
                </div>
                </div>
                </div>
                </div>

<g:if test="${filterParams?.myconnections == false}" >
					

				<!-- greenarea starts-->
				        <div class="greenarea">
						  <h3 class="page_collapsible collapse-open" id="body-section6">Companies<span><i class="fa fa-minus fa-lg"></i></span></h3>
						  <div class="container" style="display: none;">
	                        <div class="content">
						    <div class="greenbox">
							 <table width="100%" border="0" cellspacing="0" cellpadding="0">
							     <tbody><tr>
									<td colspan="2" align="left" valign="top">
									<div class="defaultP">
										<div class="ez-radio">
										<g:radio name="companyType" value="myVendors"  checked="${myvendors}" class="ez-hide"  /></div><label for="vendors">My Vendors</label>	
									</div>
									</td>
									
								  </tr>
					<!--			  
								  <tr>
									<td colspan="2" align="left" valign="top">
									<div class="defaultP">
										<div class="ez-radio">

										</div><label for="connections">My Connections</label>	
									</div>
									</td>
									
								  </tr>
					 -->			  
								  <tr>
									<td colspan="2" align="left" valign="top">
									<div class="defaultP">
										<div class="ez-radio">
										<g:radio name="companyType" value="all" checked="${allcomp}" class="ez-hide" /></div><label for="scompanies">All/ Selected Companies</label>	
									</div>
									</td>
									
								  </tr>
								  
							      <tr>
									<td colspan="2" align="left" valign="top" height="10"></td>
								  </tr>
								  <tr>
									<td colspan="2" align="left" valign="top">
										<div class="filterinput-wrap">
											<i class="fa fa-search fa-lg"></i>
											<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
											<input type="text" class="filterlist" placeholder="Search By Company">
											<ul class="unstyled bcast-filter company">
												<g:each in="${companyMap}">
												<li><input type="checkbox" id="company-${it.value.id}" /><label for="company-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
												</g:each>
											</ul>
										</div>
				
									<!--
									<select name="company" id="company" style="width:96%;"  multiple="multiple"   tabindex="1">
<g:each in="${companyMap}">
									  <option name="one" value="${it.value.id}" ${companySel?.contains(it.value.name)?"selected='selected'":""} >${it.value.name} (${it.value.hit})</option>
</g:each>
									</select>
									-->

 
								</td>
								  </tr>
								</tbody></table>
						  </div>
						    </div>
						  </div>
						</div>
				    <!-- greenarea ends-->

</g:if>


<g:if test="${filterParams?.myconnections == false}" >
				<!-- greenarea starts-->
				        <div class="greenarea">
						  <h3 class="page_collapsible collapse-open" id="body-section7">Countries<span><i class="fa fa-minus fa-lg"></i></span></h3>
						   <div class="container" style="display: none;">
	                        <div class="content">
						     <div class="greenbox">
							 <table width="100%" border="0" cellspacing="0" cellpadding="0">
								  <tbody>
						<!-- 
							<tr>
									<td colspan="2" align="left" valign="top">
									<div class="defaultP">
										<div class="ez-radio">
										<g:radio name="countryType" value="myCountries" checked="${mycntry}" id="mcountries" class="ez-hide"  />
										</div>
										<label for="mcountries">My Countries</label>	
									</div>
									</td>
									
								  </tr>
								  <tr>
									<td colspan="2" align="left" valign="top">
									<div class="defaultP">
										<div class="ez-radio">
										<g:radio name="countryType" value="all" checked="${allcntry}" id="ascompanies" class="ez-hide" /> 
										</div>
										<label for="ascompanies">All/ Selected Countries</label>	
									</div>
									</td>
									
								  </tr>
						-->
							      <tr>
									<td colspan="2" align="left" valign="top" height="10"></td>
								  </tr>
								  <tr>
									<td colspan="2" align="left" valign="top">
										<div class="filterinput-wrap">
											<i class="fa fa-search fa-lg"></i>
											<span style="display:none"><i class="fa fa-times-circle fa-lg"></i></span>
											<input type="text" class="filterlist" placeholder="Search By Country">
											<ul class="unstyled bcast-filter country">
												<g:each in="${countryMap}">
												<li><input type="checkbox" id="country-${it.value.id}" /><label for="country-${it.value.id}">${it.value.name} (${it.value.hit})</label></li>
												</g:each>
											</ul>
										</div>

									<!--
									<select name="country" id="country" multiple="multiple"  style="width:96%;" tabindex="1">
<g:each in="${countryMap}">
	<option  value="${it.value.id}" ${countrySel?.contains(it.value.name)?"selected='selected'":""} >${it.value.name} (${it.value.hit})</option>
</g:each>
									</select>
									-->
  
								   </td>
								  </tr>
								</tbody></table>
						  </div>
						    </div>
						  </div>
						</div>
				    <!-- greenarea ends-->


</g:if>
                
                
                </div>
            

					

						</form>							

				</div>
				<!--inner-contleftToppanel end -->
				
			</div>
			<!--inner-contleftpan end -->

						
<g:render template="broadcastTableSinglePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,manufacturerMap:manufacturerMap,filterParams:filterParams, broadcastTotal:broadcastTotal]}" />

<script>

	function hide_and_show()
	{
		 
		if($('#hide_and_show_filter_active_single').hasClass('hide_and_show_filter_active'))
		{
			  console.log('a');
			   $('#inner_for_single').animate({width:"-=17%"},1000);
			   $('#tableGridS div.inner-contrightpan').animate({width:"+=17%"},1000);
		       $('#hide_and_show_filter_active_single').removeClass('hide_and_show_filter_active');
				$('#hide_and_show_filter_active_single').addClass('hide_and_show_filter_inactive');
			   $('#signal_parts').css('display','none');
		}
		else
		{
				console.log('b');
				$('#inner_for_single').animate({width:"+=17%"},1000);
				$('#tableGridS div.inner-contrightpan').animate({width:"-=17%"},1000);
				$('#hide_and_show_filter_active_single').removeClass('hide_and_show_filter_inactive');
				$('#hide_and_show_filter_active_single').addClass('hide_and_show_filter_active');
				$('#signal_parts').css('display','block');
		 
		}
		  
	}

</script>


<script>
	$(document).ready(function(){
		setTimeout(function(){
			$('#signal_parts h3').each(function(){
				
				if($(this).next().is(':visible')){
					$(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
				} else {
					$(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
				}
				// $(this).find('span').html('<i class="fa fa-plus fa-lg"></i>');
			});
		}, 500);
		
		$('ul.bcast-filter').each(function(){
			if($(this).find('li').length > 5) {
				$(this).css({
					'height' : '130px',
					'overflow-y' : 'scroll'
				});
			}
		});
		
		
		// $('#signal_parts h3.collapse-open').each(function(){
			// $(this).find('span').html('<i class="fa fa-minus fa-lg"></i>');
		// });
	});
	
	$('#conditionS-filterinput').filterList();
	$('#manufacturer-filterinput').filterList();
	$('#company-filterinput').filterList();
	$('#country-filterinput').filterList();
	
	
	$('.filterinput-wrap input').bind('keyup', function(){
		
		if($(this).val() == ''){
			
			$(this).parent().find('i.fa-search').show();
			$(this).parent().find('span').hide();
			
		} else {
			
			$(this).parent().find('i.fa-search').hide();
			$(this).parent().find('span').show();
			
		}
		
	});
	
	$('.filterinput-wrap .fa-times-circle').click(function(){
		
		$(this).parent().parent().find('i.fa-search').show();
		$(this).parent().parent().find('input').val('');
		$(this).parent().parent().find('ul.bcast-filter li').show();
		$(this).parent().hide();
		
	});
</script>
