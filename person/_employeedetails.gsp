<%@page import="test2.Role" %>					
<%@page import="test2.Country" %>

<div class="row">
	<div class="col-sm-12 col-md-6 col-lg-6">
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">First Name</label>
			<div class="col-sm-8"><input type="text" name="firstname" id="firstname" class="form-control" value="${fieldValue(bean:person,field:'firstname')}" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Last Name</label>
			<div class="col-sm-8"><input type="text" name="lastname" id="category" class="form-control" value="${fieldValue(bean:person,field:'lastname')}" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Title</label>
			<div class="col-sm-8"><input type="text" name="title" id="category" class="form-control" value="${fieldValue(bean:person,field:'title')}" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Email</label>
			<div class="col-sm-8"><input type="text" name="email"  value="${person?.email?.encodeAsHTML()}" id="phone" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Cell Phone</label>
			<div class="col-sm-8"><input type="text" name="cellphone"  value="${person?.cellphone}" id="toll" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Skype</label>
			<div class="col-sm-8"><input type="text" name="skype"  value="${person?.skype?.encodeAsHTML()}" id="fax" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Yahoo</label>
			<div class="col-sm-8"><input type="text" name="yahoo"  value="${person?.yahoo?.encodeAsHTML()}" id="yearf" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Msn</label>
			<div class="col-sm-8"><input type="text" name="msn"  value="${person?.msn?.encodeAsHTML()}" id="employee" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Gtalk</label>
			<div class="col-sm-8"><input type="text" name="gtalk" value="${person?.gtalk?.encodeAsHTML()}" id="employee" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">AOL</label>
			<div class="col-sm-8"><input type="text" name="aol" value="${person?.aol?.encodeAsHTML()}" id="employee" class="form-control" /></div>
		</div>
	</div>
	<div class="col-sm-12 col-md-6 col-lg-6">
		<g:ifNotSelf person="${person}" >
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Role</label>
			<div class="col-sm-8"><g:select from="${roles}" optionKey="id" optionValue="description" id="websites2" name="role.id" value="${person?.role?.id}" ></g:select></div>
		</div>
		</g:ifNotSelf>
		
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Address</label>
			<div class="col-sm-8"><input type="text" name="address.street1" value="${person?.address?.street1?.encodeAsHTML()}" id="address" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Country</label>
			<div class="col-sm-8"><g:select id="countrySelect" optionKey="id" from="${Country.list(sort:'name', order:'asc')}" optionValue="name" name="address.country.id" value="${person?.address?.country?.id}" noSelection="['':'-Please select your country-']" ></g:select></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">State</label>
			<div class="col-sm-8"><g:select id="selectstate" optionKey="name" from="${flash.stateSelected}" optionValue="name" name="address.state" value="${person?.address?.state}" noSelection="['':'-Please select your State/Region-']" ></g:select></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">City</label>
			<div class="col-sm-8"><input type="text" name="address.city" value="${person?.address?.city?.encodeAsHTML()}" id="city" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Zip</label>
			<div class="col-sm-8"><input type="text" name="address.postalcode" value="${person?.address?.postalcode}" id="zip" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Phone</label>
			<div class="col-sm-8"><input type="text" name="address.phone" value="${person?.address?.phone}" id="zip" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Toll Free Phone</label>
			<div class="col-sm-8"><input type="text" name="address.tollfreephone" value="${person?.address?.tollfreephone}" id="toll" class="form-control" /></div>
		</div>
		<div class="form-group clearfix">
			<label for="m_p_type" class="col-sm-2 control-label gtext">Fax</label>
			<div class="col-sm-8"><input type="text" name="address.fax" value="${person?.address?.fax?.encodeAsHTML()}" id="fax" class="form-control" /></div>
		</div>
		
	</div>
	<div class="col-sm-12 col-md-12 col-lg-12 text-left clearfix">
		<button class="btn btn-broadcast" id="saveemployee" type="button">Save</button>
	</div>
</div>
<script>
$(document).ready(function() {});
function setupFormValidation() {

$(function() {
 person = $('#person');
person = person.validate({
onkeyup: false,
errorClass: 'invalid',
errorElement: 'label',
validClass: 'success',
onsubmit: true,
submitHandler: submitPerson,


errorContainer: '#personErr',
errorLabelContainer: 'div.errors ul',
wrapper: 'li',

rules: {
firstname: { required: true },
lastname: { required: true },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: true },
accountExpired: { required: true },
accountLocked: { required: true },
auth: { required: true },
authorities: { },
company: { required: true },
dateCreated: { date: true },
description: { required: true },
email: { required: true },
"address.city" : { required: true },
"address.postalcode" : { required: true },
"address.state" :  { required: true },
"address.country.id" :  { required: true },
"address.street1" :  { required: true },
emailShow: { required: true },
enabled: { required: true },
lastUpdated: { date: true },
passwd: { required: true },
passwordExpired: { required: true },
role: { required: true }
},
messages: {
firstname: { required: 'First Name cannot be blank' },
lastname: { required: 'Last Name cannot be blank' },
aol: { },
yahoo: { },
msn: { },
gtalk: { },
skype: { },
officephone: { },
cellphone: { },
tollfreephone: { },
title: { },
address: { required: 'Property [address] of class [class test2.Person] cannot be null' },
accountExpired: { required: 'Property [accountExpired] of class [class test2.Person] cannot be null' },
accountLocked: { required: 'Property [accountLocked] of class [class test2.Person] cannot be null' },
auth: { required: 'Property [auth] of class [class test2.Person] cannot be null' },
authorities: { },
company: { required: 'Property [company] of class [class test2.Person] cannot be null' },
dateCreated: { date: 'Property dateCreated must be a valid Date' },
description: { required: 'Property [description] of class [class test2.Person] cannot be null' },
email: { required: 'Email cannot be blank' },
"address.city" : { required: 'City cannot be blank'},
"address.postalcode" : { required: 'Postalcode cannot be blank'},
"address.state" : { required: 'State cannot be blank'},
"address.country.id" : { required: 'Country cannot be blank'},
"address.street1" : { required: 'Street1 cannot be blank'},
emailShow: { required: 'Property [emailShow] of class [class test2.Person] cannot be null' },
enabled: { required: 'Property [enabled] of class [class test2.Person] cannot be null' },
lastUpdated: { date: 'Property lastUpdated must be a valid Date' },
passwd: { required: 'Property [passwd] of class [class test2.Person] cannot be null' },
passwordExpired: { required: 'Property [passwordExpired] of class [class test2.Person] cannot be null' },
role: { required: 'Property [role] of class [class test2.Person] cannot be null' }
}
});
});
}
</script>