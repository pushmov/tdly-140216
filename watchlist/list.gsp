

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Watchlist List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Watchlist</g:link></span>
        </div> 
        <div class="body">
            <h1>Watchlist List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Part</th>
                   	    
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="dateCreated" title="Date Created" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${watchlistInstanceList}" status="i" var="watchlistInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${watchlistInstance.id}">${fieldValue(bean:watchlistInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:watchlistInstance, field:'part')}</td>
                        
                            <td>${fieldValue(bean:watchlistInstance, field:'dateCreated')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${watchlistInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
