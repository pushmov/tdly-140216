
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
	<resource:tabView />
<script>


function processTab(obj) {


tabs = obj.get('tabs');
idx = obj.get('activeIndex');

// add the current tab to the session
new Ajax.Request('/test2/broadcast/hello', { parameters: {currentTab: idx}})

// if the current tab is the tab I want - then lets kick off
// the ajax call


}

    </head>
    <body>
        <div class="body">
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>

<richui:tabView id="tabView"  curTab="${curTab}" event="processTab(this)">  
	<richui:tabLabels>  
		<richui:tabLabel selected="true" title="Broadcast Responses" />  
		<richui:tabLabel title="RFQ Responses" />  
		<richui:tabLabel title="Vendor Requests" />  
		<richui:tabLabel title="Consumer Enquiry" />  
		<richui:tabLabel title="Private Broadcasts" />  
		<richui:tabLabel title="Broadcasts of Interest" />  
	</richui:tabLabels>

	<richui:tabContents> 
		<richui:tabContent> 


		</richui:tabContent>

		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/wtb' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/wts' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/privatebcast' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/wtbresponds' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/wtsresponds' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/rfq' height="100%" width="100%"></iframe>
		</richui:tabContent>
		<richui:tabContent>
			<iframe id='webtabsframe' src='/test2/myActivity/rfqresponds' height="100%" width="100%"></iframe>
		</richui:tabContent>


	</richui:tabContents>
 
</richui:tabView>
        </div>
    </body>
</html>
