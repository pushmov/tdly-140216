<script language="javascript" >
 $(document).ready(function() {

	 $('#fileInput').customFileInput();	
	 $('input').customInput();

           $('#uploadInventory').live("click", function() {
                        $('#uploadform').submit();
              });


 });

</script>

<g:form name="uploadform" controller="inventory" method="post" action="uploadInventory" enctype="multipart/form-data"  >
<g:hasErrors bean="${session.inventory}">
	<div class="errors">
		<g:renderErrors bean="${session.inventory}" as="list" />
	</div>
	<br/>
</g:hasErrors>

								<div>
									<div class="label">Inventory File:</div>
									<div class="inputs">
										<span class="input_wrapper"><input id="fileInput" class="file" name="file" type="file" /></span>
									</div>
								</div>
								<div class="row" >&nbsp;</div>

				    <g:if test="${flash.inventoryexists == 'true'}">

		<input type="radio" name="invaction"  id="Overwrite" value="Overwrite" />
		<label for="Overwrite">Overwrite Existing Inventory</label>
		
		<input type="radio" name="invaction"  id="Update"  value="Update" />
		<label for="Update">Update Existing Inventory</label>

				    </g:if>
				    <g:else>
					<input type="hidden"  name="invaction"  id="Overwrite" value="Overwrite"  />
				    </g:else>
								<div class="row" >&nbsp;</div>

							 <button type="button" class="btn btn-primary btn-lg" id="uploadInventory">Upload Inventory</button>


</g:form>                        


