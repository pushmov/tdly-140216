

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Company List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Company</g:link></span>
        </div>
        <div class="body">
            <h1>Company List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	                                
                   	        <th>Company</th>
                   	        <th>Level</th>
                   	        <th>Country</th>
                   	        <th>WTB Broadcasts</th>
                   	        <th>WTS Broadcasts</th>
                   	        <th>RFQ Broadcasts</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${companyList}" >
                        <tr>

<%
	def Long cid = it[0]
%>			  
                            <td><a href="../company/view?it=${cid}">${it[1]}</a></td>
                        
                            <td>${it[2]}</td>
                            <td>${it[3]}</td>
                        
                            <td>${it[4]}</td>
                        
                            <td>${it[5]}</td>

                            <td>${it[6]}</td>
                            <td>
                            <g:form name="myForm" url="[action:'addtovendor',controller:'company']">
				<input type=hidden name=id value="<%=cid%>" > 
				<input type=submit name=submit value="Add To Vendorlist" >
	
			    </g:form>

                            </td>
                            
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
