<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		if(session.person?.address?.timezone)
		sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
			
%>
	
			<div class="tab-content-right">
				<div class="tabToggle">
					<a href="#" class="arrow-icon"></a>
				</div>
				
				<!--tabToggle-->
				<div class="tab-filter-info clearfix">
					<dl class="dl-horizontal" >
						<dt>Broadcast Type Filter :</dt>
						<dd><g:each in="${broadcastType}" >${it},&nbsp;</g:each></dd>
						<dt>Country Filter:</dt>
						<dd><g:each in="${countrySel}" >${it},&nbsp;</g:each></dd>
						<dt>Company Filter:</dt>
						<dd><g:each in="${companySel}" >${it},&nbsp;</g:each></dd>
					</dl>
				</div><!--tab-filter-info-->
				
				<div class="tab-utility-button">
					<button class="btn btn-primary btn-lg" type="button" data-toggle="modal" data-target="#modal_multi_part_broadcasts">Broadcast</button>
				</div>
				
				<!--tab-utility-button-->
				<div class="toolbar-btn-group" style="display:none">
					<div class="btn-group btn-group-sm" role="group" aria-label="sidebar button group">
						<button type="button" class="btn btn-light">
							<i class="fa fa-check-square-o" aria-hidden="true"></i> Select All
						</button>
						<button type="button" class="btn btn-light">
							<i class="fa fa-archive" aria-hidden="true"></i> Archive
						</button>
						<button type="button" class="btn btn-light">
							<i class="fa fa-trash-o" aria-hidden="true"></i> delete
						</button>
					</div>
				</div>
				<!--toolbar-btn-group-->
				
				<g:render template="broadcastRowsMultiplePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,filterParams:flash.filterParams]}" />
				
				<!-- Popup for multi part broadcasts-->
        <div class="modal fade broadcasts-popup" id="modal_multi_part_broadcasts" tabindex="-1" role="dialog" aria-labelledby="modal_multi_part_broadcastsLabel" aria-hidden="true">
          <div class="modal-dialog">
						
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_multi_part_broadcastsLabel">Create Broadcast</h4>
              </div>
							
						<div id="createmultiple" class="form-content"></div>	
              
            </div>
						
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
        </div>
        <!-- Popup for multi part broadcasts-->

			<!--inner-contrightpan start --> 
			<div class="inner-contrightpan" style="display:none">
			<div id="hide_and_show_filter_active_multiple" onClick="hide_and_show1()" class="hide_and_show_filter_active"></div>

                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                                <tr>
                                                        <td align="left" valign="top" width="90%">
                                         <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" class="conttext">
                                               <tr>
                                        <td width="16%" align="left" valign="top" class="gtext">Broadcast Type Filter</td>
                                        <td width="1%" align="left" valign="top" class="gtext">:</td>
                                        <td width="70%" align="left" valign="top" class="asstext"><g:each in="${broadcastType}" >${it},&nbsp;</g:each>   </td>
                                  </tr>
                                                   <tr>
                                                        <td align="left" valign="top" class="gtext">Country Filter</td>
                                                        <td align="left" valign="top" class="gtext">:</td>
                                                        <td align="left" valign="top" class="asstext"><g:each in="${countrySel}" >${it},&nbsp;</g:each></td>
                                                  </tr>
                                                   <tr>
                                                        <td align="left" valign="top" class="gtext">Company Filter</td>
                                                        <td align="left" valign="top" class="gtext">:</td>
                                                        <td align="left" valign="top" class="asstext"><g:each in="${companySel}" >${it},&nbsp;</g:each> </td>
                                                  </tr>
                                         </table>
                                       </td>
                                        <td align="right" valign="top" width="10%">
                                           <div class="i-headerwrapper1">
                                                <ul>
							<li>
								<!--<a href="javascript:void(0)" onClick="javascript:broadcastM();"  title="Home" class="active"><span>Broadcast</span></a>-->
								<button onClick="javascript:broadcastM();" type="button" title="Home" class="btn btn-broadcast">Broadcast</button>
                                                        </li>
                                                </ul>
                                           </div>
                                        </td>
                                   </tr>

                                </table>



                <div class="spacer"></div>

<div id="tableRowM" >
          <g:render template="broadcastRowsMultiplePart" model="${[broadcastList:broadcastList,countryMap:countryMap,companyMap:companyMap,filterParams:flash.filterParams]}" />
</div>
			<!--inner-contrightpan end -->
			</div>
			<script>
				$('.tabToggle a').on('click', function(event){
					
					event.preventDefault();
					$(this).toggleClass('is-active');
					var contentBlock = $(this).closest('.tab-content-block').toggleClass('is-open');
				});
				
				$(document).ready(function(){
					$('button[data-target="#modal_multi_part_broadcasts"]').click(function(){
						
					$('#createmultiple').html('');
	
						$.get('createmultiple', function(res){
							$('#createmultiple').html(res);
						});
					});
				});
			</script>
