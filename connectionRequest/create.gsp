

<%@ page import="test2.ConnectionRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'connectionRequest.label', default: 'ConnectionRequest')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${connectionRequestInstance}">
            <div class="errors">
                <g:renderErrors bean="${connectionRequestInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="message"><g:message code="connectionRequest.message.label" default="Message" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: connectionRequestInstance, field: 'message', 'errors')}">
                                    <g:textField name="message" value="${connectionRequestInstance?.message}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="contact"><g:message code="connectionRequest.contact.label" default="Contact" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: connectionRequestInstance, field: 'contact', 'errors')}">
                                    <g:select name="contact.id" from="${test2.Person.list()}" optionKey="id" value="${connectionRequestInstance?.contact?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="person"><g:message code="connectionRequest.person.label" default="Person" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: connectionRequestInstance, field: 'person', 'errors')}">
                                    <g:select name="person.id" from="${test2.Person.list()}" optionKey="id" value="${connectionRequestInstance?.person?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
