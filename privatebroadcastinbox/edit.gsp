

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Privatebroadcastinbox</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Privatebroadcastinbox List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Privatebroadcastinbox</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Privatebroadcastinbox</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${privatebroadcastinboxInstance}">
            <div class="errors">
                <g:renderErrors bean="${privatebroadcastinboxInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${privatebroadcastinboxInstance?.id}" />
                <input type="hidden" name="version" value="${privatebroadcastinboxInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="myvendor">Myvendor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastinboxInstance,field:'myvendor','errors')}">
                                    <g:select optionKey="id" from="${MyVendor.list()}" name="myvendor.id" value="${privatebroadcastinboxInstance?.myvendor?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="pbcast">Pbcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:privatebroadcastinboxInstance,field:'pbcast','errors')}">
                                    
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
