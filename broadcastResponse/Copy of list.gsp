

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>BroadcastResponse List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New BroadcastResponse</g:link></span>
        </div>
        <div class="body">
            <h1>BroadcastResponse List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
<gui:tabView>
    <gui:tab label="Tab 1" active="true">
        <h1>Inside Tab 1</h1>
        <p/>You can put whatever markup you want here.
            <div class="list">
                <table>
                    <thead>
                        <tr>
                   	        <g:sortableColumn property="broadcast.created" title="Broadcast Date" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="company" title="Company" />
                   	        <g:sortableColumn property="fromid" title="Contact Name" />
                   	        <g:sortableColumn property="respdate" title="Response Date" />
					  <th>Response text</th>                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastResponseList}" status="i" var="broadcastResponse">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'created')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'qty')}</td>
                        
                            <td>${fieldValue(bean:broadcastResponse.company, field:'name')}</td>
                        
                            <td>${fieldValue(bean:broadcastResponse, field:'fromid')}</td>
                        
                        
                            <td>${fieldValue(bean:broadcastResponse, field:'respdate')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'responsetext')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${BroadcastResponse.count()}" />
            </div>
    </gui:tab>
    <gui:tab label="Tab 2">
        <h1>Inside Tab 2</h2>
        <gui:richEditor id='editor' value="You can use gui components within tabs, too!"/>
    </gui:tab>
</gui:tabView>
        </div>
    </body>
</html>
