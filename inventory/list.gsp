<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Inventory" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta name="layout" content="pz" />
<link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">

</head>

<r:script disposition="defer">
	$(document).ready(function() {
		$('#dialog-broadcast, #dialog-broadcast-multiple, #dialog-bmessage').hide();
			
		$(document).on('click', '#addinvent', function(e){
			e.preventDefault();
                        $.post('../inventory/save', $(this).closest('form').serialize(), function(data){
                            if(data.indexOf("errors") != -1) {
                                $('#dialog-addInventory').html(data);
                            } else {
                                window.location.reload(true);
                            }
                        });
		});
			

    $("#add_inventory").click(function() {

        $('#dialog-addInventory').dialog('open');
    });

	    
        });

function broadcast(part,manufacturer,condition,price,quantity,mfgid) {
	$('button[data-target="#modal_single_part_broadcasts"]').click();
    $('.modal-body .errors').hide();
	$.get('../broadcast/createsingle', function(res){
		$('#createsingle').html(res);
		$(document).find('#createsingle #part').val(part);
		$(document).find('#createsingle #qty').val(quantity);
		$(document).find('#createsingle #price').val(price);
		$(document).find('#createsingle select[name="condition.id"]').val(condition);
		$(document).find('#createsingle select[name="manufacturer.id"]').val(mfgid);
		
	});
}

function submitInventory() {
			    var id = "";
			    var formData = $('#editInventory').serializeArray();
 			    $.each(formData, function(i, field) {
				if(field.name == "id" ) {
					id = field.value;
				}
                            });
			    $.ajax({
    			    type: "POST",
    			    url: "../inventory/update",
			    cache: false,
    			    data: $('#editInventory').serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   if(data.indexOf("errors") != -1) {
					$('#dialog-editInventory').html(data);
				   }
				   else {
					$("#main_table > tbody > tr[id='" + id + "']").replaceWith(data);
					$('#modal_watchlist_success #response').html('Part Changes saved.');
					$('button[data-target="#modal_watchlist_success"]').click();
					
					//$('#add').html('Part Changes saved.');
				   	//$('#dialog-editInventory').dialog("close");
				   	//$('#dialog-editmessage').dialog("open");
				   }
 				   //setTimeout(function(){$('#dialog-message').dialog("close")},2000);
    			    }
			    
                            });
		    
}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}



function deleteInventory(invid,element) {
        
        $('button[data-target="#confirm-delete"]').click();
	$('#confirm-delete .btn-delete').attr('data-id', invid);
}

$('.btn-delete').click(function(){
	
	var invid = $(this).attr('data-id');
	var url = "../inventory/delete";
        
	$.ajax({
		type: "POST",
		url: url,
		cache: false,
		data: { id : invid },
		error: function(data, textStatus, jqXHR) {
			alert("Error: " + textStatus);
		},
		success: function(data, textStatus, jqXHR) {
			window.location.reload(true);
		}
	});
});



function editInv(invid,part,manufacturer,condition,price,quantity,mfgid) {
        
	$('#editInventory #id').val(invid);
	$('#editInventory #part').val(part);
	$('#editInventory #qty').val(quantity);
	$('#editInventory #price').val(price);
	
	var manufactur_val = $('option:contains("'+manufacturer+'")').attr('value');
	$('select[name="manufacturer.id"]').val(manufactur_val);
	$('#editInventory .selectpicker[data-id="manufacturer.id"]').attr('title', manufacturer);
	$('#editInventory .selectpicker[data-id="manufacturer.id"] .filter-option').text(manufacturer);
	$('#editInventory .dropdown-menu.inner.selectpicker li').removeClass('selected');
	$('#editInventory .dropdown-menu.inner.selectpicker:visible li[rel="'+manufactur_val+'"]').prev().addClass('selected');
	
	var condition_txt = $('#editInventory select[name="condition.id"] option[value="'+condition+'"]').text();
	$('select[name="condition.id"]').val(condition);
	$('#editInventory .selectpicker[data-id="condition.id"]').attr('title', condition_txt);
	$('#editInventory .selectpicker[data-id="condition.id"] .filter-option').text(condition_txt);
	$('#editInventory .dropdown-menu.inner.selectpicker li').removeClass('selected');
	//$('#editInventory .dropdown-menu.inner.selectpicker:visible li[rel="'+condition+'"]').prev().addClass('selected');
	$('.dropdown-menu.inner.selectpicker li:contains("'+condition_txt+'")').addClass('selected')
	
	
	
    $('button[data-target="#edit_inventory"]').click();
}

function addToWatchlist(invid) {


			    $.ajax({
    			    type: "POST",
    			    url: "../inventory/addwatchlist",
			    cache: false,
    			    data: { id : invid },
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
                                
                                if(data.success === false){
                                    $('#modal_watchlist_success .modal-title').text('Error');
                                }
                                
                                $('#modal_watchlist_success #response').text(data.message);
				$('button[data-target="#modal_watchlist_success"]').click();
    			    }
			    
                            });



}

function getFilter(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {
		
			//alert($.toJSON(data));
			//alert('filterid : ' + data.id);

			$('#editInventory #part').val(data.id);
			$('#name').val(data.name);
			$('#keyword').val(data.keyword);

			if(data.wtb) {
				$('#wtb').attr('checked', true);
			}
			if(data.wts) {
				$('#wts').attr('checked', true);
			}
			if(data.myVendors) {
				$('#myVendors').attr('checked', true);
			}
			$('input:radio[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('checked', true);
		
			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
			}
				$("#manufacturer").val(mfgArr);

			$("#continents option:first").attr('selected','selected');
		        $("#countrySelect").load("../continent/updateSelectFilter",{continentid: $('#continents').val(),prevcontinentid : prevSelect ,countryid: null, domain: 'broadcastFilter' , ajax: 'true'});
			prevSelect = $(this).val();

		


			//$('#dialog-form').binddata(data);
			$('#dialog-form').dialog('open');
			

                }
             );
	
 }
 
</r:script>

<main class="content" role="main">
	<div class="main-content">
		<g:isSelf person="${person}" >
		<div style="margin-bottom:15px">
		<button id="addinventory" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#add_inventory" type="button">Add Inventory</button>
		</div>
		</g:isSelf>

		<div class="table-responsive">
			<table class="table table-bordered gray-header responsive">
				<thead>
					<tr>
						<g:sortableColumn property="part"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Part" />
						<g:if test="${actionName == 'search'}">
							<g:sortableColumn property="mfgname"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Mfg" />
						</g:if>
						<g:else>
							<g:sortableColumn property="manufacturer"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Mfg" />
						</g:else>
						<g:sortableColumn property="condition"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Condition" />
						<g:sortableColumn property="price"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Price" />
						<g:sortableColumn property="qty"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Qty" />
						<g:sortableColumn property="lastUpdated"  params="${[keyword: flash.keyword, searchtype:flash.searchtype]}" title="Updated" />
						<th>Actions</th>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		//println "TimeZone " + session.company?.address?.timezone
		if (session.company?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.company?.address?.timezone));
		}
%>
					</tr>
				</thead>
				<tbody>
					<g:each in="${inventoryList}" status="i" var="inventory">
						<g:render template="inventoryRow" var="inventory" bean="${inventory}" model="[i:i]" />
					</g:each>
				</tbody>
			</table>
		</div>
		
		<div class="pagination_n">
			<g:paginate  params="[keyword: flash.keyword, searchtype: flash.searchtype ]" total="${total}"  />
		</div>

	</div>	
</main>

                        <button data-toggle="modal" style="display: none" data-target="#modal_single_part_broadcasts"></button>
<div class="modal fade broadcasts-popup" id="modal_single_part_broadcasts" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Broadcast</h4>
              </div>
              <!--modal-header-->
              <div id="createsingle" class="form-content">
                    <!--<g:render template="addInventory" />-->
              </div>
            </div>
            <!--modal-content-->
          </div>
          <!--modal-dialog-->
</div>

<button data-toggle="modal" style="display: none" data-target="#edit_inventory"></button>
<div class="modal fade broadcasts-popup" id="edit_inventory" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Edit Inventory</h4>
			</div>
			<g:render template="editInventory" />
		</div>
	</div>
</div>

<button data-toggle="modal" style="display: none" data-target="#confirm-delete"></button>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">Delete this inventory item ?</div>
			<div class="modal-body">
				Delete this inventory item ?
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
				<a data-id="" class="btn btn-danger btn-delete">Delete</a>
			</div>
		</div>
  </div>
</div>

<div class="modal fade broadcasts-popup" id="add_inventory" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Add Inventory</h4>
			</div>
			<g:render template="addInventory" />
		</div>
	</div>
</div>


<button data-toggle="modal" style="display: none" data-target="#modal_watchlist_success"></button>
<div class="modal fade broadcasts-popup" id="modal_watchlist_success" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal_single_part_broadcastsLabel">Success</h4>
            </div>
            <!--modal-header-->
            <div class="modal-body">
                <div id="response" class="form-content">
                    Broadcast Sent.
                </div>
            </div>
        </div>
        <!--modal-content-->
    </div>
    <!--modal-dialog-->
</div>


<script>
$(document).ready(function(){
	$(document).on('click', '#submit-broadcast-single', function(){
		var form = $(this).closest('form');
                form.find('.loader').removeClass('hide');
		$.post(form.attr('action'), form.serialize() + "&description=" + $('#fsingle .Editor-editor').html(), function(response){
			if(response == 'good'){
				//$('#fsingle .ajax-resp').text('Success');
				setTimeout(function(){
                                        form.find('.loader').addClass('hide');
					form.find('button[data-dismiss="modal"]').click();
					$('.modal-backdrop').remove();
					$('#modal_watchlist_success #response').html('Broadcast Sent.');
					$('button[data-target="#modal_watchlist_success"]').click();
					$('.ajax-resp').text('');
				}, 1000);
				
			} else {
				$('#createsingle').html(response);
			}
		});
		
	});
});
</script>

</html>