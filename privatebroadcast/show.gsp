

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show Privatebroadcast</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Privatebroadcast List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Privatebroadcast</g:link></span>
        </div>
        <div class="body">
            <h1>Show Privatebroadcast</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Company:</td>
                            
                            <td valign="top" class="value"><g:link controller="company" action="show" id="${privatebroadcastInstance?.company?.id}">${privatebroadcastInstance?.company?.encodeAsHTML()}</g:link></td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Date Created:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'dateCreated')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Createdby:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'createdby')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Noofresponses:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'noofresponses')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Broadcasttype:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'broadcasttype')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Description:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'description')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Unit:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:privatebroadcastInstance, field:'unit')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${privatebroadcastInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
