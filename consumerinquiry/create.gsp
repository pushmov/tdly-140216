

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Consumerinquiry</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Consumerinquiry List</g:link></span>
        </div>
        <div class="body">
            <h1>Create Consumerinquiry</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${consumerinquiryInstance}">
            <div class="errors">
                <g:renderErrors bean="${consumerinquiryInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="alternatephone">Alternatephone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'alternatephone','errors')}">
                                    <input type="text" id="alternatephone" name="alternatephone" value="${fieldValue(bean:consumerinquiryInstance,field:'alternatephone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="city">City:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'city','errors')}">
                                    <input type="text" id="city" name="city" value="${fieldValue(bean:consumerinquiryInstance,field:'city')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comments">Comments:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'comments','errors')}">
                                    <input type="text" id="comments" name="comments" value="${fieldValue(bean:consumerinquiryInstance,field:'comments')}"/>
                                </td>
                            </tr> 
                        
                                    <input type="hidden" name="companyid" value="1" >
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="consumeremail">Consumeremail:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'consumeremail','errors')}">
                                    <input type="text" id="consumeremail" name="consumeremail" value="${fieldValue(bean:consumerinquiryInstance,field:'consumeremail')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="country">Country:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'country','errors')}">
                                    <input type="text" id="country" name="country" value="${fieldValue(bean:consumerinquiryInstance,field:'country')}"/>
                                </td>
                            </tr> 
                        
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="firstname">Firstname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'firstname','errors')}">
                                    <input type="text" id="firstname" name="firstname" value="${fieldValue(bean:consumerinquiryInstance,field:'firstname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="lastname">Lastname:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'lastname','errors')}">
                                    <input type="text" id="lastname" name="lastname" value="${fieldValue(bean:consumerinquiryInstance,field:'lastname')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="partno">Partno:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'partno','errors')}">
                                    <input type="text" id="partno" name="partno" value="${fieldValue(bean:consumerinquiryInstance,field:'partno')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="phone">Phone:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'phone','errors')}">
                                    <input type="text" id="phone" name="phone" value="${fieldValue(bean:consumerinquiryInstance,field:'phone')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="postalcode">Postalcode:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'postalcode','errors')}">
                                    <input type="text" id="postalcode" name="postalcode" value="${fieldValue(bean:consumerinquiryInstance,field:'postalcode')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="state">State:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:consumerinquiryInstance,field:'state','errors')}">
                                    <input type="text" id="state" name="state" value="${fieldValue(bean:consumerinquiryInstance,field:'state')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
