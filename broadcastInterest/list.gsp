
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Inbox - Public Broadcasts</title>


    </head>
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
							
						<div class="title_wrapper">
							
							<ul class="search_tabs">
								<li><g:link  url="[controller:'broadcastResponse', action:'bcastresponse']"   ><span><span>Broadcast Inbox</span></span></g:link></li>
								<li><g:link  url="[controller:'privatebroadcastinbox', action:'list']"   ><span><span>Private Broadcast Inbox</span></span></g:link></li>
								<li><g:link  id="selected_search_tab"   url="[controller:'broadcastInterest', action:'list']"   ><span><span>Broadcasts of Interest</span></span></g:link></li>
								<li><g:link   url="[controller:'consumerinquiry', action:'list']"   ><span><span>Consumer Inquiry</span></span></g:link></li>
								<li><g:link   url="[controller:'myVendor',action:'listrequests']"   ><span><span>Vendor Requests</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						<div id="product_list_menu">
							<!--[if !IE]>start product list tabs<![endif]-->
							<ul id="product_list_tabs">
								<li><a href="#" class="selected"><span><span>Featured</span></span></a></li>
								<li><a href="#"><span><span>All products</span></span></a></li>
								<li><a href="#"><span><span>Best Sellers</span></span></a></li>
							</ul>
							<!--[if !IE]>end product list tabs<![endif]-->
							<a href="#" class="update"><span><span><em>Add New Record</em><strong></strong></span></a>
						</div>

 			<g:form action="delete" >
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>


                        
                   	        <g:sortableColumn property="broadcasttype" title="Broadcasttype" />
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
					   <td>Delete</td>
                        
                        </tr>
                    <g:each in="${broadcastInterestInstanceList}" status="i" var="broadcastInterestInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast, field:'broadcasttype')}</td>
                            <td>${(broadcastInterestInstance.broadcast.dateCreated)}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcastInterestInstance.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${broadcastInterestInstance.broadcast.company.name}</a></td>
					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:broadcastInterestInstance.broadcast, field:'id')}');" >Respond</a>
					</td>
                       <td> 
                    		<g:checkBox name="check${broadcastInterestInstance?.id}" value="false"  />  
			     </td>

                        </tr>
                    </g:each>



							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						</g:form>
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Delete</em></span></span><input name="" onclick="return confirm('Are you sure?');"  type="submit" /></span>
									</div>
								</div>

						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
                                 <g:paginate controller="broadcastResponse" action="list" total="${biTotal}"  params="[activetab: 4]" /> 
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
				
				
				
				
			</div>
			<!--[if !IE]>end page<![endif]-->
			
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</html>
