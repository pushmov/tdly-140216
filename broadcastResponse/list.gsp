<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Person" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.Continent" %>
<%@page import="test2.AppFlags" %>

<meta name="layout" content="pz" />
<r:script language="Javascript" >


$(document).ready(function()
        {
     $('#dialog-moreMessage').dialog({
        	autoOpen: false,
	        height: 500,
        	width: 700,
	        modal: true,
        	buttons: {
		            OK: function() {
                		$(this).dialog('close');
            		    }

	        }        
             });


    $('#dialog-description').dialog({
                autoOpen: false,
                height: 500,
                width: 700,
                modal: true,
                buttons: {
                            OK: function() {
                                $(this).dialog('close');
                            }

                }
             });


	    $('#dialog-connectionRequest').dialog({
        	autoOpen: false,
	        height: 250,
        	width: 350,
	        modal: true,
        	buttons: {
		            Ok: function() {
        		        $(this).dialog('close');
            		    }

	        }        
             });






}); 



function moreMsg(respid) {


$.get("../broadcastResponse/moreResponse",{respid : respid},
		function(data,textStatus,jqXHR) {
		    $("#moreMessage").html(data.responsetext);
		    $('#dialog-moreMessage').dialog('open');
                }
);

}

function broadcastDesc(id) {


$.get("../broadcast/description",{id : id},
                function(data,textStatus,jqXHR) {
                    $("#description").html(data.description);
                    $('#dialog-description').dialog('open');
                }
);

}

function acceptConnection(id, element) {
		

			    $.ajax({
    			    type: "POST",
    			    url: "../myConnection/acceptConnection",
			    cache: false,
    			    data: { id : id  },
			    error: function(data, textStatus, jqXHR) {
				   $('#connectionRequestMessage').html(data.msg);
				   $('#dialog-connectionRequest').dialog("open");
				 
    			    },
    			    success: function(data, textStatus, jqXHR) {
                                   $(element).parent().parent().parent().parent().parent().parent().toggle();
				   $('#connectionRequestMessage').html(data.msg);
				   $('#dialog-connectionRequest').dialog("open");
    			    }
			    
                            });

		
	}


function deleteConnectionRequest(id,element) {


                            $.ajax({
                            type: "POST",
                            url: "${request.contextPath}/connectionRequest/delete",
                            cache: false,
                            data: { id : id },
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
                                   $(element).parent().parent().parent().parent().parent().parent().parent().toggle();

                            }

                            });



}


function deleteInbox(id,element,messageType) {


			var url = '${request.contextPath}/broadcastResponse/deleteInbox';
                            $.ajax({
                            type: "POST",
                            url: url,
                            cache: false,
                            data: { id : id ,messageType:messageType},
                            error: function(data, textStatus, jqXHR) {
                                alert("Error: " + textStatus);
                            },
                            success: function(data, textStatus, jqXHR) {
				location.reload();

                            }

                            });



}
</r:script>




<div id="dialog-connectionRequest" title="MyConnection"> 
	<p> 
		 <div id="connectionRequestMessage">hello</div> 
	</p> 
</div> 


<g:render template="/dialog-broadcast-response" />

<div id="dialog-moreMessage" title="Message"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		<div id="moreMessage" ></div> 
	</p> 
</div>

<div id="dialog-description" title="Broadcast">
	<p>
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
		<div id="description"></div>
	</p>
</div>

<g:render template="bcastresponse"  model="[brTotal:brTotal,messageType:params.messageType]"   />