

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit Alert</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Alert List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Alert</g:link></span>
        </div>
        <div class="body">
            <h1>Edit Alert</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${alertInstance}">
            <div class="errors">
                <g:renderErrors bean="${alertInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${alertInstance?.id}" />
                <input type="hidden" name="version" value="${alertInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="comments">Comments:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:alertInstance,field:'comments','errors')}">
                                    <input type="text" id="comments" name="comments" value="${fieldValue(bean:alertInstance,field:'comments')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="alerttype">Alerttype:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:alertInstance,field:'alerttype','errors')}">
                                    <input type="text" id="alerttype" name="alerttype" value="${fieldValue(bean:alertInstance,field:'alerttype')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="broadcast">Broadcast:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:alertInstance,field:'broadcast','errors')}">
                                    <g:select optionKey="id" from="${Broadcast.list()}" name="broadcast.id" value="${alertInstance?.broadcast?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="company">Company:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:alertInstance,field:'company','errors')}">
                                    <g:select optionKey="id" from="${Company.list()}" name="company.id" value="${alertInstance?.company?.id}" ></g:select>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
