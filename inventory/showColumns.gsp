<%@page import="pz.Excelcolumn" %>
<html>
    <head>
        <meta name="layout" content="pz" />
        <script src="/tdly/plugins/jquery-1.7.2/js/jquery/jquery-1.7.2.min.js" type="text/javascript" ></script>
        <script src="/tdly/js/pz/jquery.dd.js" type="text/javascript" ></script>
        <link rel="stylesheet" href="/tdly/css/pz/dd.css" />
        <script language="Javascript" >
        $(document).ready(function() {
                $('#saveMapping').click( function() {
                        $('#columnMap #skip').val('true');
                        $('#columnMap').submit();
                });
                $('#skipMapping').click( function() {
                        $('#columnMap #skip').val('false');
                        $('#columnMap').submit();
                });


                        try {
                                var oHandler7 = $(".msdropdown").msDropDown({mainCSS:'dd2'}).data("dd");
            } catch(e) {
                                console.log("Error: "+e.message);
            }

                        $('#dialog-broadcast, #dialog-broadcast-multiple, #dialog-bmessage').hide();
        });
        </script>
        
        <link href="${createLinkTo(dir:'assets',file:'partszap/css/app.css')}" rel="stylesheet">
    </head>

<main class="content" role="main">
    <div class="main-content">
        <p>* Select the correct column names from the dropdowns for those columns which have non standard column names in the excel </p>
        
        <g:form name="columnMap" action="savecolumns" method="post" class="form-horizontal">
            <input type=hidden name=skip id=skip value="" />
            <table class="table table-bordered gray-header responsive">
                <tbody>
                    <tr>
                        <td align="left" valign="top" width="10%">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="100%" align="left" valign="middle" class="deepcont">Excel Header</td>
                                </tr>
                                <tr>
                                    <td width="100%" align="left" valign="middle" class="deepcont">Excel Data</td>
                                </tr>
                            </table>
                        </td>
                        <td>
                        <table class="table table-bordered gray-header responsive">
                            <thead>
                                <tr>
                                    <g:each in="${session.excelcolumns}" > 
                                    <% if (flash.invalidColumns.contains(it.key)) { %>
                                    <th>${it.key}</th>
                                    <% } %>
                                    </g:each>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <g:each in="${session.excelcolumns}" > 
                                    <% if (flash.invalidColumns.contains(it.key)) { %>
                                    <td>${it.value.headerName}</td>
                                    <% } %>
                                    </g:each>
                                </tr>
                                <tr>
                                    <g:each in="${session.excelcolumns}" > 
                                    <% if (flash.invalidColumns.contains(it.key)) { %>
                                    <td>&nbsp;</td>
                                    <% } %>
                                    </g:each>
                                </tr>
                                <tr>
                                    <g:each in="${session.excelcolumns}" > 
                                    <% if (flash.invalidColumns.contains(it.key)) { %>
                                    <td>${it.value.dataValue}</td>
                                    <% } %>
                                    </g:each>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <div class="clearfix">
                <div class="row">
                    <div class="col-sm-2">
                        <p>*PZ Standard Columns</p>
                    </div>
                    
                    <g:each in="${session.excelcolumns}" >
                        <% if (flash.invalidColumns.contains(it.key)) { %>
                        <div class="col-sm-3">
                            <g:select class="form-control" name="${it.key}" from="${headings}"></g:select>
                        </div>
                        <% } %>
                    </g:each>
                    
                </div>
            </div>
            
            <div style="margin-top: 75px">
                <button id="saveMapping" class="btn btn-small btn-primary">Save Column Mapping</button>
                <button id="skipMapping" class="btn btn-small btn-primary">Don't Save Mapping</button>
            </div>
            
        </g:form>
    </div>
</main>
    
<div class="eachtab" id="tab-1" style="display:none">
<div class="spacer"></div>
         <h2>* Select the correct column names from the dropdowns for those columns which have non standard column names in the excel </h2>
<br><div class="spacer"></div>
          <g:form name="columnMap" action="savecolumns" method="post" >
		<input type=hidden name=skip id=skip value="" />
            
			
			

			<div class="btn-grp-showcolumn">
         <div class="spacer"></div>
						<div class="buttonsave">
							<a href="#" id="saveMapping"><span>Save Column Mapping</span></a>
						</div>
						<div class="buttonsave">
							<a href="#" id="skipMapping" ><span>Don't save mapping<span>
						</div>
						<span class="sixpagetext">* Select the correct column names from the dropdowns for those columns which have non standard column names in the excel </span>
			</div>
 </g:form>
    
		</div>
           
</html>
