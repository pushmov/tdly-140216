					<table valign="top">
                        <tr valign="top">
                   	        <g:sortableColumn property="broadcast.created" title="Broadcast Date" />
                   	        <g:sortableColumn property="broadcast.created" title="Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="company" title="Company" />
                   	        <g:sortableColumn property="fromid" title="Contact Name" />
                   	        <g:sortableColumn property="respdate" title="Response Date" />
                   	        <g:sortableColumn property="respdate" title="Country" />
					  <th>Response text</th>                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastResponseList}" status="i" var="broadcastResponse">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast, field:'broadcasttype')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcastResponse.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcastResponse.company, field:'name')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'fromid')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'country')}</td>
                            <td>${fieldValue(bean:broadcastResponse, field:'responsetext')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
