

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>TradeUnit List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New TradeUnit</g:link></span>
        </div>
        <div class="body">
            <h1>TradeUnit List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="condition" title="Condition" />
                        
                   	        <g:sortableColumn property="manufacturer" title="Manufacturer" />
                        
                   	        <g:sortableColumn property="part" title="Part" />
                        
                   	        <g:sortableColumn property="price" title="Price" />
                        
                   	        <g:sortableColumn property="qty" title="Qty" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${tradeUnitList}" status="i" var="tradeUnit">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${tradeUnit.id}">${fieldValue(bean:tradeUnit, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:tradeUnit, field:'condition')}</td>
                        
                            <td>${fieldValue(bean:tradeUnit, field:'manufacturer')}</td>
                        
                            <td>${fieldValue(bean:tradeUnit, field:'part')}</td>
                        
                            <td>${fieldValue(bean:tradeUnit, field:'price')}</td>
                        
                            <td>${fieldValue(bean:tradeUnit, field:'qty')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${TradeUnit.count()}" />
            </div>
        </div>
    </body>
</html>
