<div class="modal fade broadcasts-popup" id="modal_login" tabindex="-1" role="dialog" aria-labelledby="modal_single_part_broadcastsLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="modal_single_part_broadcastsLabel">Login</h4>
			</div>
			
			<form action='${postUrl?postUrl:"/tdly/j_spring_security_check"}' method='POST' id="loginForm" class="form-horizontal" name="loginForm" >
				<div class="modal-body">
					<h1 class="text-center">Login Form</h1>
					<div class="text-center alert-danger" id="errorLoginMsg"></div>
					<div class="form-group">
						<label for="j_username" class="col-sm-2 control-label">Username:</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" id="username" name="j_username">
						</div>
					</div>
					
					<div class="form-group">
						<label for="j_password" class="col-sm-2 control-label">Password:</label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="password" name="j_password">
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-8">
							<input type="checkbox" value="Remember me" safari="1" name="_spring_security_remember_me" id="remember_me" checked="checked" >Remember me
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-8">
							<a href="javascript:void(0)" onclick="forgotPassword();return false;">Forgot Your Password?</a>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-8">
							<button type="button" class="btn btn-primary btn-lg signupBtn" onclick="authAjax();return false;">Login</button>
						</div>
						<div class="col-sm-2 loader-wrapper"></div>
					</div>
				</div>
			</form>
			
		</div>
	</div>
</div>

<script>
$(document).ready(function() {
	$('#username').focus();
});
</script>

