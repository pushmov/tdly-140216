<%@page import="test2.Manufacturer" %>
<%@page import="test2.Cond" %>

								<div class="row">
									<label>Condition:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
					                                    <g:select from="${Cond.list()}" optionKey="id" optionValue="name" name="condition" value="" ></g:select>
										</span>
									</div>
								</div>
								<div class="row">
									<label>Manufacturer:</label>
									<div class="inputs">
										<span class="input_wrapper select_wrapper">
					                                    <g:select optionKey="id" from="${Manufacturer.list()}" optionValue="mfgname" name="manufacturerid" value="" ></g:select>
										</span>
									</div>
								</div>

								<div class="row">
									<label>Part:</label>
									<div class="inputs">
										<span class="input_wrapper">                                    
											<input type="text" class="text"  id="part" name="part" value=""/>
										</span>
									</div>
								</div>


								<div class="row">
									<label>Price:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input type="text" class="text"  id="price" name="price" value="" />
										</span>
									</div>
								</div>
								<div class="row">
									<label>Quantity:</label>
									<div class="inputs">
										<span class="input_wrapper">
											<input type="text" class="text" id="qty" name="qty" value="" />
										</span>
									</div>
								</div>


