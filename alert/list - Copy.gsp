<%@page import="java.text.*" %>
<%@page import="java.util.*" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>Alert List</title>
	<g:javascript library="jquery" plugin="jquery" />
	<jqui:resources/>
        <jqDT:resources/>               	                

	<jqval:resources />
        <jqvalui:resources />


    </head>
    <body> 
        <div class="body">

<script> 

$(document).ready(function()
        {

	$( "#tabs" ).tabs();
});


function getCompany(cid) {

Ext.Ajax.request({
	url : '/test2/company/showajax' , 
	params : { id : cid },
	method: 'POST',
	success: function ( result, request ) { 
		showCompany(result.responseText); 
	},
	failure: function ( result, request) { 
		Ext.MessageBox.alert('Failed', result.responseText); 
	} 
});


}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}

 function showCompany(company) {

	var comp = doJSON(company);
	myWin = new Ext.Window({ // 2 
	id : 'myWin', 
      modal : true,
	height : 500, 
	width : 700, 
	title: 'Company Details',
      layout : 'fit',
	items : [ 
		{

			xtype:'panel',

			id:'responsetext',

			name:'responsetext',

			fieldLabel:'Comments',

			height:200,

			anchor:'98%',
                 html : '<b>Name : </b>' + comp.company.name + '<br><br>' + 'Membership Level ' + comp.company.membershiplevel


		}],

		buttons: [{ 
			text: 'Close', 
			handler: function(){ 
				myWin.close();
			} 
		}]
       
	}); 	
	myWin.show();


}

</script>
<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm z");
		sdf.setTimeZone(TimeZone.getTimeZone(session.person.address.timezone));
	        
		
%>

<div id="tabs"> 
	<ul> 
		<li><a href="#tabs-1">All</a></li> 
		<li><a href="#tabs-3">On My Inventory</a></li> 
		<li><a href="#tabs-3">On My Watchlist</a></li> 
		<li><a href="#tabs-2">From My Vendors</a></li> 
		<li><a href="#tabs-3">From My Countries</a></li> 
	</ul> 
	<div id="tabs-1"> 

				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.id" title="Broadcast Id" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alertList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td><g:each in="${alertMap.getAt(broadcast)}" var="filter">${filter.name}&nbsp;&nbsp;</g:each></td>
                            <td>${sdf.format(broadcast.dateCreated)}</td>
                            <td>${broadcast.id}</td>
                            <td>${broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->
</div> <!--  end tabs-1 -->
<div id="tabs-2">
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alertInstanceList}" status="i" var="alert">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td>${fieldValue(bean:alert, field:'alerttype')}</td>
                            <td>${sdf.format(alert.dateCreated)}</td>
                            <td>${alert.broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:alert.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${alert.broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:alert.broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->

</div>
<div id="tabs-3">
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
<!--						 
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   id="${(params.action == 'list')?'selected_search_tab':' '}"  class="${(params.action == 'list')?'selected':' '}" url="[action:'list']"   ><span><span>All</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtb')?'selected_search_tab':' '}" class="${(params.action == 'wtb')?'selected':' '}" url="[action:'wtb']"   ><span><span>WTB on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wts')?'selected_search_tab':' '}" class="${(params.action == 'wts')?'selected':' '}" url="[action:'wts']"   ><span><span>WTS on Inventory</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtbWatchlist')?'selected_search_tab':' '}" class="${(params.action == 'wtbWatchlist')?'selected':' '}" url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link  id="${(params.action == 'wtsWatchlist')?'selected_search_tab':' '}"  class="${(params.action == 'wtsWatchlist')?'selected':' '}" url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
							</ul>
						</div>
 -->
				<!--[if !IE]>start sec info<![endif]-->
				<div id="sec_info">
					
					<div class="row">
						<br><br>
					</div>
					
					
					<!--[if !IE]>start product list<![endif]-->
					<div id="product_list">
						
						
						<!--[if !IE]>start table_wrapper<![endif]-->
						<div class="table_wrapper">
							<div class="table_wrapper_inner">
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
                   	        <g:sortableColumn property="alerttype" title="Alert Type" />
                   	        <g:sortableColumn property="dateCreated" title="Date" />
                   	        <g:sortableColumn property="broadcast.broadcasttype" title="Broadcast Type" />
                   	        <g:sortableColumn property="broadcast.unit.part" title="Part" />
                   	        <g:sortableColumn property="broadcast.unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="broadcast.unit.condition" title="Condition" />
                   	        <g:sortableColumn property="broadcast.unit.price" title="Price" />
                   	        <g:sortableColumn property="broadcast.unit.qty" title="Quantity" />
                   	        <g:sortableColumn property="broadcast.description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
								<th>Actions</th>

	
								</tr>
								
                    <g:each in="${alerList}" status="i" var="alert">
                        <tr class="${(i % 2) == 0 ? 'first' : 'second'}">
                        
                        
                            <td>${alertMap.getAt(alert.broadcast)}</td>
                            <td>${sdf.format(alert.dateCreated)}</td>
                            <td>${alert.broadcast.broadcasttype}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'manufacturer.mfgname')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:alert.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:alert.broadcast, field:'description')}</td>
				    <td><a href="javascript:void(0)" id="company" onClick="javascript:getCompany('${session.company.id}');" >${alert.broadcast.company.name}</a></td>

				<g:if test="${alert.broadcast.company.id != session.company.id}">

					<td>
						<a href="javascript:void(0)" id="respond" onClick="javascript:showResponse('${fieldValue(bean:alert.broadcast, field:'id')}');" >Respond</a>
					</td>
				</g:if>

				<td>
						<div class="actions_menu">
							<ul>
												<li><g:remoteLink class="delete" controller="alert" action="delete" id="${alert?.id}" before="return confirm('Are you sure?');" params="[targetUri: (request.forwardURI - request.contextPath)]" >Delete</g:remoteLink></li>
											</ul>
										</div>
									</td>

                        </tr>
                    </g:each>

								
								
							</table>
							</div>
						</div>
						<!--[if !IE]>end table_wrapper<![endif]-->
						
						
					</div>
					<!--[if !IE]>end product list<![endif]-->
					
					
					<!--[if !IE]>start pagination<![endif]-->
					<div class="pagination">
						<span class="page_no">Page 1 of 217</span>
						
						<ul class="pag_list">
                	<g:paginate total="${alertTotal}" />
	
						</ul>
						
					</div>
					<!--[if !IE]>end pagination<![endif]-->
					
					
					
					
					
					
				</div>
				<!--[if !IE]>end sec info<![endif]-->
						
						
					
					
					</div>
				</div>
				<!--[if !IE]>end main info<![endif]-->

</div> 
</div> <!-- end tabs -->
 
				
				
				
				
        </div>
    </body>
</html>
