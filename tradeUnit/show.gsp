

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Show TradeUnit</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">TradeUnit List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New TradeUnit</g:link></span>
        </div>
        <div class="body">
            <h1>Show TradeUnit</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'id')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Condition:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'condition')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Manufacturer:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'manufacturer')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Part:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'part')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Price:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'price')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Qty:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:tradeUnit, field:'qty')}</td>
                            
                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${tradeUnit?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
