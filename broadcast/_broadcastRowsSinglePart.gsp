<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Broadcast" %>
<%@page import="test2.Cond" %>
<%@page import="test2.BroadcastFilter" %>
<%@page import="test2.Manufacturer" %>
<%@page import="test2.AppFlags" %>

<%
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		SimpleDateFormat stf = new SimpleDateFormat("hh:mm a");
		if (session.person?.address?.timezone) {
			sdf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
			stf.setTimeZone(TimeZone.getTimeZone(session.person?.address?.timezone));
		}

			
%>
<div class="table-responsive">
	<table class="table table-bordered gray-header responsive">
		<thead>
			<tr>
				<util:remoteSortableColumn property="broadcasttype" title="Type" action="filter"  params="${filterParams}"  update="tableRow" />
				<util:remoteSortableColumn property="sortableDateCreated" title="Date"  action="filter"  params="${filterParams}" update="tableRow"   />
				<util:remoteSortableColumn property="part" title="Part" action="filter"  params="${filterParams}"  update="tableRow"   />
				<util:remoteSortableColumn property="mfgname" title="Mfg" action="filter"  params="${filterParams}"  update="tableRow"  params="${filterParams}"  />
				<util:remoteSortableColumn property="condition" title="Cond" action="filter"  params="${filterParams}"  update="tableRow"   />
				<util:remoteSortableColumn property="price" title="Price"  action="filter"  params="${filterParams}"  update="tableRow"  />
				<util:remoteSortableColumn property="qty" title="Qty"  action="filter"  params="${filterParams}"  update="tableRow"  />
				<th>Description</th/>
				<th>Createdby</th>
				<th>Actions</th>
			</tr>
		</thead>
		
		<tbody>
			<g:each in="${broadcastList}" status="i" var="broadcast">
				<tr>
					<td>${fieldValue(bean:broadcast, field:'broadcasttype')}</td>
					<td>${sdf.format(broadcast.dateCreated)}<br>${stf.format(broadcast.dateCreated)}</td>
					<td>${fieldValue(bean:broadcast.unit, field:'part')}</td>
					<td>${fieldValue(bean:broadcast.unit, field:'manufacturer.mfgname')}</td>
					<td>${fieldValue(bean:broadcast.unit, field:'condition.name')}</td>
					<td>${fieldValue(bean:broadcast.unit, field:'price')}</td>
					<td>${fieldValue(bean:broadcast.unit, field:'qty')}</td>
					<td class="htmldscr">${broadcast.description}</td>
					<td>
							<g:link controller="person" action="profile" id="${broadcast?.createdby?.id}" >${broadcast?.createdby?.firstname} ${broadcast?.createdby?.lastname}</g:link><br>
							<a href="../company/view?id=${broadcast?.company?.id}" id="viewcompany"  >${broadcast?.company?.name}</a>
						<td>
							<g:if test="${broadcast?.company?.id != session?.company?.id}">
                                                                <button id="respond" type="button" onClick="javascript:showResponse('${fieldValue(bean:broadcast, field:'id')}','${broadcast?.createdby?.id}');return false;" class="btn btn-small btn-broadcast">
                                                                    <i class="fa fa-envelope-o"></i>Respond</button>
							</g:if>
						</td>
				</tr>
			</g:each>
		</tbody>
	</table>
</div>

<nav class="tab-bottom-pagination">
	<util:remotePaginate params="${filterParams}" action="filter" total="${broadcastTotal}" update="tableRow" />
</nav>
<script>
		$(document).ready(function(){
			
			$('.htmldscr').each(function(){
			
				var html = $(this).text().replace(',', '');
				$(this).html(html);
			});
			
		});
</script>
