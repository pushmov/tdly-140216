<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

        <link rel="stylesheet" href="${createLinkTo(dir:'css',file:'extjs/ext-all.css')}" />

	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'saas/saas-common.css')}"  />
	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'saas/saas-default.css')}"  />
	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'saas/grid.css')}"  />



	<link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'admin/bubble.css')}"  />
	<!--[if lte IE 6]><link media="screen" rel="stylesheet" type="text/css" href="${createLinkTo(dir:'css',file:'admin/ie.css')}" /><![endif]-->
        <g:javascript library="prototype/prototype" />				
        <g:javascript src="ext-prototype-adapter-debug.js" />				
        <g:javascript src="ext-base-debug.js" />				
        <g:javascript src="ext-all-debug.js" />				
<!--        <g:javascript src="BubblePanel.js" />				
        <g:javascript src="bubble-panel.js" />				
 -->
        <g:layoutHead />
        <g:javascript library="application" />	
	  <tooltip:resources/>			


</head>

<body class="inner_page">
<!--[if !IE]>start wrapper<![endif]-->
	<div id="wrapper">
		<div id="layout">
			<!--[if !IE]>start head<![endif]-->
			<div id="head">
				<div id="head_left">
					<div id="head_right">
						<!--[if !IE]>start site name<![endif]-->
						<div id="site_name">
							<h1><a href="/"></a></h1>
							<h2></h2>
						</div>
						<!--[if !IE]>end site name<![endif]-->
						<!--[if !IE]>start user menu<![endif]-->
						<!--[if !IE]>end user menu<![endif]-->
					</div>
				</div>
			</div>
			<!--[if !IE]>end head<![endif]-->
			<!--[if !IE]>start main_menu<![endif]-->
			<!--[if !IE]>end main_menu<![endif]-->
			
			<!--[if !IE]>start page<![endif]-->
			<div id="page">

<div class="header">
	<div>
		<div class="center">
			<div class="topLinks">
				<div class="menu-top-links-container">
					<ul id="menu-top-links" class="menu">
						<li class="menu-item current-menu-item"><a href="about.html">About</a></li>
						<li class="menu-item"><a href="blog.html">Blog</a></li>
						<li class="menu-item"><a href="help.html">Help</a></li>
						<li class="menu-item"><a href="contact-us.html">Contact Us</a></li>
					</ul>
				</div>
			</div>
			<h1 id="logo"><a href="home.html"><img src="../images/logo.png" alt="SaaS WP Theme II" /></a></h1>
			<div class="nav">
				<div class="menu-main-nav-container">
					<ul id="menu-main-nav" class="menu">
						<li class="menu-item"><a href="tour.html"><span>Tour</span></a></li>
						<li class="menu-item"><a href="plans-pricing.html"><span>Plans &#038; Pricing</span></a></li>
						<li class="menu-item"><a href="sign-up.html"><span>Sign Up!</span></a></li>
					</ul>
				</div>
			</div>
			<div class="page-title">
				<div>
					<h2>Sign Up</h2>					
				</div>
			</div>
		</div>
	</div>
</div>
<!--div.header end -->
<!--div.container start -->
<div class="container">
	<div class="center">



					<g:layoutBody />

	</div>
</div>

<!--div.container end -->
<!--div.footer start -->
<div class="footer">
	<div class="prom">
		<div class="center">
			<h2>
				<strong>Start doing stuff now! </strong>Try us out free for 30 days.			
			</h2>
			<div class="right"><a href="sign-up.html"><img src="../images/btn-sign-up.png" alt="Sign Up!"></a></div>
		</div>
	</div>
	<div class="links">
		<div class="center">	
			<div>
				<h3>Company</h3>
				<p>
					<a href="#">Leadership</a><br />
					<a href="#">Partners</a><br />
					<a href="#">Investors</a>
				</p>
			</div>
			<div>
				<h3>Keep in Touch</h3>
				<ul class="social-links">
					<li><a href="http://twitter.com/themeteam"><img src="../images/i_socialTwitter.png" alt="" /></a></li>
					<li><a href="http://www.facebook.com/facebook-name"><img src="../images/i_socialFacebook.png" alt="" /></a></li>
					<li><a href="http://www.flickr.com/photos/flickr-name/"><img src="../images/i_socialFlickr.png" alt="" /></a></li>
					<li><a href="http://saas-wp-ii.worryfreelabs.com/feed/"><img src="../images/i_socialRss.png" alt="" /></a></li>
					<li><a href="http://linkedin.com/in/linkedin-name"><img src="../images/i_socialLinkedin.png" alt="" /></a></li>
					<li><a href="http://www.youtube.com/user/youtube-name"><img src="../images/i_socialYoutube.png" alt="" /></a></li>
				</ul>
			</div>
			<div>
				<h3>Support</h3>
				<p>
					<a href="#">Create a Support Ticket</a><br />
					<a href="#">FAQs</a><br />
					<a href="#">Forum</a><br />
					<a href="#">Live Chat</a>
				</p>
			</div>
			<div>
				<h3>SaaS Corp, Inc.</h3>
				<p>544 Oenoke Ridge<br />
				New Canaan, CT 06480</p>
				<p><a href="http://gothemeteam.com/">http://gothemeteam.com/</a><br />
				Toll Free: (800) 555-1212</p>
			</div>		
		</div>
	</div>
</div>
<!--div.footer end -->



			</div>
			<!--[if !IE]>end page<![endif]-->
			
			<!--[if !IE]>start footer<![endif]-->
			<!--[if !IE]>end footer<![endif]-->
			
			
		</div>
	</div>
<!--[if !IE]>end wrapper<![endif]-->

</body>
</html>
