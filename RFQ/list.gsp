

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>RFQ List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New RFQ</g:link></span>
        </div>
        <div class="body">
            <h1>RFQ List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Company</th>
                   	    
                   	        <g:sortableColumn property="needby" title="Needby" />
                        
                   	        <g:sortableColumn property="rfqenddate" title="Rfqenddate" />
                        
                   	        <g:sortableColumn property="items" title="Items" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${RFQList}" status="i" var="RFQ">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${RFQ.id}">${fieldValue(bean:RFQ, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:RFQ, field:'company')}</td>
                        
                            <td>${fieldValue(bean:RFQ, field:'needby')}</td>
                        
                            <td>${fieldValue(bean:RFQ, field:'rfqenddate')}</td>
                        
                            <td>${fieldValue(bean:RFQ, field:'items')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${RFQ.count()}" />
            </div>
        </div>
    </body>
</html>
