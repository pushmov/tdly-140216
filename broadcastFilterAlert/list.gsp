 
<%@ page import="test2.BroadcastFilterAlert" %>
<%@ page import="test2.BroadcastFilter" %>
<%@ page import="test2.Manufacturer" %>
<%@ page import="test2.Continent" %>
<html>
    <head> 
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="pz" />
        <g:set var="entityName" value="${message(code: 'broadcastFilterAlert.label', default: 'BroadcastFilterAlert')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>

<script language="Javascript" >

$(document).ready(function()
        {


  $('#dialog-form').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            'Create a Reminder': function() {
		$('#broadcastFilterAlertForm').submit();              
                $(this).dialog('close');
            },
            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

  $('#dialog-form-edit').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            'Create a Reminder': function() {
		$('#broadcastFilterAlertFormEdit').submit();              
                $(this).dialog('close');
            },
            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });

    $('#dialog-responseSave').dialog({
        autoOpen: false,
        height: 150,
        width: 250,
        modal: true,
        buttons: {
            Ok: function() {
                $(this).dialog('close');
            }

        }        
    });

  $('#dialog-filter').dialog({
        autoOpen: false,
        height: 600,
        width: 650,
        modal: true,
        buttons: {
            'Create a Reminder': function() {
		$('#broadcastFilterForm').submit();              
                $(this).dialog('close');
            },
            Cancel: function() {
                $(this).dialog('close');
            }

        }        
    });


	      $('#broadcastFilterForm').submit( function() {
    
                            alert('frm submitted');
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFilter/save",
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
					alert('success');
				   //$('#dialog-responseSave').dialog("open");
    			    }
			    
                            });

        	            return false;

              });




	      $('#broadcastFilterAlertForm').submit( function() {


			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFilterAlert/save",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   //$('#dialog-responseSave').dialog("open");
					$('#broadcastFilterAlertTable > tbody:last').append(data);
    			    }
			    
                            });

        	            return false;

              });

	      $('#broadcastFilterAlertFormEdit').submit( function() {

		            var formData = $(this).serializeArray();
			    var id = "";
			$.each(formData,function(i,field) {
				if (field.name == "id") {
					id = field.value;
				}
			});	
			    $.ajax({
    			    type: "POST",
    			    url: "../broadcastFilterAlert/update",
			    cache: false,
    			    data: $(this).serialize(),
			    error: function(data, textStatus, jqXHR) {
			        alert("Error: " + textStatus);
    			    },
    			    success: function(data, textStatus, jqXHR) {
				   $("#broadcastFilterAlertTable > tbody >  tr[id='broadcastFilterAlert" + id + "']").replaceWith(data);
    			    }
			    
                            });

        	            return false;

              });


  $("#newAlert").click(function() {
	$('#dialog-form').dialog('open');
    });





  })

 function getFilter(id) {
	$.get("../broadcastFilter/get",{id: id},
		function(data,textStatus,jqXHR) {
		
			//alert($.toJSON(data));
			//alert('filterid : ' + data.id);

			$('#id').val(data.id);
			$('#name').val(data.name);
			$('#keyword').val(data.keyword);

			$('#broadcasttype').val(data.broadcasttype);
			$('#partlist').val(data.partlist);

			if(data.myVendors) {
				$('#myVendors').attr('checked', true);
			}
			$('input:radio[name="myCountries"]').filter('[value='+ data.myCountries + ']').attr('checked', true);
		
			var mfgArr = new Array(data.manufacturers.length);	
			for(i=0;i<data.manufacturers.length;i++) {
				mfgArr[i] = data.manufacturers[i].id;
			}
				$("#manufacturer").val(mfgArr);

			$("#continents option:first").attr('selected','selected');
			//prevSelect = $(this).val();

		


			//$('#dialog-form').binddata(data);
			$('#dialog-filter').dialog('open');
			

                }
             );
	
 }
  
 function getFilterAlert(id,name) {
	$.get("../broadcastFilterAlert/get",{id: id},
		function(data,textStatus,jqXHR) {
		
			//alert($.toJSON(data));
			//alert(data.broadcastFilter.name);
			$('#editBroadcastFilter').text(name);
			$('#broadcastFilterAlertFormEdit #id').val(id);
			$('#broadcastFilterAlertFormEdit #sms').attr('checked', data.sms);
			$('#broadcastFilterAlertFormEdit #email').attr('checked', data.email);
			$('#broadcastFilterAlertFormEdit #inbox').attr('checked', data.inbox);
			$('#broadcastFilterAlertFormEdit #gtalk').attr('checked', data.gtalk);
			$('#broadcastFilterAlertFormEdit #aol').attr('checked', data.aol);
			$('#broadcastFilterAlertFormEdit #yahoo').attr('checked', data.yahoo);
			$('#broadcastFilterAlertFormEdit #msn').attr('checked', data.msn);
			$('#dialog-form-edit').dialog('open');
                }
             );
	
 }


</script>


    </head>
    <body>

        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></span>
            <span class="menuButton"><a href="#" class="create" id="newAlert" ><g:message code="default.new.label" args="[entityName]" /></a></span>
        </div>
        <div class="body">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
<div id="dialog-responseSave" title="BroadcastFilter Communication Preference"> 
	<p> 
		<span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span> 
		 BroadcastFilter Communication Preference Saved.
	</p> 
</div>

<g:render template="/filterDialog"  />

<div id="dialog-form" title="Create new Reminder">
            <g:form name="broadcastFilterAlertForm" class="broadcastFilterAlertForm"  action="save" >

				<div class="row">
					<label>Filter Name:</label>
					<div class="inputs">
					<span class="input_wrapper select_wrapper">
                                   <g:select optionKey="id" from="${broadcastFilterList}" optionValue="name" class="bfclass" name="broadcastFilter.id" value="" ></g:select>
					</span>
					</div>
												<div class="module_options">
													<div class="module_options_inner">
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="sms" value="${sms}" />SMS</dd>
															<dd><g:checkBox name="email"  value="${email}" />Email</dd>
															<dd><g:checkBox name="inbox"  value="${inbox}" />Inbox</dd>
														</dl>
													</div>
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="gtalk"  value="${gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="yahoo"  value="${yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="msn"  value="${msn}" />MSN</dd>
															<dd><g:checkBox name="aol"  value="${aol}" />AIM</dd>
														</dl>
													</div>
													</div>
												</div>
				</div>
 

	    </g:form>
</div>
<div id="dialog-form-edit" title="Create new Reminder">
            <g:form name="broadcastFilterAlertFormEdit" class="broadcastFilterAlertFormEdit"  action="update" >

				<div class="row">
					<label>Filter Name:</label>
					<div id="editBroadcastFilter">
					</div>
						<g:hiddenField name="id"  value="" />
												<div class="module_options">
													<div class="module_options_inner">
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="sms" value="${sms}" />SMS</dd>
															<dd><g:checkBox name="email"  value="${email}" />Email</dd>
															<dd><g:checkBox name="inbox"  value="${inbox}" />Inbox</dd>
														</dl>
													</div>
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="gtalk"  value="${gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="yahoo"  value="${yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="msn"  value="${msn}" />MSN</dd>
															<dd><g:checkBox name="aol"  value="${aol}" />AIM</dd>
														</dl>
													</div>
													</div>
												</div>
				</div>
 

	    </g:form>
</div>

            <div class="list">
                <table id="broadcastFilterAlertTable" >
                    <thead>
                        <tr>
                        
                            <g:sortableColumn property="broadcastFilterAlert.broadcastFilter.name" title="${message(code: 'broadcastFilterAlert.broadcastFilter.name.label', default: 'Filter Name')}" />
                            <th>Alert Mode</th> 
                            <th>Action</th> 
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${broadcastFilterAlertInstanceList}" status="i" var="broadcastFilterAlertInstance">
          					    <g:render template="broadcastFilterAlertRow" var="broadcastFilterAlertInstance" bean="${broadcastFilterAlertInstance}" />
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${broadcastFilterAlertInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
