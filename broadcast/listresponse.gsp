 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="emerald" />
	<gui:resources components="['dialog','richEditor']"/>
        <title>Broadcast List</title>
	<script>
	function assignBid(bid) {
		dialog3Form.bid.value = bid
	}
	</script>
    </head>
    <body>
        <div class="body">
            <div class="list">
                <table border=1 width="1000px">
                    <thead>
                        <tr> 
                        
                   	        <g:sortableColumn property="broadcasttype" title="Broadcasttype" />
                   	        <g:sortableColumn property="created" title="Created" />
                   	        <g:sortableColumn property="unit.part" title="Part" />
                   	        <g:sortableColumn property="unit.manufacturer" title="Manufacturer" />
                   	        <g:sortableColumn property="unit.condition" title="Condition" />
                   	        <g:sortableColumn property="unit.price" title="Price" />
                   	        <g:sortableColumn property="unit.qty" title="quantity" />
                   	        <g:sortableColumn property="description" title="Description" />
                   	        <g:sortableColumn property="company" title="Company" />
                        
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${myBroadcastResponsesList}" status="i" var="broadcast">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                        
                            <td>${fieldValue(bean:broadcast.broadcast, field:'broadcasttype')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast, field:'dateCreated')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.unit, field:'part')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.unit, field:'manufacturer')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.unit, field:'condition')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.unit, field:'price')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.unit, field:'qty')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast, field:'description')}</td>
                            <td>${fieldValue(bean:broadcast.broadcast.company, field:'name')}</td>
			    <td width="200px">
		                    <g:each in="${broadcast.responses}" status="r" var="bresponse">
		                            ${fieldValue(bean:bresponse, field:'responsetext')}<br>
		                    </g:each>
			    </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>

            <div class="paginateButtons">
                <g:paginate total="${Broadcast.count()}" />
            </div>
        </div>
    </body>
</html>
