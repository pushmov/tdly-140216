<%@page import="java.text.*" %>
<%@page import="java.util.*" %>
<%@page import="test2.Continent" %>
<%@page import="test2.Country" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="pz" />
        <title>My Countries</title> 


<script language="javascript" >

function fadeit(e) {
		resp = doJSON(e.responseText);
		var title = "";
		if (resp.success) {
			title = "Success";
		}
		else {
			title = "Failure";
		}

		Ext.MessageBox.alert(title,resp.msg);
		var stopClose = function() {
			Ext.MessageBox.hide();
		};
		setTimeout(stopClose, 1500);
	}





function saveCountries(continents,countries) {

	cont_selected = new Array(); 
	for (var i = 0; i < continents.options.length; i++)  {
		if (continents.options[ i ].selected) 
			cont_selected.push(continents.options[ i ].value);
	}

	count_selected = new Array(); 
	for (var i = 0; i < countries.options.length; i++)  {
		if (countries.options[ i ].selected) 
			count_selected.push(countries.options[ i ].value);
	}

Ext.Ajax.request({
	url : '/test2/preferences/saveCountries' , 
	params : { continent : cont_selected, countries : count_selected },
	method: 'POST',
	success: function ( result,request ) { 
		var jsonData = doJSON(result.responseText);
		Ext.MessageBox.alert('Success', jsonData.msg	); 
	},
	failure: function ( result, request) { 
		var jsonData = doJSON(result.responseText);
		Ext.MessageBox.alert('Failed', jsonData.msg); 
	} 
});
}

function doJSON(stringData) {
		try {
			var jsonData = Ext.util.JSON.decode(stringData);
			return jsonData;
		}
		catch (err) {
			Ext.MessageBox.alert('ERROR', 'Could not decode ' + stringData);
		}
	}


 function setOptions(chosen) {
var selbox = document.myform.opttwo;
 
selbox.options.length = 0;
if (chosen == " ") {
  selbox.options[selbox.options.length] = new Option('Please select one of the options above first',' ');
 
}
if (chosen == "2") {
	<%
		def cont = Continent.get(2)
		cont.id = 2
		def countries = Country.findAllByContinent(cont)
		countries.each {
	%>
  selbox.options[selbox.options.length] = new Option('${it.name}','${it.id}');
	<%
		}
	%>
}
if (chosen == "3") {
	<%
		cont = Continent.get(3)
		cont.id = 3
		countries = Country.findAllByContinent(cont)
		countries.each {
	%>
  selbox.options[selbox.options.length] = new Option('${it.name}','${it.id}');
	<%
		}
	%>
}
if (chosen == "4") {
	<%
		cont = Continent.get(4)
		cont.id = 4
		countries = Country.findAllByContinent(cont)
		countries.each {
	%>
  selbox.options[selbox.options.length] = new Option('${it.name}','${it.id}');
	<%
		}
	%>
}
if (chosen == "5") {
	<%
		cont = Continent.get(5)
		cont.id = 5
		countries = Country.findAllByContinent(cont)
		countries.each {
	%>
  selbox.options[selbox.options.length] = new Option('${it.name}','${it.id}');
	<%
		}
	%>
}
if (chosen == "6") {
	<%
		cont = Continent.get(6)
		cont.id = 6
		countries = Country.findAllByContinent(cont)
		countries.each {
	%>
  selbox.options[selbox.options.length] = new Option('${it.name}','${it.id}');
	<%
		}
	%>
}
}

</script>


    </head>
    <body> 
				
				<!--[if !IE]>start main info<![endif]-->
				<div id="main_info">
					<div id="main_info_bottom">
						
						<div class="title_wrapper">
							
							<ul id="search_tabs" class="search_tabs" >
								<li><g:link   url="[action:'wtb']"   ><span><span>WTB</span></span></g:link></li>
								<li ><g:link   id="selected_search_tab"   url="[action:'wts']"   ><span><span>WTS</span></span></g:link></li>
								<li><g:link   url="[action:'wtbWatchlist']"   ><span><span>WTB on Watchlist</span></span></g:link></li>
								<li><g:link   url="[action:'wtsWatchlist']"   ><span><span>WTS on Watchlist</span></span></g:link></li>
								<li><g:link  url="[action:'mycountries']"   ><span><span>My Countries</span></span></g:link></li>
							</ul>
						</div>

				<!--[if !IE]>start sec info<![endif]-->
						<div class="title_wrapper">
							<h3>Alert Settings</h3>
						</div>
						
						
						<!--[if !IE]>start product page<![endif]-->
						<div id="product_page">
							<!--[if !IE]>start product content<![endif]-->
								<div id="product_content">
									<h4>WTS Broadcasts</h4>
								<!--[if !IE]>start forms<![endif]-->


<g:formRemote name="myform" url="[controller:'preferences',action:'saveWTS']"  onSuccess="fadeit(e)" onFailure="fadeit(e)"  >
															

									<div class="modules">
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
								<div class="row">
									<br/>
									<br/>
									<br/>
								</div>




										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Broadcasts from My Vendors</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="mvsms" value="${comm?.bcastwtsmyvendor?.sms}" />SMS</dd>
															<dd><g:checkBox name="mvemail"  value="${comm?.bcastwtsmyvendor?.email}" />Email</dd>
															<dd><g:checkBox name="mvinbox"  value="${comm?.bcastwtsmyvendor?.inbox}" />Inbox</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="mvgtalk"  value="${comm?.bcastwtsmyvendor?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="mvyahoo"  value="${comm?.bcastwtsmyvendor?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="mvmsn"  value="${comm?.bcastwtsmyvendor?.msn}" />MSN</dd>
															<dd><g:checkBox name="mvaol"  value="${comm?.bcastwtsmyvendor?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->

										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Broadcasts from My Countries</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="mcsms"  value="${comm?.bcastwts?.mycountryoptions?.sms}" />SMS</dd>
															<dd><g:checkBox name="mcemail"  value="${comm?.bcastwts?.mycountryoptions?.email}" />Email</dd>
															<dd><g:checkBox name="mcinbox"  value="${comm?.bcastwts?.mycountryoptions?.inbox}" />Inbox</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="mcgtalk"  value="${comm?.bcastwts?.mycountryoptions?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="mcyahoo"  value="${comm?.bcastwts?.mycountryoptions?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="mcmsn"  value="${comm?.bcastwts?.mycountryoptions?.msn}" />MSN</dd>
															<dd><g:checkBox name="mcaol"  value="${comm?.bcastwts?.mycountryoptions?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->

										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Broadcasts from All Other Companies/Countries</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="othsms"  value="${comm?.bcastwts?.othercountryoptions?.sms}" />SMS</dd>
															<dd><g:checkBox name="othemail"  value="${comm?.bcastwts?.othercountryoptions?.email}" />Email</dd>
															<dd><g:checkBox name="othinbox"  value="${comm?.bcastwts?.othercountryoptions?.inbox}" />Inbox</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="othgtalk"  value="${comm?.bcastwts?.othercountryoptions?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="othyahoo"  value="${comm?.bcastwts?.othercountryoptions?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="othmsn"  value="${comm?.bcastwts?.othercountryoptions?.msn}" />MSN</dd>
															<dd><g:checkBox name="othaol"  value="${comm?.bcastwts?.othercountryoptions?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
								<div class="row">
									<br/>
									<br/>
									<br/>
								</div>



</g:formRemote >



	
								

								
							</div> <!-- end modules -->						
						
				</div> <!-- product_content -->

								<div id="product_gallery">
									<h4>WTS Responses</h4>
								<!--[if !IE]>start forms<![endif]-->


<g:formRemote name="myform" url="[controller:'preferences',action:'saveWTSResp']"  onSuccess="fadeit(e)" onFailure="fadeit(e)"  >
															

									<div class="modules">
								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
								<div class="row">
									<br/>
									<br/>
									<br/>
								</div>




										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Responses from My Vendors</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="mvsms" value="${comm?.respwtsmyvendor?.sms}" />SMS</dd>
															<dd><g:checkBox name="mvemail"  value="${comm?.respwtsmyvendor?.email}" />Email</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="mvgtalk"  value="${comm?.respwtsmyvendor?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="mvyahoo"  value="${comm?.respwtsmyvendor?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="mvmsn"  value="${comm?.respwtsmyvendor?.msn}" />MSN</dd>
															<dd><g:checkBox name="mvaol"  value="${comm?.respwtsmyvendor?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->

										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Responses from My Countries</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="mcsms"  value="${comm?.respwts?.mycountryoptions?.sms}" />SMS</dd>
															<dd><g:checkBox name="mcemail"  value="${comm?.respwts?.mycountryoptions?.email}" />Email</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="mcgtalk"  value="${comm?.respwts?.mycountryoptions?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="mcyahoo"  value="${comm?.respwts?.mycountryoptions?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="mcmsn"  value="${comm?.respwts?.mycountryoptions?.msn}" />MSN</dd>
															<dd><g:checkBox name="mcaol"  value="${comm?.respwts?.mycountryoptions?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->

										<!--[if !IE]>start module<![endif]-->
										<div class="module">
											<!--[if !IE]>start module top<![endif]-->
											<div class="module_top">
												<h5>Alerts for WTS Responses from All Other Companies/Countries</h5>
												<a href="#" class="edit_module help_module">Help</a>
											</div>
											<!--[if !IE]>end module top<![endif]-->
											<!--[if !IE]>start module bottom<![endif]-->
											<div class="module_bottom">
												<!--[if !IE]>start module options<![endif]-->
												<div class="module_options">
													<div class="module_options_inner">
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>General</dt>
															<dd><g:checkBox name="othsms"  value="${comm?.respwts?.othercountryoptions?.sms}" />SMS</dd>
															<dd><g:checkBox name="othemail"  value="${comm?.respwts?.othercountryoptions?.email}" />Email</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<div class="module_option">
														<dl>
															<dt>Instant Messenger</dt>
															<dd><g:checkBox name="othgtalk"  value="${comm?.respwts?.othercountryoptions?.gtalk}" />Gtalk</dd>
															<dd><g:checkBox name="othyahoo"  value="${comm?.respwts?.othercountryoptions?.yahoo}" />Yahoo</dd>
															<dd><g:checkBox name="othmsn"  value="${comm?.respwts?.othercountryoptions?.msn}" />MSN</dd>
															<dd><g:checkBox name="othaol"  value="${comm?.respwts?.othercountryoptions?.aol}" />AIM</dd>
														</dl>
													</div>
													<!--[if !IE]>end module option<![endif]-->
													<!--[if !IE]>start module option<![endif]-->
													<!--[if !IE]>end module option<![endif]-->
													</div>
												</div>
												<!--[if !IE]>end module options<![endif]-->
											</div>
											<!--[if !IE]>end module bottom<![endif]-->
										</div>
										<!--[if !IE]>end module<![endif]-->
 

								<div class="row">
									<div class="inputs">
										<span class="button blue_button search_button"><span><span><em>Save</em></span></span><input name="" type="submit" /></span>
									</div>
								</div>
								<div class="row">
									<br/>
									<br/>
									<br/>
								</div>



</g:formRemote >



	
								

								
							</div> <!-- end modules -->						
						
				</div> <!-- product_content -->


			</div> <!-- product_page -->
		</div> <!-- main_info_bottom -->
	</div> <!-- main_info -->
</body>
</html>

